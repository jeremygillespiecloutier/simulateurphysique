<html>
<p>
L'onglet performance permet d'afficher des statistiques en lien avec la performance du logiciel.
<ul>
		<li>Temps �coul�</li> Le temps �coul� dans l'animation de la sc�ne.
		<li>Fr�quence image cible</li> La fr�quence d'actualisation vis�e.
		<li>Fr�quence image</li> La fr�quence d'actualisation r�elle de l'animation.
		<li>Temps r�el</li> Le temps �coul� sur l'horloge de l'ordinateur depuis le d�but de l'animation.
		<li>Delta t</li> L'interval de temps entre chaque actualisation du moteur physique.
		<li>�cart</li> Le pourcentage d'�cart entre la fr�quence cible et la fr�quence actuelle.
	</ul>
<img src=mlg420<images-aide/ongletPerformance.png> style="width:50px;height:50px" />
</p>
</html>