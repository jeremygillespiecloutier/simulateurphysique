<html>
<p>
Cette application est un simulateur physique qui permet de cr�er et d'animer des objets de fa�on fid�le au monde r�el. Le moteur physique de l'application effectue la
 d�tection de collision entre les diff�rents objets et d�termine la r�ponse � adopter. L'application permet �galement d'interragir avec les objets cr��s en pla�ant des
 explosions, des zones de gravit� et des ressorts. Les sc�nes cr��s par les utilisateurs peuvent �galement �tre export�es et partag�es avec d'autres gens.
 <br/>
 <br/>
 <b><u>Fonctionnalit�s de base:</u></b><br/><br/>
<b>Lancer l'animation :</b> Appuyer sur le bouton d�marrer ou la touche espace <br/>
<b>Changer la vitesse d'animation :</b> D�placer le curseur ou appuyer sur les touches gauche et droite du clavier.
</p>
</html>