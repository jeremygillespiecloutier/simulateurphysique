<html>
<p>
<img src=mlg420<images-aide/dialogueFonctions.png> style="width:50px;height:50px" /><br/>
<i>Le bouton "Ajouter" ajoute une fonction à la liste d'objets et le bouton "Ok" ajoute la fonction sélectionnée dans la liste 
au corps présentement sélectionné. Chaque fonction possède un paramètre de vitesse ainsi qu'un nom permettant de l'identifier.</i>
<br/>
<br/>
(Ce menu est seulement disponible si un corps unique est sélectionné)<br/>
Fonctionnalités:
<ul>
		<li>Ajouter une fonction linéaire <b>Alt+L</b></li>
		Ajoute une fonction linéaire de déplacement au corps avec la pente et la longueur spécifiées.
		<li>Ajouter une fonction cercle <b>Alt+C</b></li>
		Ajoute une fonction cercle de déplacement au corps avec le rayon spécifié. <br/>
		L'option inverser sens change le sens de déplacement du corps sur le cercle.
		<li>Ajouter une fonction trigonométrique <b>Alt+T</b></li>
		Ajoute une fonction sinus ou cosinus avec l'amplitude, la longueur d'onde et le nombre de cycles spécifiés.<br/>
		L'option inversion est seulement disponible pour les fonctions sinus et inverse l'amplitude de la fonction quand elle fini ses cycles.
		<li>Supprimer fonction <b>Alt+S</b></li>
		Supprime la fonction du corps sélectionné.
</p>
</html>