<html>
<p>
	Permet de dessiner une zone dans laquelle la gravit� est diff�rente de celle de la sc�ne.<br/>
	Les polygones sont affect�s par cette gravit� lorsque leur centre se trouve dans la zone de gravit�.
</p>
<p>
	<b>Raccourci : Z</b>
	<img src=mlg420<outilZone50x50.png> style="width:50px;height:50px" />
</p>
</html>