package moteur;

import java.awt.geom.Area;
import java.io.Serializable;

import maths.Vecteur;
import dessinable.DessinableScientifique;
import enums.TypeObjet;

/**
 * @author Francis Labelle
 * Classe de base pour les objets physiques (Corps et ZoneDeGravite).
 */
public abstract class ObjetPhysique implements DessinableScientifique, Serializable {
	private static final long serialVersionUID = -211607074949613635L;
	private boolean selectionne = false;
	private boolean estEnDeplacement = false;
	private double rotation = 0;
	public abstract TypeObjet getTypeObjet();
	public abstract ObjetPhysique copier();
	/**
	 * Indique si l'objet contient un point.
	 * @param point Le point � tester.
	 * @return Bool�en indiquant si l'objet contient un point.
	 */
	public abstract boolean contient(Vecteur point);
	/**
	 * Indique si l'objet contient une aire.
	 * @param aire L'aire � tester.
	 * @return Bool�en indiquant si l'objet contient une aire.
	 */
	public abstract boolean contient(Area aire);
	public abstract Vecteur getPosition();
	public abstract void setPosition(Vecteur position);
	/**
	 * Retourne le minimum du cadre entourant l'objet.
	 * @return Le minimum du cadre entourant l'objet.
	 */
	public abstract Vecteur getMin();
	/**
	 * Retourne le maximum du cadre entourant l'objet.
	 * @return Le maximum du cadre entourant l'objet.
	 */
	public abstract Vecteur getMax();
	public abstract String getNomFamilier(int numero);
	/**
	 * Tourne l'objet physique d'un angle autour d'un point
	 * @param angle L'angle de rotation.
	 * @param centre Le centr de rotation.
	 */
	public void tourner(double angle, Vecteur centre){
		this.rotation = (this.rotation + angle) % (2.0 * Math.PI);
		if(centre.equals(getPosition()))
			return;
		Vecteur rayon1 = centre.sub(getPosition());
		Vecteur rayon2 = new Vecteur(rayon1.longueur() * Math.cos(rayon1.getOrientation() + angle), rayon1.longueur() * Math.sin(rayon1.getOrientation() + angle));
		setPosition(getPosition().sub(rayon2.sub(rayon1)));
	}
	
	/**
	 * Retourne un bool�en indiquant si l'objet est s�lectionn�.
	 * @return Bool�en indiquant si l'objet est s�lectionn�.
	 */
	public boolean getSelectionne() {
		return selectionne;
	}

	/**
	 * Change la valeur de s�lection de l'objet.
	 * @param selectionne Bool�en indiquant si l'objet est s�lectionn�.
	 */
	public void setSelectionne(boolean selectionne) {
		this.selectionne = selectionne;
	}

	/**
	 * M�thode appel�e au d�but du d�placement de l'objet.
	 */
	public void debutDeplacement() {
		estEnDeplacement = true;
	}

	/**
	 * Retourne une valeur bool�enne indiquant si l'objet est en d�placement.
	 * @return Bool�en indiquant si l'objet est en d�placement.
	 */
	public boolean enDeplacement() {
		return estEnDeplacement;
	}

	/**
	 * M�thode appel�e � la fin du d�placement de l'objet.
	 */
	public void finDeplacement() {
		estEnDeplacement = false;
	}

	/**
	 * Retourne la rotation de l'objet.
	 * @return La rotation de l'objet.
	 */
	public double getRotation() {
		return this.rotation;
	}

	/**
	 * Change la rotation de l'objet.
	 * @param rotation La rotation de l'objet.
	 */
	public void setRotation(double rotation) {
		this.rotation = rotation;
	}
}
