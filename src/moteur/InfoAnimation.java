package moteur;

import java.io.Serializable;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe enregistrant des informations relatives au d�roulement de l'animation du programme.
 */
public class InfoAnimation implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6362140968426958323L;
	private double tempsEcoule;
	private double deltaT;
	private double freq;
	private double freqCible;
	private double tempsReel;
	/**
	 * Cr�e un objet InfoAnimation
	 * @param tempsEcoule Le temps �coul� dans l'animation.
	 * @param deltaT L'intervalle de temps utilis� dans l'animation.
	 * @param freq La fr�quence actuelle d'animation.
	 * @param freqCible La fr�quence cible d'animation.
	 * @param tempsReel Le temps r�el �coul�
	 */
	public InfoAnimation(double tempsEcoule, double deltaT, double freq, double freqCible, double tempsReel){
		this.tempsEcoule=tempsEcoule;
		this.deltaT=deltaT;
		this.freq=freq;
		this.freqCible=freqCible;
		this.tempsReel=tempsReel;
	}
	/**
	 * Retourne le temps �coul� dans l'animation.
	 * @return Le temps �coul� dans l'animation.
	 */
	public double getTempsEcoule(){
		return this.tempsEcoule;
	}
	/**
	 * Retourne l'intervalle de temps utilis� dans l'animation.
	 * @return L'intervalle de temps utilis� dans l'animation.
	 */
	public double getDeltaT(){
		return this.deltaT;
	}
	/**
	 * Retourne la fr�quence de l,animation.
	 * @return La fr�quence de l'animation.
	 */
	public double getFreq(){
		return this.freq;
	}
	/**
	 * Retourne la fr�quence cible de l'animation.
	 * @return La fr�quence cible de l'animation.
	 */
	public double getFreqCible(){
		return this.freqCible;
	}
	/**
	 * Retourne le temps r�el �coul� de puis le d�but de l'animation.
	 * @return Le temps r�el �coul�
	 */
	public double getTempsReel(){
		return this.tempsReel;
	}
}
