package moteur;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.DefaultListModel;

import materiaux.Materiau;
import materiaux.TypeMateriau;
import maths.Vecteur;
import physique.Corps;
import physique.Explosion;
import physique.Ressort;
import physique.ZoneDeGravite;
import collision.Collision;
import collision.InfoCollision;
import dessinable.VecteurDessinable;
import ecouteurs.EcouteurUniversel;
import enums.TypeObjet;
import formes.Polygone;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe qui contient l'ensemble des objets qui sot anim�s et effectue les calculs physiques servant � d�terminer
 * la position des objets.
 */
public class Scene implements Serializable {
	private Vecteur gravite = new Vecteur(0, -10);
	private static final long serialVersionUID = -1494679835638002245L;
	private final double LARGEUR = 60;
	private final double HAUTEUR = 60;
	private ArrayList<ObjetPhysique> objets = new ArrayList<ObjetPhysique>();
	private ArrayList<Corps> corps = new ArrayList<Corps>();
	private ArrayList<ZoneDeGravite> zones=new ArrayList<ZoneDeGravite>();
	private ArrayList<Explosion> explosions=new ArrayList<Explosion>();
	private ArrayList<Ressort> ressorts=new ArrayList<Ressort>();
	private Vecteur position = new Vecteur(0, 0);
	private double distanceQuadrillage = 1.0;
	private ArrayList<Ellipse2D.Double> contacts = new ArrayList<Ellipse2D.Double>();
	private transient EcouteurUniversel ecouteur;
	private Corps cotes[] = new Corps[4];
	private ArrayList<VecteurDessinable> normales = new ArrayList<VecteurDessinable>();
	private ArrayList<Paire> paires = new ArrayList<Paire>();
	private static final double FROTTEMENT_MIN = 0.0001;
	private static final double POURCENTAGE = 0.8;
	private static final double DEBORDAGE = 0.08;
	private String locationFichier = "";
	private final double tailleContacts=0.15;
	private ArrayList<ObjetPhysique> objetsASupprimer=new ArrayList<ObjetPhysique>();
	private ArrayList<ObjetPhysique> objetsAAjouter=new ArrayList<ObjetPhysique>();
	private TypeMateriau materiau = TypeMateriau.brique;
	private ArrayList<ObjetPhysique> selection=new ArrayList<ObjetPhysique>();
	private boolean vueDeHaut=false;
	/**
	 * Cr�e une nouvelle sc�ne
	 */
	public Scene() {
		cotes[0] = new Corps(new Polygone(1000000, 1000000, new Vecteur(-LARGEUR / 2.0 - 500000, 0)));((Polygone)cotes[0].getForme()).setNormaleSpeciale(new Vecteur(-1,0));
		cotes[1] = new Corps(new Polygone(1000000, 1000000, new Vecteur(LARGEUR / 2.0 + 500000, 0)));((Polygone)cotes[1].getForme()).setNormaleSpeciale(new Vecteur(1,0));
		cotes[2] = new Corps(new Polygone(1000000, 1000000, new Vecteur(0, -HAUTEUR / 2.0 - 500000)));((Polygone)cotes[2].getForme()).setNormaleSpeciale(new Vecteur(0,-1));
		cotes[3] = new Corps(new Polygone(1000000, 1000000, new Vecteur(0, HAUTEUR / 2.0 + 500000)));((Polygone)cotes[3].getForme()).setNormaleSpeciale(new Vecteur(0,1));
		for(int i = 0; i < cotes.length; i++) {
			cotes[i].setFixe(true);
		}
	}
	/**
	 * M�thode appel�e quand une nouvelle sc�ne est charg�e. Rend les objets non s�lectionn�s et non suivis
	 */
	public void sceneChargee() {
		//quand on load une scene d'un fichier mlg
		for(int i = 0; i < objets.size(); i++) {
			if(objets.get(i).getTypeObjet() == TypeObjet.CORPS)
				((Corps) objets.get(i)).setSuivi(false);
			objets.get(i).setSelectionne(false);
		}
	}
	/**
	 * Change l'�couteur qui sera notifi� par la classe sc�ne.
	 * @param ecouteur L'�couteur notifi�.
	 */
	public void ajouterEcouteur(EcouteurUniversel ecouteur) {
		this.ecouteur = ecouteur;
	}
	/**
	 * Retourne la largeur de la sc�ne.
	 * @return La largeur de la sc�ne (m�tres).
	 */
	public double getLargeur() {
		return this.LARGEUR;
	}
	/**
	 * Retourne la hauteur de la sc�ne.
	 * @return La hauteur de la sc�ne (m�tres).
	 */
	public double getHauteur() {
		return this.HAUTEUR;
	}
	/**
	 * Retourne la position de la sc�ne.
	 * @return La position de la sc�ne.
	 */
	public Vecteur getPosition() {
		return position;
	}
	/**
	 * Change la position de la sc�ne.
	 * @param position La position de la sc�ne.
	 */
	public void setPosition(Vecteur position) {
		this.position = position;
	}
	/**
	 * D�place la sc�ne d'une certaine position.
	 * @param x Le d�placement en x.
	 * @param y Le d�placement en y.
	 */
	public void changerPosition(double x, double y) {
		position = new Vecteur(position.getX() + x, position.getY() + y);
	}
	/**
	 * Change la position de la sc�ne.
	 * @param x La position de la sc�ne en x.
	 * @param y La position de la sc�ne en y.
	 */
	public void setPosition(double x, double y) {
		position = new Vecteur(x, y);
	}
	/**
	 * Dessine tous les objets contenus dans la sc�ne.
	 * @param g2d L'objet graphique sur lequel les objets sont dessin�s.
	 * @param matMC la matrice de transformation appliqu�e au dessin.
	 * @param scientifique Bool�en indiquant si le mode scientifique doit �tre dessin�.
	 * @param performance Boll�en indiquant si le mode performance (simplifi�) doit �tre dessin�.
	 * @param scale l'agrandissement des textures
	 * @param chemin Bool�en indiquant si on doit dessiner sans textures (mode chemin actif).
	 */
	public synchronized void dessiner(Graphics2D g2d, AffineTransform matMC, boolean scientifique, boolean performance, boolean chemin, double scale) {
		Color couleurInitiale = g2d.getColor();
		for(ZoneDeGravite zone : zones){
			zone.dessiner(g2d, matMC, scale);
		}
		for(ObjetPhysique objet : objets) {
			if(objet.getTypeObjet() != TypeObjet.ZONE_DE_GRAVITE){
				if(objet.getSelectionne() || (objet.getTypeObjet() == TypeObjet.CORPS && ((Corps)objet).getSuivi())){
					selection.add(objet);
				}else{
					objet.dessiner(g2d, matMC, scale);
				}
			}
		}
		for(ObjetPhysique objet : selection) {
			objet.dessiner(g2d, matMC, scale);
		}
		if(scientifique){
			for(ObjetPhysique objet : objets) {
				objet.dessinerScientifique(g2d, matMC);
			}
			
			for(Ellipse2D.Double contact : contacts) {
				g2d.setColor(Color.BLACK);
				g2d.fill(matMC.createTransformedShape(contact));
			}
			
			for(VecteurDessinable normale : normales) {
				normale.setCouleur(Color.RED);
				normale.dessiner(g2d, matMC, scale);
			}
		}
		if(chemin){
			for(Corps c : corps) {
				if(c.getFixe() && c.getFonction()!=null){
					c.getFonction().dessinerFonction(g2d, matMC, c.getPosition());
				}
			}
		}
		selection.clear();
		g2d.setColor(couleurInitiale);
	}
	/**
	 * M�thode appel�e a chaque it�ration du thread pour actualiser les positions des objets.
	 * @param deltaT L'intervalle de temps �coul�.
	 */
	public synchronized void actualiser(double deltaT) {
		contacts.clear();
		normales.clear();
		paires.clear();
		//Applique les explosions aux objets.
		for(int i=0;i<explosions.size();i++){
			for(int j=0;j<corps.size();j++){
				explosions.get(i).appliquer(corps.get(j));
			}
		}
		//Applique la force des ressorts.
		for(int j=0;j<ressorts.size();j++){
			ressorts.get(j).setForces();
		}
		//Change la gravit� des objets en fonction dez zones et actualise leurs forces
		for(int i = 0; i < corps.size(); i++) {
			Corps c=corps.get(i);
			calculerGravite(c);
			if(!c.enDeplacement() && !c.getFixe()) {
				c.actualiserForces(deltaT, vueDeHaut);
			}
		}
		// Teste le collisions entre les paires d'objets 
		for(int i = 0; i < corps.size(); i++) {
			Corps c1 = corps.get(i);
			for(int k = 0; k < cotes.length; k++) {
				InfoCollision murs = Collision.testerCollision(c1.getForme(), cotes[k].getForme());
				if(murs != null) {
					paires.add(new Paire(i, -1, murs, k));
					appliquerImpulsion(c1, cotes[k], murs);
				}
			}
			for(int j = i + 1; j < corps.size(); j++) {
				Corps c2 = corps.get(j);
				InfoCollision info = Collision.testerCollision(c1.getForme(), c2.getForme());
				if(info != null) {
					paires.add(new Paire(i, j, info, -1));
					appliquerImpulsion(c1, c2, info);
				}
			}
		}
		// Actualise la vitesse des objets.
		for(int i = 0; i < corps.size(); i++) {
			Corps c = corps.get(i);
			if(!c.enDeplacement()) {
				if(c.getFixe() && c.getFonction()!=null){
					c.setPosition(c.getPosition().add(c.getFonction().getDeplacement(deltaT)));
				}
				c.actualiserVitesses(deltaT);
				c.setForce(new Vecteur(0,0));
			}
			if(c.getSuivi() && !c.enDeplacement()) {
				ecouteur.bougerScene(c.getPosition());
			}
		}
		// Corrige la position d'une paire d'objets en collision.
		for(int i = 0; i < paires.size(); i++) {
			Paire p = paires.get(i);
			Corps c1 = corps.get(p.getIndice1());
			Corps c2;
			if(p.getIndiceLimite() != -1) {
				c2 = cotes[p.getIndiceLimite()];
			} else {
				c2 = corps.get(p.getIndice2());
			}
			corrigerPosition(c1, c2, p.getInfoCollision());
		}
		//Supprime les explosions qui ont �t� appliqu�es.
		boolean changement=false;
		for(int i=0;i<explosions.size();i++){
			changement=true;
			supprimerObjet(explosions.get(i));
		}
		//supprime les objets dans la liste d'objets � supprimer
		effectuerSuppression();
		effectuerAjout();
		if(changement){
			ecouteur.objetsChanges(true);
		}
	}
	/**
	 * Calcul la gravit� exerc�e sur un corps.
	 * @param c Le corps sur lequel la gravit� est exerc�e.
	 */
	public void calculerGravite(Corps c){
		c.setGravite(gravite);
		for(int h=0;h<zones.size();h++){
			ZoneDeGravite zone=zones.get(h);
			if(zone.contient(c.getPosition())){
				c.setGravite(c.getGravite().add(zone.getGravite()));
			}
		}
		c.setAcceleration(c.getGravite());
	}
	/**
	 * Applique une impulsion aux corps entr�s en collision.
	 * @param c1 Le premier corps.
	 * @param c2 Le second corps.
	 * @param info Les informations relatives � la collision.
	 */
	public void appliquerImpulsion(Corps c1, Corps c2, InfoCollision info) {
		//Ajout des normales de collision � la liste des normales � dessiner.
		VecteurDessinable normale1 = new VecteurDessinable(info.getNormale().mul(-1));
		normale1.setPosition(c1.getPosition());
		VecteurDessinable normale2 = new VecteurDessinable(info.getNormale());
		normale2.setPosition(c2.getPosition());
		normales.add(normale1);
		normales.add(normale2);
		//calcul des propri�t�s des objets.
		ArrayList<Vecteur> points = info.getPoints();
		double m1Inv = c1.getMasseInverse();
		double m2Inv = c2.getMasseInverse();
		double restitution = Materiau.getRestitution(c1.getMateriau(), c2.getMateriau());
		Vecteur v1 = c1.getVitesse();
		Vecteur v2 = c2.getVitesse();
		double vAng1 = c1.getVitesseAngulaire();
		double vAng2 = c2.getVitesseAngulaire();
		//on applique une impulsion pour chaque point de contacts de la collision.
		for(int i = 0; i < points.size(); i++) {
			Vecteur point=points.get(i);
			//Ajout du point de contacts � la liste de points de contacts � dessiner.
			contacts.add(new Ellipse2D.Double(point.getX()-tailleContacts/2.0, point.getY()-tailleContacts/2.0, tailleContacts, tailleContacts));
			Vecteur rayon1 = points.get(i).sub(c1.getPosition());
			Vecteur rayon2 = points.get(i).sub(c2.getPosition());
			Vecteur vitesseRelative = v2.add(rayon2.croix(vAng2)).sub(v1).sub(rayon1.croix(vAng1));
			double vitesseNormale = vitesseRelative.produitScalaire(info.getNormale());
			if(vitesseNormale > 0) {
				return;
			}
			//calcul de l'impulsion de collision � appliquer.
			double w1 = Math.pow(rayon1.produitCroix(info.getNormale()), 2) * c1.getInertieInverse();
			double w2 = Math.pow(rayon2.produitCroix(info.getNormale()), 2) * c2.getInertieInverse();
			double impulsion = -1 * (1 + restitution) * vitesseNormale / (m1Inv + m2Inv + w1 + w2) / points.size();
			Vecteur vImpulsion = info.getNormale().mul(impulsion);
			c1.appliquerImpulsion(vImpulsion.mul(-1), rayon1);
			c2.appliquerImpulsion(vImpulsion, rayon2);

			//calcul de l'impulsion de frottement � appliquer
			vitesseRelative = c2.getVitesse().add(rayon2.croix(c2.getVitesseAngulaire())).sub(c1.getVitesse()).sub(rayon1.croix(c1.getVitesseAngulaire()));
			Vecteur tangente = vitesseRelative.sub(info.getNormale().mul(vitesseRelative.produitScalaire(info.getNormale()))).unitaire();
			double impulsionF = -(1 + restitution) * vitesseRelative.produitScalaire(tangente) / (m1Inv + m2Inv + w1 + w2) / points.size();
			double friction = Materiau.getStatique(c1.getMateriau(), c2.getMateriau());
			Vecteur vImpulsionF;
			//On d�termine si on doit appliquer un frottement statique ou cin�tique.
			if(Math.abs(impulsionF) < impulsion * friction) {
				vImpulsionF = tangente.mul(impulsionF);
			} else {
				friction = Materiau.getCinetique(c1.getMateriau(), c2.getMateriau());
				vImpulsionF = tangente.mul(impulsion).mul(-friction);
			}
			//On applique seulement l'impulsion si  elle est suffisament grande.
			if(Math.abs(impulsionF) > Scene.FROTTEMENT_MIN) {
				c1.appliquerImpulsion(vImpulsionF.mul(-1), rayon1);
				c2.appliquerImpulsion(vImpulsionF, rayon2);
			}
		}

	}
	/**
	 * Rend un objet s�lectionn� et d�selectionne tous les autres.
	 * @param objet L'objet � s�lectionner.
	 */
	public void setSelection(ObjetPhysique objet) {
		for(ObjetPhysique o : objets) {
			if(o != null) {
				o.setSelectionne(o.equals(objet));
			}
		}
	}
	/**
	 * Rend un objet s�lectionn� et d�selectionne tous les autres.
	 * @param index L'indice de l'objet � s�lectionner.
	 */
	public void setSelection(int index) {
		for(int i = 0 ; i < objets.size() ; i++) {
			objets.get(i).setSelectionne(i == index);
		}
	}
	/**
	 * Corrige la position des corps afins qu'ils n'entrent pas en collision.
	 * @param c1 Le premier corps.
	 * @param c2 Le second corps.
	 * @param info L,information relative � la collision.
	 */
	public void corrigerPosition(Corps c1, Corps c2, InfoCollision info) {
		Vecteur correction = info.getNormale().mul(Math.max(info.getProfondeur() - Scene.DEBORDAGE, 0) / (c1.getMasseInverse() + c2.getMasseInverse()) * Scene.POURCENTAGE);
		if(!c1.getFixe() && !c1.enDeplacement()) {
			c1.setPosition(c1.getPosition().sub(correction.mul(c1.getMasseInverse())));
		}
		if(!c2.getFixe() && !c2.enDeplacement()) {
			c2.setPosition(c2.getPosition().add(correction.mul(c2.getMasseInverse())));
		}
	}
	/**
	 * Ajoute un objet � la sc�ne.
	 * @param objet L'objet � ajouter.
	 */
	public void ajouterObjet(ObjetPhysique objet) {
		objetsAAjouter.add(objet);
		if(!ecouteur.animationEnCours()){
			effectuerAjout();
		}
	}
	/**
	 * Ajoute les objets qui sont dans la liste d'objets � ajouter.
	 */
	private void effectuerAjout(){
		for(int j=0;j<objetsAAjouter.size();j++){
			ObjetPhysique objet=objetsAAjouter.get(j);
			switch(objet.getTypeObjet()) {
				case CORPS:
					corps.add((Corps)objet);
					calculerGravite((Corps)objet);
					break;
				case ZONE_DE_GRAVITE:
					zones.add((ZoneDeGravite)objet);
					break;
				case EXPLOSION:
					explosions.add((Explosion)objet);
					break;
				case RESSORT:
					ressorts.add((Ressort)objet);
					break;
			}
			objets.add(objet);
		}
		objetsAAjouter.clear();
	}
	/**
	 * Retourne la distance entre les lignes du quadrillage.
	 * @return Distance entre les lignes du quadrillage.
	 */
	public double getDistanceQuadrillage() {
		return distanceQuadrillage;
	}
	/**
	 * D�termine quel objet contient un point.
	 * @param v Le point � v�rifier
	 * @return L'objet qui contient le point.
	 */
	public ObjetPhysique contient(Vecteur v) {
		ObjetPhysique resultat = null;
		for(int i = 0; i < objets.size(); i++) {
			ObjetPhysique objet = objets.get(i);
			if(objet.getTypeObjet()!=TypeObjet.ZONE_DE_GRAVITE){
				if(objet.contient(v) && objet.getSelectionne() == false) {
					resultat = objet;
				} else if(objet.contient(v) && objet.getSelectionne() == true) {
					resultat = objet;
					return resultat;
				}
			}
		}
		if(resultat==null){
			for(int i = 0; i < zones.size(); i++) {
				ZoneDeGravite zone = zones.get(i);
				if(zone.contient(v) && zone.getSelectionne() == false) {
					resultat = zone;
				} else if(zone.contient(v) && zone.getSelectionne() == true) {
					resultat = zone;
					return resultat;
				}
			}
		}
		return resultat;
	}
	/**
	 * Retourne la liste des corps contenus dans la sc�ne.
	 * @return La liste des corps contenus dans la sc�ne.
	 */
	public ArrayList<Corps> getCorps() {
		return this.corps;
	}
	/**
	 * Retourne la liste des objets contenus dans la sc�ne.
	 * @return La liste des objets contenus dans la sc�ne.
	 */
	public ArrayList<ObjetPhysique> getObjets() {
		return this.objets;
	}
	/**
	 * Supprime un objet de la liste d'objets.
	 * @param objet L'objet � supprimer.
	 */
	public void supprimerObjet(ObjetPhysique objet) {
		objetsASupprimer.add(objet);
		if(!ecouteur.animationEnCours()){
			effectuerSuppression();
		}
	}
	/**
	 * Supprime les objets qui sont dans la liste d'objets � supprimer.
	 */
	private void effectuerSuppression(){
		ArrayList<Ressort> ressortsASupprimer=new ArrayList<Ressort>();
		if(!objetsASupprimer.isEmpty()){
			normales.clear();
			contacts.clear();
		}
		for(int j=0;j<objetsASupprimer.size();j++){
			ObjetPhysique objet=objetsASupprimer.get(j);
			if(objet != null) {
				if(objet.getTypeObjet() == TypeObjet.CORPS) {
					Corps c=(Corps)objet;
					for(int i=0;i<ressorts.size();i++){
						Ressort r=ressorts.get(i);
						if((r.getCorps1() !=null && r.getCorps1().equals(c)) || (r.getCorps2() != null && r.getCorps2().equals(c))){
							ressortsASupprimer.add(r);
						}
					}
					corps.remove(c);
				}else if(objet.getTypeObjet() == TypeObjet.EXPLOSION) {
					explosions.remove((Explosion)objet);
				}else if(objet.getTypeObjet() == TypeObjet.ZONE_DE_GRAVITE){
					zones.remove((ZoneDeGravite)objet);
				}else if(objet.getTypeObjet() == TypeObjet.RESSORT){
					ressorts.remove((Ressort)objet);
				}
				objets.remove(objet);
				while(!ressortsASupprimer.isEmpty()){
					Ressort r=ressortsASupprimer.get(ressortsASupprimer.size()-1);
					ressortsASupprimer.remove(r);
					ressorts.remove(r);
					objets.remove(r);
				}
			}
			ressortsASupprimer.clear();
		}
		objetsASupprimer.clear();
	}
	/**
	 * Supprime un objet de la liste d'objets.
	 * @param index L'index de l'objet � supprimer.
	 */
	public void supprimerObjet(int index) {
		if(index < objets.size()) {
			ObjetPhysique objet = objets.get(index);
			supprimerObjet(objet);
		}
	}
	/**
	 * Retourne une rectangle repr�sentant les limites de la sc�ne.
	 * @return Rectangle repr�sentant les limites de la sc�ne.
	 */
	public Rectangle2D.Double getBounds() {
		return new Rectangle2D.Double(-LARGEUR / 2.0, -HAUTEUR / 2.0, LARGEUR, HAUTEUR);
	}
	/**
	 * 
	 * @author Jeremy Gillespie-Cloutier
	 * Classe permettant d'enregistrer une paires de corps impliqu�s dans une collision.
	 */
	public class Paire implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 5068551704240823755L;
		private InfoCollision info;
		private int indice1;
		private int indice2;
		private int indiceLimite = -1;
		/**
		 * Initialise la paire de corps.
		 * @param indice1 L'indice du premier corps.
		 * @param indice2 L'indice du second corps.
		 * @param info L'information relative � la collision.
		 * @param indiceLimite l Indice utilis� lorsque l'on v�rifie les collisions avec les bordures de la sc�ne
		 * plut�t qu'avec les autres corps.
		 */
		public Paire(int indice1, int indice2, InfoCollision info, int indiceLimite) {
			this.indiceLimite = indiceLimite;
			this.indice1 = indice1;
			this.indice2 = indice2;
			this.info = info;
		}
		/**
		 * Retourne l'indice du premier corps.
		 * @return L'indice du premier corps.
		 */
		public int getIndice1() {
			return this.indice1;
		}
		/**
		 * Retourne l'indice du second corps.
		 * @return L'indice du second corps.
		 */
		public int getIndice2() {
			return this.indice2;
		}
		/**
		 * Retourne l'indice limite (pour les bordures de la sc�ne).
		 * @return L,indice limite.
		 */
		public int getIndiceLimite() {
			return this.indiceLimite;
		}
		/**
		 * Retourne l'information relative � la collision entre les deux corps.
		 * @return L'information relative � la collision entre les deux corps.
		 */
		public InfoCollision getInfoCollision() {
			return this.info;
		}
	}
	/**
	 * Change l'emplacement de sauvegarde de la sc�ne.
	 * @param location L'emplacement de sauvegarde de la sc�ne.
	 */
	public void setLocationFichier(String location) {
		this.locationFichier = location;
	}
	/**
	 * Retourne l'emplacement de sauvegarde de la sc�ne.
	 * @return L'emplacement de sauvegarde de la sc�ne.
	 */
	public String getLocationFichier() {
		return this.locationFichier;
	}
	/**
	 * Retourne la gravit� de la sc�ne.
	 * @return La gravit� de la sc�ne.
	 */
	public Vecteur getGravite() {
		return this.gravite;
	}
	/**
	 * Change la gravit� de la sc�ne.
	 * @param gravite La gravit� de la sc�ne.
	 */
	public void setGravite(Vecteur gravite) {
		this.gravite = gravite;
	}
	/**
	 * Retourne la liste des noms des objets de la sc�ne.
	 * @return La liste des noms des objets de la sc�ne.
	 */
	public DefaultListModel<String> getListeObjets() {
		DefaultListModel<String> liste = new DefaultListModel<String>();
		for (int i = 0; i < objets.size(); i++) {
			liste.addElement(objets.get(i).getNomFamilier(i + 1));
		}
		return liste;
	}
	/**
	 * Retourne le nombre d'objets contenus dans la sc�ne.
	 * @return Le nombre d'objets contenus dans la sc�ne.
	 */
	public int getNombreObjets() {
		return objets.size();
	}
	/**
	 * Retourne l'objet � l'index donn�.
	 * @param index l'index de l'objet.
	 * @return L'objet � l'index donn�.
	 */
	public ObjetPhysique getObjet(int index) {
		if(index < objets.size()) {
			return objets.get(index);
		} else {
			return null;
		}
	}
	/**
	 * Retourne l'index d'un objet donn�e.
	 * @param objet L'objet dont on veut l'index.
	 * @return L'index de l'objet.
	 */
	public int getIndexObjet(ObjetPhysique objet) {
		for(int i = 0 ; i < objets.size() ; i++) {
			if(objets.get(i) == objet) {
				return i;
			}
		}
		
		return -1;
	}
	/**
	 * Retourne le mat�riau de la sc�ne.
	 * @return Le mat�riau de la sc�ne.
	 */
	public TypeMateriau getMateriau(){
		return this.materiau;
	}
	/**
	 * Change le mat�riau de la sc�ne.
	 * @param materiau Le mat�riau de la sc�ne.
	 */
	public void setMateriau(TypeMateriau materiau){
		this.materiau=materiau;
	}
	/**
	 * D�termine si la sc�ne est en mode "vue de haut"
	 * @return Bool�en indiquant si la sc�ne est en mode "vue de haut"
	 */
	public boolean getVueDeHaut(){
		return this.vueDeHaut;
	}
	/**
	 * Active ou d�sactive le mode "vue de haut"
	 * @param vueDeHaut Bool�en indiquant si le mode "vue de haut" doit �tre activ�.
	 */
	public void setVueDeHaut(boolean vueDeHaut){
		this.vueDeHaut=vueDeHaut;
	}
}
