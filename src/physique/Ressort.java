package physique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.io.Serializable;

import enums.TypeObjet;
import maths.Vecteur;
import moteur.ObjetPhysique;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe repr�sentant un ressort qui applique un force aux objets qui y sont attach�s
 */
public class Ressort extends ObjetPhysique implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3979700271367622573L;
	private Vecteur ancrage;
	private double longueur;
	private double constante;
	private double nombre;
	private final double ECART=0.02;
	private final double BORDURE=0.2;
	private final double COEFFICIENT=0.3;
	private Corps corps1;
	private Corps corps2;
	private int indice1=-1;
	private int indice2=-1;
	/**
	 * Constructeur de ressort.
	 * @param longueur La longueur du ressort.
	 * @param constante La constante de rappel du ressort.
	 * @param corps1 Le corps � la premi�re extr�mit� du ressort.
	 * @param corps2 Le corps � la seconde extr�mit� du ressort.
	 * @param ancrage Le point d'ancrage du ressort sur le mur.
	 */
	public Ressort(double longueur, double constante, Corps corps1, Corps corps2, Vecteur ancrage){
		this.corps1=corps1;
		this.corps2=corps2;
		this.ancrage=ancrage;
		this.longueur=longueur;
		this.constante=constante;
		nombre=Math.floor(longueur/ECART);
	}
	/**
	 * Constructeur de ressort.
	 * @param r Le ressort � copier.
	 */
	public Ressort(Ressort r){
		this.corps1=(Corps) r.getCorps1();
		this.corps2=(Corps) r.getCorps2();
		this.ancrage=new Vecteur(r.getAncrage());
		this.longueur=r.getLongueur();
		this.constante=r.getConstante();
		this.indice1=r.getIndice1();
		this.indice2=r.getIndice2();
		nombre=Math.floor(this.longueur/ECART);
	}
	/**
	 * D�termine la force appliqu�e sur chacun des corps du ressort.
	 */
	public void setForces(){
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		Vecteur direction=pos.sub(corps1.getPosition()).unitaire();
		Vecteur equilibre=direction.mul(longueur);
		Vecteur etirement=pos.sub(corps1.getPosition()).sub(equilibre);
		if(!corps1.enDeplacement() && !corps1.getFixe())
			corps1.setForce(corps1.getForce().add(etirement.mul(constante).sub(corps1.getVitesse().mul(COEFFICIENT))));
		if(corps2!=null && !corps2.enDeplacement() && !corps2.getFixe())
			corps2.setForce(corps2.getForce().sub(etirement.mul(constante).sub(corps2.getVitesse().mul(COEFFICIENT))));
	}
	/**
	 * Retourne la longueur du ressort.
	 * @return La longueur du ressort.
	 */
	public double getLongueur(){
		return this.longueur;
	}
	/**
	 * Retourne la constante de rappel du ressort.
	 * @return La constante de rappel du ressort.
	 */
	public double getConstante(){
		return this.constante;
	}
	/**
	 * Retourne le premier corps attach� au ressort.
	 * @return Le premier corps attach� au ressort.
	 */
	public Corps getCorps1(){
		return this.corps1;
	}
	/**
	 * Retourne le second corps attach� au ressort.
	 * @return Le second corps attach� au ressort.
	 */
	public Corps getCorps2(){
		return this.corps2;
	}
	/**
	 * Change le premier corps attach� au ressort.
	 * @param corps1 Le premier corps attach� au ressort.
	 */
	public void setCorps1(Corps corps1){
		this.corps1=corps1;
	}
	/**
	 * Change le second corps attach� au ressort.
	 * @param corps2 Le second corps attach� au ressort.
	 */
	public void setCorps2(Corps corps2){
		this.corps2=corps2;
	}
	/**
	 * Retourne le point d'ancrage du ressort.
	 * @return Le point d'ancrage du ressort.
	 */
	public Vecteur getAncrage(){
		return this.ancrage;
	}
	/**
	 * Retourne la position au centre du ressort.
	 * @return La position au centre du ressort.
	 */
	public Vecteur getPosition(){
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		Vecteur direction=pos.sub(corps1.getPosition());
		return corps1.getPosition().add(direction.div(2));
	}
	public void dessinerScientifique(Graphics2D g2d, AffineTransform mat) {
		
	}
	/**
	 * Dessine le ressort.
	 */
	public void dessiner(Graphics2D g2d, AffineTransform mat, double scale) {
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(this.getSelectionne()?Color.ORANGE:Color.BLACK);
		g2d.setStroke(new BasicStroke(2.0f));
		int cote=1;
		Vecteur direction=corps1.getPosition().sub(pos);
		double ecart=direction.longueur()/nombre;
		Vecteur partie=corps1.getPosition().sub(pos).unitaire().mul(ecart);
		Vecteur perp=partie.perpendiculaire().unitaire().mul(BORDURE);
		for(int i=1;i<=nombre;i++){
			Vecteur p1=i==1?pos:pos.add(partie.mul(i-1).add(perp.mul(cote*-1)));
			Vecteur p2=i==nombre?corps1.getPosition():pos.add(partie.mul(i).add(perp.mul(cote)));
			g2d.draw(mat.createTransformedShape(new Line2D.Double(p1.getX(), p1.getY(), p2.getX(), p2.getY())));
			cote*=-1;
		}
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	public TypeObjet getTypeObjet() {
		return TypeObjet.RESSORT;
	}
	public ObjetPhysique copier() {
		return new Ressort(this);
	}
	@Override
	public boolean contient(Vecteur point) {
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		Vecteur axe=pos.sub(corps1.getPosition());
		Vecteur perp=axe.perpendiculaire().unitaire().mul(BORDURE);
		Path2D.Double path=new Path2D.Double();
		Vecteur p1=corps1.getPosition().add(perp);
		Vecteur p2=p1.add(axe);
		Vecteur p3=p2.sub(perp.mul(2));
		Vecteur p4=p3.sub(axe);
		path.moveTo(p1.getX(), p1.getY());
		path.lineTo(p2.getX(), p2.getY());
		path.lineTo(p3.getX(), p3.getY());
		path.lineTo(p4.getX(), p4.getY());
		path.closePath();
		Area aire=new Area(path);
		return aire.contains(point.getX(), point.getY()) && !corps1.contient(point) && ((corps2 == null) || (corps2!=null && !corps2.contient(point)));
	}
	@Override
	public void setPosition(Vecteur position) {
		// TODO Auto-generated method stub
		
	}
	public String getNomFamilier(int numero) {
		return "Ressort "+numero;
	}
	/**
	 * Change la constante de rappel du ressort.
	 * @param constante La constante de rappel du ressort.
	 */
	public void setConstante(double constante){
		this.constante=constante;
	}
	/**
	 * Change la longueur du ressort
	 * @param longueur La longueur du ressort.
	 */
	public void setLongueur(double longueur){
		this.longueur=longueur;
		nombre=Math.floor(this.longueur/ECART);
	}
	@Override
	public boolean contient(Area aire) {
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		Vecteur axe=pos.sub(corps1.getPosition());
		Vecteur perp=axe.perpendiculaire().unitaire().mul(BORDURE);
		Path2D.Double path=new Path2D.Double();
		Vecteur p1=corps1.getPosition().add(perp);
		Vecteur p2=p1.add(axe);
		Vecteur p3=p2.sub(perp.mul(2));
		Vecteur p4=p3.sub(axe);
		path.moveTo(p1.getX(), p1.getY());
		path.lineTo(p2.getX(), p2.getY());
		path.lineTo(p3.getX(), p3.getY());
		path.lineTo(p4.getX(), p4.getY());
		path.closePath();
		Area inter=new Area(aire);
		inter.intersect(new Area(path));
		return !inter.isEmpty();
	}
	@Override
	public Vecteur getMin() {
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		return new Vecteur(pos.getX()<corps1.getPosition().getX()?pos.getX():corps1.getPosition().getX(), pos.getY()<corps1.getPosition().getY()?pos.getY():corps1.getPosition().getY());
	}
	@Override
	public Vecteur getMax() {
		Vecteur pos=corps2==null?ancrage:corps2.getPosition();
		return new Vecteur(pos.getX()>corps1.getPosition().getX()?pos.getX():corps1.getPosition().getX(), pos.getY()>corps1.getPosition().getY()?pos.getY():corps1.getPosition().getY());
	}
	/**
	 * Change l'indice du premier corps du ressort.
	 * @param indice1 L'indice du premier corps du ressort.
	 */
	public void setIndice1(int indice1){
		this.indice1=indice1;
	}
	/**
	 * Retourne l'indice du premier corps du ressort.
	 * @return L'indice du premier corps du ressort.
	 */
	public int getIndice1(){
		return this.indice1;
	}
	/**
	 * Change l'indice du second corps du ressort.
	 * @param indice2 L'indice du second corps du ressort.
	 */
	public void setIndice2(int indice2){
		this.indice2=indice2;
	}
	/**
	 * Retourne l'indice du second corps du ressort.
	 * @return L'indice du second corps du ressort.
	 */
	public int getIndice2(){
		return this.indice2;
	}
}
