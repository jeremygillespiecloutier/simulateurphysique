package physique;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;

import maths.Vecteur;
import moteur.ObjetPhysique;
import moteur.Vue;
import enums.TypeObjet;

/**
 * @author Francis Labelle
 * Classe servant � simuler une r�gion dans laquelle la gravit� est diff�rente de celle de la sc�ne.
 */
public class ZoneDeGravite extends ObjetPhysique implements Serializable {
	private static final long serialVersionUID = -6846047556187254963L;

	private static final Color COULEUR_PAR_DEFAUT = new Color(200, 0, 0, 100);
	private static final Color COULEUR_SELECTION_DEFAUT = new Color(9, 23, 227, 100);
	private transient Area aire;
	private Color couleur = COULEUR_PAR_DEFAUT;
	private Color couleurSelection = COULEUR_SELECTION_DEFAUT;
	private Vecteur gravite = new Vecteur(0, -10);
	private Vecteur position = new Vecteur();
	private Vecteur positionInitiale = new Vecteur();
	private Path2D.Double path;
	private AffineTransform copie;

	/**
	 * Cree une zone de gravit�.
	 * @param points La liste de points repr�sentant le contour de la zone de gravit�.
	 */
	public ZoneDeGravite(ArrayList<Vecteur> points) {
		if(points.size() > 0) {
			path = new Path2D.Double();
			path.moveTo(points.get(0).getX(), points.get(0).getY());

			for(Vecteur point : points) {
				path.lineTo(point.getX(), point.getY());
			}

			path.closePath();
			aire = new Area(path);
			Rectangle2D rect = aire.getBounds2D();
			positionInitiale = new Vecteur(rect.getCenterX(), rect.getCenterY());
			setPosition(new Vecteur(positionInitiale));
		} else {
			aire = new Area();
		}
	}

	/**
	 * Cr�e une zone de gravit�.
	 * @param path L'objet Path2D.Double repr�sentant le contour de la zone de gravit�.
	 */
	public ZoneDeGravite(Path2D.Double path) {
		this.path = path;
		aire = new Area(path);
		Rectangle2D rect = aire.getBounds2D();
		positionInitiale = new Vecteur(rect.getCenterX(), rect.getCenterY());
		setPosition(new Vecteur(positionInitiale));
	}

	/**
	 * Cr�e une zone de gravit� � partir d'une autre zone.
	 * @param zone La zone de gravit� � copier.
	 */
	public ZoneDeGravite(ZoneDeGravite zone) {
		this.path = zone.getPath();
		aire = new Area(zone.getAire());
		couleur = zone.couleur;
		gravite = zone.gravite;
		position = zone.position;
		Rectangle2D rect = aire.getBounds2D();
		positionInitiale = new Vecteur(rect.getCenterX(), rect.getCenterY());
		setPosition(new Vecteur(zone.getPosition()));
		setRotation(zone.getRotation());
	}

	/**
	 * D�termine si la zone de gravit� est vide.
	 * @return Bool�en indiquant si la zone est vide.
	 */
	public boolean estVide() {
		return aire.isEmpty();
	}

	/**
	 * D�termine si la zone de gravit� contient un point.
	 * @param point Le vecteur � tester.
	 * @return Bool�en indiquant si la zone contient le point.
	 */
	public boolean contient(Vecteur point) {
		copie = new AffineTransform();
		Vecteur ecart = position.sub(positionInitiale);
		copie.translate(ecart.getX(), ecart.getY());
		copie.rotate(getRotation(), positionInitiale.getX(), positionInitiale.getY());
		return aire.createTransformedArea(copie).contains(point.getX(), point.getY());
	}

	/**
	 * Dessine la zone de gravit�.
	 * @param g2d L'objet graphique sur lequel la zone est dessin�e.
	 * @param mat La matrice de transformation � appliquer sur le dessin.
	 */
	@Override
	public void dessiner(Graphics2D g2d, AffineTransform mat, double scale) {
		if(aire == null)
			aire = new Area(path);
		Color couleurPrecedente = g2d.getColor();
		Vecteur ecart = position.sub(positionInitiale);
		mat.translate(ecart.getX(), ecart.getY());
		mat.rotate(getRotation(), positionInitiale.getX(), positionInitiale.getY());
		g2d.setColor(getSelectionne() ? couleurSelection : couleur);
		if(!Vue.getPerformance()) {
			g2d.fill(mat.createTransformedShape(aire));
		} else {
			g2d.draw(mat.createTransformedShape(aire));
		}
		g2d.setColor(couleurPrecedente);
		mat.rotate(-getRotation(), positionInitiale.getX(), positionInitiale.getY());
		mat.translate(-ecart.getX(), -ecart.getY());
	}

	/**
	 * Dessine le mode scientifique de la zone de gravit�.
	 * @param g2d L'objet graphique sur lequel la zone est dessin�e.
	 * @param mat La matrice de transformation � appliquer sur le dessin. 
	 */
	@Override
	public void dessinerScientifique(Graphics2D g2d, AffineTransform mat) {

	}

	/**
	 * Retourne la couleur de la zone de gravit�.
	 * @return La couleur de la zone de gravit�.
	 */
	public Color getCouleur() {
		return couleur;
	}

	/**
	 * Change la couleur de la zone de gravit�.
	 * @param couleur La couleur de la zone de gravit�.
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}

	/**
	 * Change la couleur de la zone de gravit� lorsque s�lectionn�e.
	 * @param couleurSelection : La couleur de la zone de gravit� lorsque s�lectionn�e.
	 */
	public void setCouleurSelection(Color couleurSelection) {
		this.couleurSelection = couleurSelection;
	}

	/**
	 * Retourne la couleur de la zone de gravit� lorsque s�lectionn�e.
	 * @return La couleur de la zone de gravit� lorsque s�lectionn�e.
	 */
	public Color getCouleurSelection() {
		return this.couleurSelection;
	}

	/**
	 * Retourne la gravit� de la zone.
	 * @return La gravit� de la zone.
	 */
	public Vecteur getGravite() {
		return gravite;
	}

	/**
	 * Change la gravit� de la zone.
	 * @param gravite La gravit� de la zone.
	 */
	public void setGravite(Vecteur gravite) {
		this.gravite = gravite;
	}

	/**
	 * Retourne le type d'objet (ZONE_DE_GRAVITE).
	 * @return Le type d'objet (ZONE_DE_GRAVITE).
	 */
	@Override
	public TypeObjet getTypeObjet() {
		return TypeObjet.ZONE_DE_GRAVITE;
	}

	/**
	 * Retourne la position de la zone de gravit�.
	 * @return La position de la zone de gravit�.
	 */
	@Override
	public Vecteur getPosition() {
		return position;
	}

	/**
	 * Change la position de la zone de gravit�.
	 * @param position La position de la zone de gravit�.
	 */
	@Override
	public void setPosition(Vecteur position) {
		this.position = position;
	}

	/**
	 * Copie la zone de gravit�.
	 * @return Copie de la zone de gravit�.
	 */
	@Override
	public ObjetPhysique copier() {
		return new ZoneDeGravite(this);
	}

	/**
	 * Retourne le nom de la zone de gravit� en fonction d'un indice pour son affichage dans la liste d'objets.
	 * @param numero L'indice de l'objet.
	 * @return Le nom de la zone de gravit�.
	 */
	@Override
	public String getNomFamilier(int numero) {
		return "Zone de gravit� " + numero;
	}

	/**
	 * Retourne l'aire de la zone de gravit�.
	 * @return L'aire de la zone de gravit�.
	 */
	public Area getAire() {
		return this.aire;
	}

	/**
	 * M�thode servant � tester si la zone de gravit� contient une aire.
	 * @param aire : Aire � tester.
	 * @return Vrai si l'aire est dans la zone de gravit�.
	 */
	@Override
	public boolean contient(Area aire) {
		Area inter = new Area(aire);
		Vecteur ecart = position.sub(positionInitiale);
		copie = new AffineTransform();
		copie.translate(ecart.getX(), ecart.getY());
		copie.rotate(getRotation(), positionInitiale.getX(), positionInitiale.getY());
		inter.intersect(this.aire.createTransformedArea(copie));
		return !inter.isEmpty();
	}

	/**
	 * @return Le point en bas � gauche du rectangle englobant la zone de gravit�.
	 */
	@Override
	public Vecteur getMin() {
		copie = new AffineTransform();
		Vecteur ecart = position.sub(positionInitiale);
		copie.translate(ecart.getX(), ecart.getY());
		copie.rotate(getRotation(), positionInitiale.getX(), positionInitiale.getY());
		Rectangle2D rect = aire.createTransformedArea(copie).getBounds2D();
		return new Vecteur(rect.getMinX(), rect.getMinY());
	}
	
	/**
	 * @return Le point en haut � droite du rectangle englobant la zone de gravit�.
	 */
	@Override
	public Vecteur getMax() {
		copie = new AffineTransform();
		Vecteur ecart = position.sub(positionInitiale);
		copie.translate(ecart.getX(), ecart.getY());
		copie.rotate(getRotation(), positionInitiale.getX(), positionInitiale.getY());
		Rectangle2D rect = aire.createTransformedArea(copie).getBounds2D();
		return new Vecteur(rect.getMaxX(), rect.getMaxY());
	}

	/**
	 * Retourne le Path2D de la zone.
	 * @return Le Path2D.
	 */
	public Path2D.Double getPath() {
		return this.path;
	}
}
