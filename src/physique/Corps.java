package physique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.Serializable;

import materiaux.Materiau;
import materiaux.TypeMateriau;
import maths.Vecteur;
import moteur.ObjetPhysique;
import moteur.Vue;
import dessinable.VecteurDessinable;
import enums.TypeObjet;
import fonctions.Fonction;
import formes.Cercle;
import formes.Forme;
import formes.Polygone;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe repr�sentant un corps physique avec des propri�t�s telles qu'une forme, masse, densit� et mat�riau. Les corps
 * sont utilis�s dans les collisions des objets contenus dans les sc�nes de l'application.
 */
public class Corps extends ObjetPhysique implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4598351515363666941L;
	private Forme forme;
	private double masse = 0;
	private double masseInverse=0;
	private Vecteur vitesse = new Vecteur(0, 0);
	private Vecteur acceleration = new Vecteur(0, 0);
	private Vecteur gravite = new Vecteur(0, 0);
	private boolean fixe = false;
	private TypeMateriau materiau = TypeMateriau.bois;
	private boolean suivi = false;
	private double inertie = 0;
	private double inertieInverse=0;
	private double vitesseAngulaire = 0;
	private double densite = 0;
	private Vecteur force=new Vecteur(0,0);
	private Fonction fonction=null;
	private Vecteur positionInitiale;
	/**
	 * Constructeur d'un corps � partir d'une forme
	 * @param forme La forme � copier
	 */
	public Corps(Forme forme) {
		this.forme = forme;
		this.positionInitiale=forme.getPosition();
		calculerProprietes();
	}
	/**
	 * Constructeur de corp � partir d'un autre corps
	 * @param c Le corps � copier
	 */
	public Corps(Corps c) {
		if(c.getForme().getClass() == Polygone.class) {
			this.forme = new Polygone((Polygone) c.getForme());
		} else if(c.getForme().getClass() == Cercle.class) {
			this.forme = new Cercle((Cercle) c.getForme());
		}
		this.materiau = c.getMateriau();
		this.masse = c.getMasse();
		this.fixe = c.getFixe();
		if(fixe)
			this.vitesseAngulaire = c.getVitesseAngulaire();
		this.positionInitiale=c.getPositionInitiale();
		if(c.getFonction()!=null)
			this.fonction=c.getFonction().copie();
		calculerProprietes();
	}
	/**
	 * Dessine le corps. S'il est s�lectionn�, met son contour en gras. Si le mode performance est d�sactiv�, 
	 * dessin �galement la texture du corps.
	 * @param g2d L'objet graphique sur lequel on dessine.
	 * @param matMC La matrice de transformation.
	 */
	public void dessiner(Graphics2D g2d, AffineTransform matMC, double scale) {
		Stroke trait = g2d.getStroke();
		Paint paint=g2d.getPaint();
		Color couleurInitiale=g2d.getColor();
		BufferedImage img = Materiau.getTexture(materiau);
		double largeurImage = img.getWidth()*scale;
		double hauteurImage = img.getHeight()*scale;
		double x = getPosition().getX()*matMC.getScaleX();
		double y = getPosition().getY()*matMC.getScaleY();
		Shape s=forme.getShapeInitiale(matMC);
		double diffX=getPosition().getX()-forme.getPositionInitiale().getX();
		double diffY=getPosition().getY()-forme.getPositionInitiale().getY();
		g2d.rotate(-this.getOrientation(), x+matMC.getTranslateX(), y+matMC.getTranslateY());
		g2d.translate(diffX*matMC.getScaleX(), diffY*matMC.getScaleY());
		if(!Vue.getPerformance()){
			TexturePaint p = new TexturePaint(img, new Rectangle((int)(matMC.getTranslateX()+positionInitiale.getX()*matMC.getScaleX()-largeurImage/2.0), (int)(matMC.getTranslateY()+positionInitiale.getY()*matMC.getScaleY()-hauteurImage/2.0), (int)(largeurImage), (int)(hauteurImage)));
			g2d.setPaint(p);
			g2d.fill(s);
		}else{
			g2d.setColor(Color.BLACK);
			g2d.draw(s);
		}
		if(suivi){
			g2d.setColor(new Color(72, 252, 23, 75));
			g2d.fill(s);
		}
		if(getSelectionne()) {
			g2d.setStroke(new BasicStroke(2f));
			g2d.setColor(Color.black);
			g2d.draw(s);
		}
		g2d.translate(-diffX*matMC.getScaleX(), -diffY*matMC.getScaleY());
		g2d.rotate(this.getOrientation(), x+matMC.getTranslateX(), y+matMC.getTranslateY());
		g2d.setStroke(trait);
		g2d.setPaint(paint);
		g2d.setColor(couleurInitiale);
	}
	/**
	 * Dessinne le mode scientifique du corps (Vecteur vitesse)
	 * @param g2d L'objet graphique sur leqeul on dessine.
	 * @param mat La matrice de transformation.
	 */
	public void dessinerScientifique(Graphics2D g2d, AffineTransform mat) {
		VecteurDessinable vitesseDessinable;
		vitesseDessinable = new VecteurDessinable(getVitesse(), forme.getPosition());
		vitesseDessinable.setCouleur(Color.DARK_GRAY);
		vitesseDessinable.setTailleTrait(2.0f);
		vitesseDessinable.setEchelle(0.2);
		vitesseDessinable.dessiner(g2d, mat, 0);
	}
	/**
	 * Calcule les propri�t�s du corps qui sont li�es � son mat�riau.
	 */
	private void calculerProprietes() {
		densite = Materiau.getDensite(materiau);
		masse = forme.getAire() * densite;
		inertie = forme.calculerInertie(masse, densite);
		masseInverse=1.0/masse;
		inertieInverse=1.0/inertie;
	}
	/**
	 * Attribue une valeur d'acc�l�ration et fonction des forces appliqu�es.
	 * @param deltaT L'intervalle de temps sur lequel les forces sont appliqu�es.
	 * @param vueDeHaut Bool�en indiquant si la gravit� doit agir comme frottement.
	 */
	public void actualiserForces(double deltaT, boolean vueDeHaut) {
		if(!fixe) {
			vitesse = vitesse.add(!vueDeHaut?acceleration.mul(deltaT):new Vecteur(0,0)).add(force.mul(deltaT));
			if(vueDeHaut){
				double vInit=vitesse.longueur();
				Vecteur axe=vitesse.unitaire();
				if(!axe.isNaN()){
					Vecteur frottement=axe.mul(deltaT*acceleration.longueur());
					if(frottement.longueur()>vInit)
						frottement=axe.mul(vitesse.longueur());
					vitesse=vitesse.sub(frottement);
					vitesseAngulaire*=(vitesse.longueur()/vInit);
				}
			}
		}
	}
	/**
	 * D�place le corps selon sa vitesse.
	 * @param deltaT L'intervalle de temps �coul�.
	 */
	public void actualiserVitesses(double deltaT) {
		forme.deplacer(vitesse.mul(deltaT));
		forme.tourner(vitesseAngulaire * deltaT);
	}
	/**
	 * D�termine si le corps contient un point (Vecteur).
	 * @param point Le vecteur � tester.
	 * @return Bool�en indiquant si le point est contenu dans le corps.
	 */
	public boolean contient(Vecteur point) {
		return forme.contient(point);
	}
	/**
	 * Change la position de l'objet.
	 * @param v Le vecteur repr�sentant la position de l'objet.
	 */
	public void setPosition(Vecteur v) {
		forme.setPosition(v);
	}
	/**
	 * Retourne la position de l'objet.
	 * @return La position de l'objet.
	 */
	public Vecteur getPosition() {
		return forme.getPosition();
	}
	/**
	 * Retourne la masse de l'objet.
	 * @return La masse de l'objet.
	 */
	public double getMasse() {
		return this.masse;
	}
	/**
	 * Retourne l'inverse de la masse, ou 0 si le corps est fixe.
	 * @return La masse inverse.
	 */
	public double getMasseInverse() {
		return fixe?0:masseInverse;
	}
	/**
	 * Retourne si le corps est fixe.
	 * @return Boolean repr�sentant si l'objet est fixe.
	 */
	public boolean getFixe() {
		return this.fixe;
	}
	/**
	 * Retourne la forme du corps
	 * @return La forme du corps.
	 */
	public Forme getForme() {
		return this.forme;
	}
	/**
	 * Change le mat�riau du corps.
	 * @param materiau Le nouveau mat�riau.
	 */
	public void setMateriau(TypeMateriau materiau) {
		this.materiau = materiau;
		calculerProprietes();
	}
	/**
	 * Retourne le mat�riau du corps.
	 * @return Le mat�riau du corps.
	 */
	public TypeMateriau getMateriau() {
		return this.materiau;
	}
	/**
	 * Attribue une valeur de v�rit� � la propri�t� fixe.
	 * @param fixe Bool�en repr�sentant si l'objet est fixe.
	 */
	public void setFixe(boolean fixe) {
		this.fixe = fixe;
		this.vitesse = new Vecteur(0, 0);
	}
	/**
	 * Retourne la vitesse du corps.
	 * @return La vitesse du corps.
	 */
	public Vecteur getVitesse() {
		if(fixe && fonction!=null){
			return fonction.getVelocite();
		}
		return this.vitesse;
	}
	/**
	 * Change la vitesse du corps.
	 * @param vitesse La nouvelle vitesse du Corps.
	 */
	public void setVitesse(Vecteur vitesse) {
		this.vitesse = vitesse;
	}
	/**
	 * Tourne le corps d'un certain angle en radians.
	 * @param angle L'angle en radians.
	 */
	public void tourner(double angle, Vecteur centre) {
		super.tourner(angle, centre);
		forme.tourner(angle);
	}
	/**
	 * Retourne un bool�en repr�sentant si le corps est suivi.
	 * @return Bool�en repr�sentant si le corps est suivi.
	 */
	public boolean getSuivi() {
		return this.suivi;
	}
	/**
	 * Attribue une valeur bool�enne � la propri�t� suivi du corps.
	 * @param suivi Bool�en repr�sentant si le corps doit �tre suivi.
	 */
	public void setSuivi(boolean suivi) {
		this.suivi = suivi;
	}
	/**
	 * Retourne l'inertie du corps.
	 * @return L'inertie du corps.
	 */
	public double getInertie() {
		return this.inertie;
	}
	/** 
	 * Retourne l'inertie inverse du corps ou 0 s'il est fixe.
	 * @return L'inertie du corps.
	 */
	public double getInertieInverse() {
		return fixe?0:inertieInverse;
	}
	/**
	 * Retourne la vitesse angulaire du corps.
	 * @return La vitesse Angulaire du corps.
	 */
	public double getVitesseAngulaire() {
		return this.vitesseAngulaire;
	}
	/**
	 * Change la vitesse angulaire du corps.
	 * @param vitesseAngulaire La vitesse angulaire du corps.
	 */
	public void setVitesseAngulaire(double vitesseAngulaire) {
		this.vitesseAngulaire = vitesseAngulaire;
	}
	/**
	 * Applique une impulsion au corps qui change sa vitesse et sa vitesse angulaire.
	 * @param impulsion Le vecteur d'impulsion
	 * @param rayon Le Vecteur repr�sentant la distance entre le centre de masse et le point de contact de la collision.
	 */
	public void appliquerImpulsion(Vecteur impulsion, Vecteur rayon) {
		if(!fixe && !enDeplacement()) {
			vitesse = vitesse.add(impulsion.mul(getMasseInverse()));
			vitesseAngulaire += rayon.produitCroix(impulsion) * 1 / inertie;
		}
	}
	/**
	 * Retourne l'aire du corps.
	 * @return L'aire du corps.
	 */
	public double getAire() {
		return forme.getAire();
	}
	/**
	 * Retourne la densit� du corps.
	 * @return La densit� du corps.
	 */
	public double getDensite() {
		return densite;
	}
	/**
	 * Retourne le vecteur acc�l�ration du corps.
	 * @return L'acc�l�ration du corps.
	 */
	public Vecteur getAcceleration() {
		return this.acceleration;
	}
	/**
	 * Change l'acceleration du corps.
	 * @param acceleration l'acceleration du corps.
	 */
	public void setAcceleration(Vecteur acceleration){
		this.acceleration=acceleration;
	}
	/**
	 * Retourne l'orientation du corps.
	 * @return L'orientation du corps en radians.
	 */
	public double getOrientation() {
		return this.forme.getRotation();
	}
	/**
	 * Change la gravit� du corps.
	 * @param gravite La gravit� du corps.
	 */
	public void setGravite(Vecteur gravite) {
		this.gravite = gravite;
	}
	/**
	 * Retourne la gravit� du corps.
	 * @return La gravit� du corps.
	 */
	public Vecteur getGravite(){
		return this.gravite;
	}
	/**
	 * Change la force externe appliqu�e sur le corps.
	 * @param force La force appliqu�e sur le corps.
	 */
	public void setForce(Vecteur force) {
		this.force = force;
	}
	/**
	 * Retourne la force externe appliqu�e sur le corps.
	 * @return La force appliqu�e sur le corps.
	 */
	public Vecteur getForce(){
		return this.force;
	}
	/**
	 * Retourne le type d'objet (CORPS).
	 * @return Le type d'objet (CORPS).
	 */
	@Override
	public TypeObjet getTypeObjet() {
		return TypeObjet.CORPS;
	}
	/**
	 * M�thode appel�e quand l'utilisateur commence � "drag4 le corps. Met sa vitesse � 0.
	 */
	@Override
	public void debutDeplacement() {
		super.debutDeplacement();
		vitesse = new Vecteur(0, 0);
		force=new Vecteur(0,0);
		if(!fixe) {
			vitesseAngulaire = 0.0;
		}
	}
	/**
	 * Cr�e une copie du corps.
	 * @return La copie du corps.
	 */
	@Override
	public ObjetPhysique copier() {
		return new Corps(this);
	}
	/**
	 * Retourne le nom du corps en fonction d'un indice pour son affichage dans la liste d'objets.
	 * @param numero L'indice de l'objet.
	 * @return Le nom du corps.
	 */
	@Override
	public String getNomFamilier(int numero) {
		if(forme.getClass() == Polygone.class) {
			return "Polygone " + numero;
		} else {
			return "Sph�re " + numero;
		}
	}
	/**
	 * Change la fonction du corps.
	 * @param fonction La fonction du corps.
	 */
	public void setFonction(Fonction fonction){
		this.fonction=fonction;
	}
	/**
	 * Retourne la fonction du corps.
	 * @return La fonction du corps.
	 */
	public Fonction getFonction(){
		return this.fonction;
	}
	@Override
	public boolean contient(Area aire) {
		return forme.contient(aire);
	}
	@Override
	public Vecteur getMin() {
		return forme.getMin();
	}
	@Override
	public Vecteur getMax() {
		return forme.getMax();
	}
	public Vecteur getPositionInitiale(){
		return positionInitiale;
	}
}
