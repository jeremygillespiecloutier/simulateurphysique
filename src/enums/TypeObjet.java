package enums;

/**
 * @author Francis Labelle
 * �num�re les diff�rents types d'objets physiques.
 */
public enum TypeObjet {
	CORPS, ZONE_DE_GRAVITE, EXPLOSION, RESSORT
}
