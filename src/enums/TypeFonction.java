package enums;
/**
 * 
 * @author Francis Labelle
 * Énumération du type de fonction pour les chemins.
 */
public enum TypeFonction {
	SINUS,
	COSINUS,
	LINEAIRE,
	CERCLE,
	PERSONNALISEE
}
