package enums;

/**
 * 
 * @author Francis Labelle
 * Énumération du type d'affichage de l'application.
 */
public enum TypeAffichage {
	AFFICHAGE_GRILLE, AFFICHAGE_SCIENTIFIQUE, AFFICHAGE_PERFORMANCE, AFFICHAGE_CHEMIN
}
