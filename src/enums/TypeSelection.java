package enums;

import java.io.Serializable;
/**
 * Énumération des différents types d'outils.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public enum TypeSelection implements Serializable {
	RECTANGLE, SPHERE, EXPLOSION, CRAYON, ROTATION, ZONE, LIGNE, SOURIS, RESSORT, GROUPE
}
