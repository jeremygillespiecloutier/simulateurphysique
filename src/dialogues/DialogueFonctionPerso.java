package dialogues;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

import composants.LimiteTexte;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer une fonction personnalis�e.
 */
public class DialogueFonctionPerso extends JDialog {
	private static final long serialVersionUID = -6556982797460022412L;
	private final JPanel contentPanel = new JPanel();
	private JTextField txtNom;
	private JSpinner spnVitesse;
	private boolean confirmer=false;
	private JCheckBox chckbxAllerRetour;
	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		try {
			DialogueFonctionPerso dialog = new DialogueFonctionPerso();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DialogueFonctionPerso() {
		setResizable(false);
		setTitle("Options fonction");
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 316, 231);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton btnOk = new JButton("Ok");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					confirmer=true;
					setVisible(false);
				}
			});
			btnOk.setBounds(32, 143, 120, 23);
			contentPanel.add(btnOk);
			btnOk.setActionCommand("OK");
			getRootPane().setDefaultButton(btnOk);
		}
		{
			JButton btnAnnuler = new JButton("Annuler");
			btnAnnuler.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}
			});
			btnAnnuler.setBounds(171, 143, 119, 23);
			contentPanel.add(btnAnnuler);
			btnAnnuler.setActionCommand("Cancel");
		}
		{
			JLabel lblNom = new JLabel("Nom:");
			lblNom.setBounds(31, 49, 46, 14);
			contentPanel.add(lblNom);
		}
		{
			JLabel lblVitesse = new JLabel("Vitesse (m/s):");
			lblVitesse.setBounds(31, 75, 85, 14);
			contentPanel.add(lblVitesse);
		}
		{
			txtNom = new JTextField();
			txtNom.setBounds(128, 46, 107, 20);
			contentPanel.add(txtNom);
			txtNom.setColumns(10);
			txtNom.setDocument(new LimiteTexte(5));
		}
		{
			spnVitesse = new JSpinner();
			spnVitesse.setModel(new SpinnerNumberModel(5.0, 1.0, 15.0, 1.0));
			spnVitesse.setBounds(126, 72, 109, 20);
			contentPanel.add(spnVitesse);
		}
		
		chckbxAllerRetour = new JCheckBox("Aller-Retour");
		chckbxAllerRetour.setBounds(32, 96, 97, 23);
		contentPanel.add(chckbxAllerRetour);
	}
	/**
	 * Retourne le nom de la fonction personnalis�e.
	 * @return Le nom de la fonction personnalis�e.
	 */
	public String getNom(){
		return txtNom.getText();
	}
	/**
	 * Retourne la vitesse de la fonction.
	 * @return La vitesse de la fonction.
	 */
	public double getVitesse(){
		return (double) spnVitesse.getValue();
	}
	/**
	 * Retourne un bool�en indiquant si la fonction doit changer de direction lorsqu'elle atteint la fin.
	 * @return Bool�en indiquant si la fonction doit changer de direction lorsqu'elle atteint la fin.
	 */
	public boolean getInversion(){
		return chckbxAllerRetour.isSelected();
	}
	/**
	 * Retourne un bool�en indiquant si le dialogue a �t� confirm�.
	 * @return Bool�en indiquant si le dialogue a �t� confirm�.
	 */
	public boolean getConfirmer(){
		return this.confirmer;
	}
}
