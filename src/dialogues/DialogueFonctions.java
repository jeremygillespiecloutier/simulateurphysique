package dialogues;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import composants.LimiteTexte;

import enums.TypeFonction;
import fonctions.Fonction;
import fonctions.FonctionCercle;
import fonctions.FonctionLineaire;
import fonctions.FonctionTrigo;

/**
 * Dialogue servant � cr�er et choisir des chemins (fonctions) pouvant �tre assign�s � des polygones.
 * @author Francis Labelle
 */
public class DialogueFonctions extends JDialog {
	private static final long serialVersionUID = 9074584764118906217L;
	private final JPanel contentPane = new JPanel();
	private final ButtonGroup btngrpTrigoBoutons = new ButtonGroup();
	private JPanel panNouvelleFonction;
	private JTabbedPane panOnglets;
	private JPanel panLineaire;
	private JSpinner spnLineairePente;
	private JSpinner spnLineaireLongueur;
	private JPanel panCercle;
	private JLabel lblCercleRayon;
	private JSpinner spnCercleRayon;
	private JPanel panTrigo;
	private JRadioButton rdbtnTrigoSin;
	private JRadioButton rdbtnTrigoCos;
	private JLabel lblTrigoAmplitude;
	private JSpinner spnTrigoAmplitude;
	private JPanel panFonctionExistante;
	private JScrollPane scrlpanExistante;
	private JList<Fonction> listeFonctions;
	private JButton btnAjouter;
	private JButton btnSupprimer;
	private JPanel btnpaneBoutons;
	private JButton btnOk;
	private JButton btnAnnuler;
	private DefaultListModel<Fonction> fonctions = new DefaultListModel<Fonction>();
	private EcouteurDialogueFonction ecouteur = null;
	private JLabel lblVitesseEnX;
	private JSpinner spnVitesse;
	private JPanel panel;
	private JSpinner spnCycles;
	private JSpinner spnPeriode;
	private JCheckBox chckbxInversion;
	private JLabel lblNom;
	private JTextField txtNom;
	private JCheckBox chckbcInverserSens;

	/**
	 * Constructeur pour la bo�te de dialogue.
	 */
	public DialogueFonctions() {
		setModal(true);
		setResizable(false);
		setTitle("Assigner un chemin");
		setBounds(100, 100, 542, 383);
		getContentPane().setLayout(new BorderLayout());
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPane, BorderLayout.CENTER);
		contentPane.setLayout(null);
		
		panNouvelleFonction = new JPanel();
		panNouvelleFonction.setBorder(new TitledBorder(null, "Nouvelle fonction", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panNouvelleFonction.setBounds(10, 11, 313, 227);
		contentPane.add(panNouvelleFonction);
		panNouvelleFonction.setLayout(null);
		
		{
			panOnglets = new JTabbedPane(JTabbedPane.TOP);
			panOnglets.setBounds(10, 21, 292, 163);
			panNouvelleFonction.add(panOnglets);
			{
				panLineaire = new JPanel();
				panOnglets.addTab("Lin\u00E9aire", null, panLineaire, null);
				panLineaire.setLayout(null);
				
				spnLineairePente = new JSpinner();
				spnLineairePente.setModel(new SpinnerNumberModel(1.0, -90.0, 90.0, 1.0));
				spnLineairePente.setBounds(128, 14, 75, 20);
				panLineaire.add(spnLineairePente);
				
				spnLineaireLongueur = new JSpinner();
				spnLineaireLongueur.setModel(new SpinnerNumberModel(1.0, 0.0, 10.0, 1.0));
				spnLineaireLongueur.setBounds(128, 45, 75, 20);
				panLineaire.add(spnLineaireLongueur);
				
				JLabel lblPente = new JLabel("Pente:");
				lblPente.setBounds(10, 17, 90, 14);
				panLineaire.add(lblPente);
				
				JLabel lblLongueur = new JLabel("Longueur (m):");
				lblLongueur.setBounds(10, 48, 90, 14);
				panLineaire.add(lblLongueur);
			}
			
			panCercle = new JPanel();
			panOnglets.addTab("Cercle", null, panCercle, null);
			panCercle.setLayout(null);
			
			lblCercleRayon = new JLabel("Rayon (m):");
			lblCercleRayon.setBounds(10, 38, 113, 14);
			panCercle.add(lblCercleRayon);
			
			spnCercleRayon = new JSpinner();
			spnCercleRayon.setModel(new SpinnerNumberModel(2.0, 1.0, 10.0, 1.0));
			spnCercleRayon.setBounds(133, 35, 68, 20);
			panCercle.add(spnCercleRayon);
			
			chckbcInverserSens = new JCheckBox("Inverser sens");
			chckbcInverserSens.setBounds(20, 59, 103, 23);
			panCercle.add(chckbcInverserSens);
			
			panTrigo = new JPanel();
			panOnglets.addTab("Trigonom\u00E9trique", null, panTrigo, null);
			panTrigo.setLayout(null);
			
			rdbtnTrigoSin = new JRadioButton("Sinus");
			rdbtnTrigoSin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					chckbxInversion.setEnabled(true);
				}
			});
			rdbtnTrigoSin.setSelected(true);
			btngrpTrigoBoutons.add(rdbtnTrigoSin);
			rdbtnTrigoSin.setBounds(6, 7, 109, 23);
			panTrigo.add(rdbtnTrigoSin);
			
			rdbtnTrigoCos = new JRadioButton("Cosinus");
			rdbtnTrigoCos.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					chckbxInversion.setEnabled(false);
				}
			});
			btngrpTrigoBoutons.add(rdbtnTrigoCos);
			rdbtnTrigoCos.setBounds(117, 7, 109, 23);
			panTrigo.add(rdbtnTrigoCos);
			
			lblTrigoAmplitude = new JLabel("Amplitude (m) :");
			lblTrigoAmplitude.setBounds(6, 34, 127, 14);
			panTrigo.add(lblTrigoAmplitude);
			
			spnTrigoAmplitude = new JSpinner();
			spnTrigoAmplitude.setModel(new SpinnerNumberModel(2.0, -10.0, 10.0, 1.0));
			spnTrigoAmplitude.setBounds(143, 31, 72, 20);
			panTrigo.add(spnTrigoAmplitude);
			
			JLabel lblPeriode = new JLabel("Longueur d'onde (m):");
			lblPeriode.setBounds(6, 54, 127, 14);
			panTrigo.add(lblPeriode);
			
			spnPeriode = new JSpinner();
			spnPeriode.setModel(new SpinnerNumberModel(2.0, 1.0, 10.0, 1.0));
			spnPeriode.setBounds(143, 51, 72, 20);
			panTrigo.add(spnPeriode);
			
			JLabel lblCycles = new JLabel("Nmb cycles:");
			lblCycles.setBounds(6, 76, 127, 14);
			panTrigo.add(lblCycles);
			
			spnCycles = new JSpinner();
			spnCycles.setModel(new SpinnerNumberModel(2, 1, 10, 1));
			spnCycles.setBounds(143, 73, 72, 20);
			panTrigo.add(spnCycles);
			
			chckbxInversion = new JCheckBox("Inversion");
			chckbxInversion.setBounds(82, 97, 97, 23);
			panTrigo.add(chckbxInversion);
		}
		
		btnAjouter = new JButton("Ajouter");
		btnAjouter.setBounds(10, 193, 292, 23);
		panNouvelleFonction.add(btnAjouter);
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Component ongletActuel = panOnglets.getSelectedComponent();
				Fonction fonction = null;
				double vitesse = (double)spnVitesse.getValue();
				
				if(ongletActuel == panLineaire) {
					double pente = (double)spnLineairePente.getValue();
					double longueur = (double)spnLineaireLongueur.getValue();
					fonction = new FonctionLineaire(pente, longueur, vitesse, txtNom.getText());
					
				} else if(ongletActuel == panCercle) {
					double rayon = (double)spnCercleRayon.getValue();
					fonction = new FonctionCercle(rayon, chckbcInverserSens.isSelected()?-vitesse:vitesse, txtNom.getText());
					
				} else if(ongletActuel == panTrigo) {
					double amplitude = (double) spnTrigoAmplitude.getValue();
					double periode = (double) spnPeriode.getValue();
					int nbCycles = (int) spnCycles.getValue();
					boolean inversion = (boolean) (chckbxInversion.isEnabled() ? chckbxInversion.isSelected() : false);
					fonction = new FonctionTrigo(amplitude, periode, nbCycles, inversion, vitesse, rdbtnTrigoSin.isSelected() ? TypeFonction.SINUS : TypeFonction.COSINUS, txtNom.getText());
				}
				
				ajouterFonctionListe(fonction);
			}
		});
		
		panFonctionExistante = new JPanel();
		panFonctionExistante.setBorder(new TitledBorder(null, "Fonction existante", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panFonctionExistante.setBounds(333, 11, 179, 227);
		contentPane.add(panFonctionExistante);
		panFonctionExistante.setLayout(null);
		
		scrlpanExistante = new JScrollPane();
		scrlpanExistante.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrlpanExistante.setBounds(10, 23, 159, 160);
		panFonctionExistante.add(scrlpanExistante);
		
		listeFonctions = new JList<Fonction>();
		listeFonctions.setModel(fonctions);
		scrlpanExistante.setViewportView(listeFonctions);
		
		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int indexFonction = listeFonctions.getSelectedIndex();
				if(indexFonction >= 0) {
					fonctions.remove(indexFonction);
					
					int taille = fonctions.size();
					if(indexFonction < taille) {
						listeFonctions.setSelectedIndex(indexFonction);
					} else if(taille > 0) {
						listeFonctions.setSelectedIndex(taille - 1);
					}
				}
			}
		});
		btnSupprimer.setBounds(10, 194, 159, 23);
		panFonctionExistante.add(btnSupprimer);
		
		panel = new JPanel();
		panel.setBounds(10, 249, 502, 52);
		contentPane.add(panel);
		panel.setBorder(new TitledBorder(null, "Param\u00E8tres globaux", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setLayout(null);
		
		lblVitesseEnX = new JLabel("Vitesse (m/s) :");
		lblVitesseEnX.setBounds(20, 24, 86, 14);
		panel.add(lblVitesseEnX);
		
		spnVitesse = new JSpinner();
		spnVitesse.setBounds(116, 21, 76, 20);
		panel.add(spnVitesse);
		spnVitesse.setModel(new SpinnerNumberModel(5.0, 1.0, 15.0, 1.0));
		
		lblNom = new JLabel("Nom:");
		lblNom.setBounds(202, 24, 58, 14);
		panel.add(lblNom);
		
		txtNom = new JTextField();
		txtNom.setBounds(270, 21, 86, 20);
		txtNom.setDocument(new LimiteTexte(5));
		panel.add(txtNom);
		txtNom.setColumns(10);
		{
			btnpaneBoutons = new JPanel();
			btnpaneBoutons.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(btnpaneBoutons, BorderLayout.SOUTH);
			{
				btnOk = new JButton("OK");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int indexFonction = listeFonctions.getSelectedIndex();
						if(indexFonction < 0) {
							JOptionPane.showMessageDialog(DialogueFonctions.this,
															"Vous devez s�lectionner une fonction.",
															"Erreur",
															JOptionPane.ERROR_MESSAGE);
						} else {
							setVisible(false);
							if(ecouteur != null) {
								Fonction fonction = fonctions.get(indexFonction).copie();
								ecouteur.dialogueAccepte(fonction);
							}
						}
					}
				});
				btnOk.setActionCommand("OK");
				btnpaneBoutons.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				btnAnnuler = new JButton("Annuler");
				btnAnnuler.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						if(ecouteur != null) {
							ecouteur.dialogueAnnule();
						}
					}
				});
				btnAnnuler.setActionCommand("Cancel");
				btnpaneBoutons.add(btnAnnuler);
			}
		}
	}
	public void montrer(TypeFonction type){
		switch(type){
		case LINEAIRE:
			panOnglets.setSelectedIndex(0);
			break;
		case CERCLE:
			panOnglets.setSelectedIndex(1);
			break;
		case SINUS:
			panOnglets.setSelectedIndex(2);
			break;
		case COSINUS:
			panOnglets.setSelectedIndex(2);
			break;
		case PERSONNALISEE:
			break;
		}
	}
	public void ajouterFonctionListe(Fonction fonction){
		Fonction f=fonction.copie();
		if(f != null) {
			if(!fonctions.contains(f)) {
				fonctions.addElement(f);
			}else{
				 JOptionPane.showMessageDialog(this, "Cette fonction existe d�j�.", "Erreur", JOptionPane.ERROR_MESSAGE);
			}
			listeFonctions.setSelectedValue(f, true);
		}
	}
	/**
	 * Assigner un nouvel �couteur au dialogue.
	 * @param ecouteur : Nouvel �couteur.
	 */
	public void setEcouteur(EcouteurDialogueFonction ecouteur) {
		this.ecouteur = ecouteur;
	}
}
