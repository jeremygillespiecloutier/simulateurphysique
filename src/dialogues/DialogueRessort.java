package dialogues;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet de type ressort.
 */
public class DialogueRessort extends JDialog {
	private static final long serialVersionUID = 6559160357252469413L;
	private final JPanel contentPanel = new JPanel();
	private JSpinner spnConstante;
	private JSpinner spnLongueur;
	private boolean confirmer=false;
	private JCheckBox chckbxRessort;
	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		try {
			DialogueRessort dialog = new DialogueRessort(false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Constructeur du dialogue d'insertion de ressort.
	 * @param deuxCorps Bool�en indiquant si le ressort a la possibilit� d'�tre connect� � un second corps.
	 */
	public DialogueRessort(boolean deuxCorps) {
		setResizable(false);
		setTitle("Options ressort");
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		setBounds(100, 100, 316, 231);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton btnOk = new JButton("Ok");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					confirmer=true;
					setVisible(false);
				}
			});
			btnOk.setBounds(32, 143, 120, 23);
			contentPanel.add(btnOk);
			btnOk.setActionCommand("OK");
			getRootPane().setDefaultButton(btnOk);
		}
		{
			JButton btnAnnuler = new JButton("Annuler");
			btnAnnuler.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setVisible(false);
				}
			});
			btnAnnuler.setBounds(171, 143, 119, 23);
			contentPanel.add(btnAnnuler);
			btnAnnuler.setActionCommand("Cancel");
		}
		{
			JLabel lblConstante = new JLabel("Constante (N/m):");
			lblConstante.setBounds(32, 31, 100, 14);
			contentPanel.add(lblConstante);
		}
		{
			spnConstante = new JSpinner();
			spnConstante.setModel(new SpinnerNumberModel(10.0, 1.0, 20.0, 1.0));
			spnConstante.setBounds(142, 28, 109, 20);
			contentPanel.add(spnConstante);
		}
		
		JLabel lblLongueur = new JLabel("Longueur (m):");
		lblLongueur.setBounds(32, 68, 81, 14);
		contentPanel.add(lblLongueur);
		
		spnLongueur = new JSpinner();
		spnLongueur.setModel(new SpinnerNumberModel(0.3, 0.1, 5.0, 0.1));
		spnLongueur.setBounds(142, 65, 109, 20);
		contentPanel.add(spnLongueur);
		
		chckbxRessort = new JCheckBox("Coller sur le mur");
		if(!deuxCorps)
			chckbxRessort.setEnabled(true);
		chckbxRessort.setEnabled(deuxCorps);
		chckbxRessort.setBounds(32, 103, 153, 23);
		contentPanel.add(chckbxRessort);
	}
	/**
	 * Retourne la constante de rappel du ressort.
	 * @return La constante de rappel du ressort.
	 */
	public double getConstante(){
		return (double) spnConstante.getValue();
	}
	/**
	 * Retourne la longueur � l'�quilibre du ressort.
	 * @return La longueur � l'�quilibre du ressort.
	 */
	public double getLongueur(){
		return (double) spnLongueur.getValue();
	}
	/**
	 * Retourne un bool�en indiquant si le dialogue a �t� confirm�.
	 * @return Bool�en indiquant si le dialogue a �t� confirm�.
	 */
	public boolean getConfirmer(){
		return this.confirmer;
	}
	/**
	 * Retourne un bool�en indiquant si le ressort est fix� � un mur.
	 * @return Bool�en indiquant si le ressort est fix� � un mur.
	 */
	public boolean getColle(){
		return chckbxRessort.isSelected();
	}
}
