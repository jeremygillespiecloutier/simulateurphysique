package dialogues;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'afficher le menu sources.
 */
public class DialogueSources extends JFrame {
	private static final long serialVersionUID = 4211102919693955801L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueSources frame = new DialogueSources();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogueSources() {
		setResizable(false);
		setTitle("Sources");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 661, 422);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnOk.setBounds(274, 359, 98, 26);
		contentPane.add(btnOk);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(5, 11, 640, 336);
		contentPane.add(scrollPane);
		
		JTextPane texteSources = new JTextPane();
		scrollPane.setViewportView(texteSources);
		texteSources.setEditable(false);
		texteSources.setOpaque(false);
		texteSources.setBackground(new Color(0,0,0,0f));
		texteSources.setContentType("text/html");
		texteSources.setText("<html>\r\n<b>Sources externes:</b>\r\n<br>\r\n<br>\r\nImage Concept ressorts: http://www.britannica.com/EBchecked/topic/271336/Hookes-law\r\n<br>\r\nImage Concept friction : http://www.logicbay.com/blog/bid/97595/4-Best-Practices-for-Reducing-Friction-in-the-Channel\r\n<br>\r\nImage concept SAT: http://back2basic.phatcode.net/?Issue_%231:2D_Convex_Polygon_Collision_using_SAT\r\n<br>\r\nTexture bois: http://www.mb3d.co.uk/mb3d/Wood_Seamless_and_Tileable_High_Res_Textures.html\r\n<br>\r\nTexture brique: http://www.mb3d.co.uk/mb3d/Brick_Seamless_and_Tileable_High_Res_Textures.html\r\n<br>\r\nTexture abstrait: http://rosshilbert.deviantart.com/art/Synaptic-Pathways-262358808\r\n<br>\r\nTexture pierre:\r\nhttp://www.deviantart.com/morelikethis/276223718?view_mode=2\r\n<br>\r\nTexture glace:\r\nhttp://vk.com/photo123164635_348965574\r\n<br>\r\nTexture metal: http://kevbrett.deviantart.com/art/Metal-Texture-149174979\r\n<br>\r\nOption performance: http://www.iconeasy.com/icon/status-battery-charging-low-icon/\r\n<br>\r\nOption quadrillage: http://icons.webtoolhub.com/icon-n92192-detail.aspx\r\n<br>\r\nOption scientifique: https://openclipart.org/detail/199756/primary%20integral%20func\r\n<br>\r\nOutil crayon: http://www.summitcountymedicalalliance.org/#!application/c1woh\r\n<br>\r\nOutil explosion: http://www.iconeasy.com/icon/bomb-4-icon/\r\n<br>\r\nOutil rotation: http://www.clker.com/clipart-297804.html\r\n<br>\r\nOutil ressort: http://www.suttonautofactorsonline.co.uk/page.php?product=concentric-slave-cylider\r\n<br>\r\nOutil groupe: https://www.iconfinder.com/icons/365295/area_arrow_cursor_region_select_selection_tool_icon\r\n<br>\r\nOutil sphere: http://commons.wikimedia.org/wiki/File:Sphere_-_monochrome_simple.svg?uselang=fr\r\n<br>\r\nAide algorithme collision: http://www.dyn4j.org/category/gamedev/collision-detection/\r\n<br>\r\nAide moteur physique: http://gamedevelopment.tutsplus.com/series/how-to-create-a-custom-physics-engine--gamedev-12715\r\n<br>\r\n\r\n</html>");
		texteSources.setCaretPosition(0);
	}
}
