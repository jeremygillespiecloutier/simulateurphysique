package dialogues;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import physique.Explosion;
import maths.Vecteur;
import ecouteurs.EcouteurUniversel;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet de type zone explosion.
 */
public class InsertionExplosion extends JDialog {
	private static final long serialVersionUID = -1219675678694436407L;
	private JPanel contentPane;
	private EcouteurUniversel ecouteur;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertionExplosion frame = new InsertionExplosion(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur du dialogue d'insertion d'explosion.
	 * @param ec L'�couteur auqul on ajoutera l'explosion.
	 */
	public InsertionExplosion(EcouteurUniversel ec) {
		setModal(true);
		setResizable(false);
		this.ecouteur = ec;
		setTitle("Insertion rectangle");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 357, 197);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRayon = new JLabel("Rayon d'explosion (m):");
		lblRayon.setBounds(21, 29, 138, 16);
		contentPane.add(lblRayon);

		JLabel lblVitesse = new JLabel("Vitesse transmise (m/s):");
		lblVitesse.setBounds(21, 67, 151, 16);
		contentPane.add(lblVitesse);
		
		final JSpinner spnRayon = new JSpinner();
		spnRayon.setModel(new SpinnerNumberModel(5.0, 0.0, 10.0, 0.5));
		spnRayon.setBounds(196, 27, 98, 20);
		contentPane.add(spnRayon);
		
		final JSpinner spnVitesse = new JSpinner();
		spnVitesse.setModel(new SpinnerNumberModel(5.0, 0.0, 10.0, 0.5));
		spnVitesse.setBounds(196, 65, 98, 20);
		contentPane.add(spnVitesse);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double rayon = (double) spnRayon.getValue();
				double vitesse = (double) spnVitesse.getValue();
				Explosion explosion=new Explosion(vitesse, rayon, new Vecteur(0,0));
				ecouteur.objetCree(explosion);
				dispose();
			}
		});
		btnAjouter.setBounds(21, 115, 98, 26);
		contentPane.add(btnAjouter);

		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(185, 115, 98, 26);
		contentPane.add(btnAnnuler);
		this.getRootPane().setDefaultButton(btnAjouter);
	}
}
