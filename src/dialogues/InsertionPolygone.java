package dialogues;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ecouteurs.EcouteurUniversel;
import formes.Polygone;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet corps en forme de polygone r�gulier.
 */
public class InsertionPolygone extends JDialog {
	private static final long serialVersionUID = 6792119630354622473L;
	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtLongueur;
	private EcouteurUniversel ecouteur;
	private static final double EPSILON=0.0000001;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertionPolygone frame = new InsertionPolygone(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur du dialogue d'insertion de polygone.
	 * @param ec L'�couteur auquel sera sjout� le polygone.
	 */
	public InsertionPolygone(EcouteurUniversel ec) {
		setModal(true);
		setResizable(false);
		this.ecouteur = ec;
		setTitle("Insertion polygone");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 345, 194);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre de c\u00F4t\u00E9s:");
		lblNombre.setBounds(10, 24, 130, 16);
		contentPane.add(lblNombre);

		JLabel lblLongueur = new JLabel("Longueur des c\u00F4t\u00E9s (m):");
		lblLongueur.setBounds(10, 62, 160, 16);
		contentPane.add(lblLongueur);

		txtNombre = new JTextField();
		txtNombre.setText("6");
		txtNombre.setBounds(180, 24, 114, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);

		txtLongueur = new JTextField();
		txtLongueur.setText("1");
		txtLongueur.setBounds(180, 62, 114, 20);
		contentPane.add(txtLongueur);
		txtLongueur.setColumns(10);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int nombre;
				double longueur;
				try {
					nombre = Integer.parseInt(txtNombre.getText());
					longueur = Double.parseDouble(txtLongueur.getText());
					if (nombre < 3) {
						JOptionPane.showMessageDialog(null, "3 c�t�s minimum");
						return;
					}
					if(longueur<=0)
						throw new Exception();
					Polygone p = new Polygone(nombre, longueur);
					p.tourner(EPSILON);
					ecouteur.formeCree(p);
					dispose();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Dimensions invalides");
				}
			}
		});
		btnAjouter.setBounds(20, 94, 98, 26);
		contentPane.add(btnAjouter);

		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(184, 94, 98, 26);
		contentPane.add(btnAnnuler);
		this.getRootPane().setDefaultButton(btnAjouter);
	}
}
