package dialogues;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.AbstractListModel;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JTextPane;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.JButton;

import ecouteurs.EcouteurUniversel;
import moteur.Scene;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.InputStream;
import java.io.ObjectInputStream;
import javax.swing.JScrollPane;
/**
 * Classe permettant de charger des sc�nes de d�monstration dans le dossier ressources.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public class DialogueDemonstration extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3545603795481592443L;
	private JPanel contentPane;
	private LinkedHashMap<String, String> demonstrations = new LinkedHashMap<String, String>();
	private JList<String> listeDemos;
	private JTextPane texteDemo;
	private EcouteurUniversel ecouteur;
	private JScrollPane scrollPane;
	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueDemonstration frame = new DialogueDemonstration(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param ec l'�couteur du dialogue.
	 */
	public DialogueDemonstration(EcouteurUniversel ec) {
		this.ecouteur=ec;
		this.setModal(true);
		setResizable(false);
		setTitle("D\u00E9monstrations");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		demonstrations.put("Chute Libre", "Permet d'observer l'effet de la gravit� sur plusieurs objets et les collisions qui prennent place entre eux.");
		demonstrations.put("Restitution", "Permet de constater la diff�rence du coefficient de restitution entre les types de mat�riaux lors des collisions.");
		demonstrations.put("Frottement", "Permet de constater la diff�rence du coefficient de frottement entre les types de mat�riaux");
		demonstrations.put("Ressorts", "Permet d'observer la physique des ressorts.");
		demonstrations.put("Explosions", "Permet d'animer des corps avec des explosions.");
		demonstrations.put("Trajectoire", "Permet d'observer des objets suivant une trajectoire.");
		demonstrations.put("Billard", "Permet de simuler un jeu de billard avec un ressort. Utilisation du mode vue de haut.");
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 178, 228);
		contentPane.add(scrollPane);
		listeDemos = new JList<String>();
		scrollPane.setViewportView(listeDemos);
		listeDemos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeDemos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listeDemos.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				texteDemo.setText(demonstrations.get(listeDemos.getSelectedValue()));
			}
		});
		listeDemos.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = -4706234808436406700L;
			ArrayList<String> values=new ArrayList<String>(demonstrations.keySet());
			public int getSize() {
				return demonstrations.size();
			}
			public String getElementAt(int index) {
				return values.get(index);
			}
		});
		
		texteDemo = new JTextPane();
		texteDemo.setBackground(Color.LIGHT_GRAY);
		texteDemo.setBounds(178, 0, 266, 228);
		contentPane.add(texteDemo);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String fichier="/demonstrations/"+listeDemos.getSelectedValue().replaceAll(" ", "").toLowerCase()+".mlg";
					InputStream fin = this.getClass().getResourceAsStream(fichier);
					ObjectInputStream ois = new ObjectInputStream(fin);
					Object obj = ois.readObject();
					if(obj.getClass() == Scene.class) {
						Scene s = (Scene) obj;
						s.setLocationFichier("");
						ois.close();
						ecouteur.ajouterScene(s);
					}
				} catch(Exception ex) {
					JOptionPane.showMessageDialog(DialogueDemonstration.this,
							"Erreur de lecture du fichier!",
							"Erreur",
							JOptionPane.ERROR_MESSAGE);
				}
				dispose();
			}
		});
		btnOk.setBounds(0, 227, 222, 44);
		contentPane.add(btnOk);
		
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(222, 227, 222, 44);
		contentPane.add(btnAnnuler);
		this.getRootPane().setDefaultButton(btnOk);
		if(listeDemos.getModel().getSize()>0)
			listeDemos.setSelectedIndex(0);
	}
}
