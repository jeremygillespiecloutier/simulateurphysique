package dialogues;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
/**
 * Classe affichant le dialogue � propos.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public class DialogueAPropos extends JFrame {
	private static final long serialVersionUID = 7312158909046777241L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueAPropos frame = new DialogueAPropos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogueAPropos() {
		setResizable(false);
		setTitle("A propos");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 428, 205);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnOk.setBounds(158, 130, 98, 26);
		contentPane.add(btnOk);
		
		JTextPane texteAPropos = new JTextPane();
		texteAPropos.setEditable(false);
		texteAPropos.setOpaque(false);
		texteAPropos.setBackground(new Color(0,0,0,0f));
		texteAPropos.setContentType("text/html");
		texteAPropos.setText("<html>\r\n<div align=center>\r\nSimulateur physique version finale\r\n<br>\r\nApplication r\u00E9alis\u00E9e par Jeremy Gillespie-Cloutier et Francis Labelle\r\n<br>\r\n2015\r\n<br>\r\nColl\u00E8ge de Maisonneuve\r\n<br>\r\nDans le cadre du cours <i>420\u2010SCD Int\u00E9gration des apprentissages\r\n</html>");
		texteAPropos.setBounds(10, 11, 392, 164);
		contentPane.add(texteAPropos);
	}
}
