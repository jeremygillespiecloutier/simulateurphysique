package dialogues;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Path2D;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import physique.ZoneDeGravite;
import maths.Vecteur;
import ecouteurs.EcouteurGravite;
import ecouteurs.EcouteurUniversel;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import composants.ComposantGravite;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet de type zone de gravit�
 */
public class InsertionZone extends JDialog implements EcouteurGravite {
	private static final long serialVersionUID = -1219675678694436407L;
	private JPanel contentPane;
	private JTextField txtLargeur;
	private JTextField txtHauteur;
	private EcouteurUniversel ecouteur;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertionZone frame = new InsertionZone(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur du dialogue d'insertion de zone de gravit�.
	 * @param ec L'�couteur auquel sera ajout�e la zone de gravit�.
	 */
	public InsertionZone(EcouteurUniversel ec) {
		setModal(true);
		setResizable(false);
		this.ecouteur = ec;
		setTitle("Insertion zone");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 389, 325);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLargeur = new JLabel("Largeur (m):");
		lblLargeur.setBounds(59, 13, 82, 16);
		contentPane.add(lblLargeur);

		JLabel lblHauteur = new JLabel("Hauteur (m):");
		lblHauteur.setBounds(59, 51, 82, 16);
		contentPane.add(lblHauteur);

		txtLargeur = new JTextField();
		txtLargeur.setText("1");
		txtLargeur.setBounds(181, 11, 114, 20);
		contentPane.add(txtLargeur);
		txtLargeur.setColumns(10);

		txtHauteur = new JTextField();
		txtHauteur.setText("1");
		txtHauteur.setBounds(181, 49, 114, 20);
		contentPane.add(txtHauteur);
		txtHauteur.setColumns(10);
		
		final JSpinner spnNorme = new JSpinner();
		spnNorme.setModel(new SpinnerNumberModel(10.0, 0.0, 20.0, 2.0));
		spnNorme.setBounds(181, 86, 89, 20);
		contentPane.add(spnNorme);
		
		final ComposantGravite composantGravite = new ComposantGravite(this);
		composantGravite.setBounds(181, 125, 150, 150);
		composantGravite.setCouleurFond(this.getContentPane().getBackground());
		contentPane.add(composantGravite);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double largeur;
				double hauteur;
				try {
					largeur = Double.parseDouble(txtLargeur.getText());
					hauteur = Double.parseDouble(txtHauteur.getText());
					if(largeur<=0 || hauteur<=0)
						throw new Exception();
					Path2D.Double path=new Path2D.Double();
					path.moveTo(-largeur / 2.0, hauteur / 2.0);
					path.lineTo(-largeur / 2.0, -hauteur / 2.0);
					path.lineTo(largeur / 2.0, -hauteur / 2.0);
					path.lineTo(largeur / 2.0, hauteur / 2.0);
					ZoneDeGravite zone=new ZoneDeGravite(path);
					double angle=composantGravite.getOrientation();
					System.out.println(angle);
					double norme=(double) spnNorme.getValue();
					zone.setGravite(new Vecteur(norme*Math.cos(angle), norme*Math.sin(angle)));
					ecouteur.objetCree(zone);
					dispose();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Dimensions invalides");
				}
			}
		});
		btnAjouter.setBounds(25, 198, 98, 26);
		contentPane.add(btnAjouter);

		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(25, 236, 98, 26);
		contentPane.add(btnAnnuler);
		
		JLabel lblNorme = new JLabel("Norme (N):");
		lblNorme.setBounds(59, 88, 82, 16);
		contentPane.add(lblNorme);
		this.getRootPane().setDefaultButton(btnAjouter);
	}

	@Override
	public void orientationChangee(double orientation) {
		// TODO Auto-generated method stub
		
	}
}
