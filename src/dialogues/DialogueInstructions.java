package dialogues;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
/**
 * Classe permettant d'afficher le menu instructions en chargeant des fichiers html.
 * @author Francis Labelle
 *
 */
public class DialogueInstructions extends JFrame {
	private static final long serialVersionUID = -562930301354396341L;
	private JPanel contentPane;
	private JList<String> liste;
	private JScrollPane scrollPaneZoneTexte;
	private JTextPane zoneTexte;
	private HashMap<String, String> aide = new HashMap<String, String>();
	private JScrollPane scrollPaneListe;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueInstructions frame = new DialogueInstructions();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogueInstructions() {
		setResizable(false);
		setTitle("Instructions completes");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 735, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPaneZoneTexte = new JScrollPane();
		scrollPaneZoneTexte.setBounds(163, 5, 566, 448);
		contentPane.add(scrollPaneZoneTexte);
		
		zoneTexte = new JTextPane();
		zoneTexte.setContentType("text/html");
		zoneTexte.setEditable(false);
		scrollPaneZoneTexte.setViewportView(zoneTexte);
		
		JButton button = new JButton("Ok");
		button.setBounds(5, 453, 724, 38);
		contentPane.add(button);
		
		scrollPaneListe = new JScrollPane();
		scrollPaneListe.setBounds(5, 5, 159, 448);
		contentPane.add(scrollPaneListe);
		
		liste = new JList<String>();
		liste.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneListe.setViewportView(liste);
		liste.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				zoneTexte.setText(aide.get(liste.getSelectedValue()));
				zoneTexte.setCaretPosition(0);
				repaint();
			}
		});
		
		liste.setModel(new AbstractListModel<String>() {
			private static final long serialVersionUID = -4706234808436406700L;
			String[] values = new String[] {"Fonctionnement G\u00E9n\u00E9ral", "1. Outils", "       1.1 S\u00E9lection", "       1.2 Groupe", "       1.3 Rectangle", "       1.4 Explosion", "       1.5 Ligne", "       1.6 Zone de gravit\u00E9", "       1.7 Sph\u00E8re", "       1.8 Tourner", "       1.9 Ressort", "       1.10 Dessin libre", "2. Modes d'affichage", "       2.1 Grille", "       2.2 Scientifique", "       2.3 Chemin", "       2.4 Performance", "3. Inspecteurs", "       3.1 Sc\u00E8ne", "       3.2 Polygone", "       3.3 Zone de gravit\u00E9", "       3.4 Explosion", "       3.5 Ressort",  "4. Menus", "       4.1 Menu Sc\u00E8ne", "       4.2 Menu Insertion", "       4.3 Menu Fonctions", "       4.4 Menu Groupe", "       4.5 Menu S\u00E9lection", "       4.6 Menu Aide", "5. Onglet Performance", "6. Materiaux"};
			public int getSize() {
				return values.length;
			}
			public String getElementAt(int index) {
				return values[index];
			}
		});
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		//NB : NE PAS OUBLIER DE RAFRA�CHIR LE DOSSIERS RESSOURCES APR�S MODIFICATION DE L'AIDE SINON L'ANCIENNE VERSION SERA LUE.
		chargerAide();
		liste.setSelectedIndex(0);
	}
	
	/**
	 * Charger l'aide sp�cifique � chaque section.
	 */
	private void chargerAide() {
		ListModel<String> sections = liste.getModel();
		for(int i = 0 ; i < sections.getSize() ; i++) {
			String section = sections.getElementAt(i);
			String nomFichier = section.trim() + ".txt";
			InputStream flux = this.getClass().getClassLoader().getResourceAsStream("aide/" + nomFichier);
		
			try {
				if(flux == null) {
					throw new IOException();
				}
				
				BufferedReader lecteur = new BufferedReader(new InputStreamReader(flux));
				String aideStr = "";
				String ligne = lecteur.readLine();
				
				while(ligne != null) {
					String expression="mlg420<";
					if(ligne.contains(expression)){
						try{
							String fichier=ligne.substring(ligne.indexOf(expression)+expression.length());
							fichier=fichier.substring(0, fichier.indexOf(">"));
							String url=this.getClass().getClassLoader().getResource(fichier).toString();
							ligne=ligne.replaceFirst(expression+fichier+">", '"'+url.replaceFirst("/", "").replaceAll("%20", " ")+'"');
						}catch(Exception e){
							
						}
					}
					aideStr += ligne + "\n";
					ligne = lecteur.readLine();
				}
				lecteur.close();
				if(aideStr.isEmpty()) {
					throw new IOException();
				} else {
					aide.put(section, aideStr);
				}
			} catch(IOException e) {
				aide.put(section, "<html><p color='red'>Cette section de l'aide est manquante.</p></html>");
			}
		}
	}
}
