package dialogues;

import fonctions.Fonction;

/**
 * Interface utilis�e par le dialogue des fonctions pour avertir un objet de diff�rents �v�nements.
 * @author Francis Labelle
 */
public interface EcouteurDialogueFonction {
	/**
	 * M�thode appel�e lorsque le dialogue est annul�.
	 */
	public void dialogueAnnule();
	
	/**
	 * M�thode appel�e lorsque le dialogue est accept�.
	 * @param fonction : Fonction choisie qui doit �tre assign�e � un polygone.
	 */
	public void dialogueAccepte(Fonction fonction);
	
	/**
	 * M�thode appel�e lorsqu'une fonction est supprim�e.
	 * @param fonction : Fonction qui a �t� supprim�e.
	 */
	public void fonctionSupprimee(Fonction fonction);
}
