package dialogues;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'afficher le menu jeux d'essai.
 */
public class DialogueJeuxEssai extends JFrame {
	private static final long serialVersionUID = 701918691857102267L;
	private JPanel contentPane;
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueJeuxEssai frame = new DialogueJeuxEssai();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogueJeuxEssai() {
		setResizable(false);
		setTitle("Jeux d'essai");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 607, 355);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnOk.setBounds(245, 285, 98, 26);
		contentPane.add(btnOk);
		
		JTextPane texteJeuxEssai = new JTextPane();
		texteJeuxEssai.setEditable(false);
		texteJeuxEssai.setOpaque(false);
		texteJeuxEssai.setBackground(new Color(0,0,0,0f));
		texteJeuxEssai.setContentType("text/html");
		texteJeuxEssai.setText("<html>\r\n<u>Jeu d'essai version finale</u>\r\n<ul>\r\n  <li>S\u00E9lectionner l'item de menu D\u00E9monstrations  dans le menu Sc\u00E8ne.</li>\r\n <li>S\u00E9lectionner dans la liste du dialogue la d\u00E9monstration \"Chute Libre\"</li>\r\n<li>Appuyer sur le bouton ok pour que la sc\u00E8ne se charge</li>\r\n<li>D\u00E9placer la molette de souris vers le bas jusqu'\u00E0 ce que le zoom soit de 0%</li>\r\n<li>Appuyer sur la touche espace du clavier pour lancer l'animation</li>\r\n<li>Pendant que l'animation est en cours, s\u00E9lectionner la grosse sph\u00E8re avec un clic droit de la souris et la d\u00E9placer en la faisant entrer en collision avec les autres objets</li>\r\n<li>Constater l'effet des collisions entre les corps</li>\r\n</ul>\r\n<u>Autres jeux d'essai</u><br>\r\nSe rendre dans le menu d\u00E9monstrations et charger les autres d\u00E9monstrations.\r\n</div>\r\n<html>");
		texteJeuxEssai.setBounds(33, 11, 525, 285);
		contentPane.add(texteJeuxEssai);
	}
}
