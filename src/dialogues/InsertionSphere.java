package dialogues;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import maths.Vecteur;
import ecouteurs.EcouteurUniversel;
import formes.Cercle;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet corps en forme de sph�re.
 */
public class InsertionSphere extends JDialog {
	private static final long serialVersionUID = -3644261644556167421L;
	private JPanel contentPane;
	private JTextField txtRayon;
	private EcouteurUniversel ecouteur;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertionSphere frame = new InsertionSphere(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur du dialogue d'insertion de sph�re.
	 * @param ec L'�couteur auquel sera ajout�e la sph�re.
	 */
	public InsertionSphere(EcouteurUniversel ec) {
		setModal(true);
		setResizable(false);
		this.ecouteur = ec;
		setTitle("Insertion sphere");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 286, 138);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRayon = new JLabel("Rayon (m):");
		lblRayon.setBounds(10, 13, 82, 16);
		contentPane.add(lblRayon);

		txtRayon = new JTextField();
		txtRayon.setText("1");
		txtRayon.setBounds(132, 11, 114, 20);
		contentPane.add(txtRayon);
		txtRayon.setColumns(10);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double rayon;
				try {
					rayon = Double.parseDouble(txtRayon.getText());
					if(rayon<=0)
						throw new Exception();
					Cercle c = new Cercle(new Vecteur(0, 0), rayon);
					ecouteur.formeCree(c);
					dispose();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Dimensions invalides");
				}
			}
		});
		btnAjouter.setBounds(10, 54, 98, 26);
		contentPane.add(btnAjouter);

		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(155, 54, 98, 26);
		contentPane.add(btnAnnuler);
		this.getRootPane().setDefaultButton(btnAjouter);
	}
}
