package dialogues;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import maths.Vecteur;
import ecouteurs.EcouteurUniversel;
import formes.Polygone;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'ins�rer un objet corps en forme de ligne.
 */
public class InsertionLigne extends JDialog {
	private static final long serialVersionUID = 5427457035766228099L;
	private JPanel contentPane;
	private JTextField txtLongueur;
	private JTextField txtOrientation;
	private EcouteurUniversel ecouteur;

	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertionLigne frame = new InsertionLigne(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructeur du dialogue d'insertion de ligne.
	 * @param ec L'�couteur auquel sera ajout�e la ligne.
	 */
	public InsertionLigne(EcouteurUniversel ec) {
		setModal(true);
		setResizable(false);
		this.ecouteur = ec;
		setTitle("Insertion polygone");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 312, 165);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblLongueur = new JLabel("Longueur du plan (m):");
		lblLongueur.setBounds(10, 13, 130, 16);
		contentPane.add(lblLongueur);

		JLabel lblOrientation = new JLabel("Orientation (degr\u00E9s):");
		lblOrientation.setBounds(10, 51, 130, 16);
		contentPane.add(lblOrientation);

		txtLongueur = new JTextField();
		txtLongueur.setText("1");
		txtLongueur.setBounds(158, 11, 114, 20);
		contentPane.add(txtLongueur);
		txtLongueur.setColumns(10);

		txtOrientation = new JTextField();
		txtOrientation.setText("0");
		txtOrientation.setBounds(158, 49, 114, 20);
		contentPane.add(txtOrientation);
		txtOrientation.setColumns(10);

		JButton btnAjouter = new JButton("Ajouter");
		btnAjouter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double longueur;
				double orientation;
				double distanceAxe=0.15;
				try {
					longueur = Double.parseDouble(txtLongueur.getText());
					orientation = Double.parseDouble(txtOrientation.getText())*Math.PI/180;
					if(longueur<=0)
						throw new Exception();
					Polygone p = new Polygone(longueur, distanceAxe, new Vecteur(0,0));
					p.tourner(orientation);
					ecouteur.formeCree(p);
					dispose();
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null, "Dimensions invalides");
				}
			}
		});
		btnAjouter.setBounds(10, 90, 98, 26);
		contentPane.add(btnAjouter);

		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnAnnuler.setBounds(174, 90, 98, 26);
		contentPane.add(btnAnnuler);
		this.getRootPane().setDefaultButton(btnAjouter);
	}
}
