package dialogues;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant d'afficher le menu concepts scientifiques.
 */
public class DialogueConcepts extends JFrame {
	private static final long serialVersionUID = -4939105812937894496L;
	private JPanel contentPane;
	private String concepts = "";
	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DialogueConcepts frame = new DialogueConcepts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DialogueConcepts() {
		setResizable(false);
		setTitle("Concepts scientifiques");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 559, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnOk.setBounds(211, 387, 98, 26);
		contentPane.add(btnOk);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 11, 521, 365);
		contentPane.add(scrollPane);
		
		JTextPane texteConcepts = new JTextPane();
		scrollPane.setViewportView(texteConcepts);
		texteConcepts.setContentType("text/html");
		texteConcepts.setOpaque(false);
		texteConcepts.setBackground(new Color(0,0,0,0f));
		texteConcepts.setEditable(false);
		texteConcepts.setText(concepts);
		InputStream flux = this.getClass().getClassLoader().getResourceAsStream("aide/concepts.txt");
		try {
			BufferedReader lecteur = new BufferedReader(new InputStreamReader(flux));
			String ligne = lecteur.readLine();
			while(ligne != null) {
				String expression="mlg420<";
				if(ligne.contains(expression)){
					try{
						String fichier=ligne.substring(ligne.indexOf(expression)+expression.length());
						fichier=fichier.substring(0, fichier.indexOf(">"));
						String url=this.getClass().getClassLoader().getResource(fichier).toString();
						ligne=ligne.replaceFirst(expression+fichier+">", '"'+url.replaceFirst("/", "").replaceAll("%20", " ")+'"');
					}catch(Exception e){
						
					}
				}
				concepts += ligne + "\n";
				ligne = lecteur.readLine();
			}
			lecteur.close();
		} catch(IOException e) {
			
		}
		texteConcepts.setText(concepts);
		texteConcepts.setCaretPosition(0);
	}
}
