package inspecteurs;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import composants.ComposantGravite;
import ecouteurs.EcouteurGravite;
import maths.Vecteur;
import physique.ZoneDeGravite;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'afficher et de modifier les objets de type zone de gravit�.
 */
public class InspecteurZoneDeGravite extends JPanel implements Inspecteur, EcouteurGravite {
	private static final long serialVersionUID = 7053304898511833769L;
	private JLabel lblInspecteur;
	private ZoneDeGravite zone;
	private JLabel lblGravite;
	private JLabel lblOrientation;
	private double norme=10;
	private double orientation=0;
	private JLabel lblNorme;
	private JSpinner spnNorme;
	private ComposantGravite composantGravite;
	/**
	 * Create the panel.
	 */
	public InspecteurZoneDeGravite() {
		setSize(new Dimension(300, 300));
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		lblInspecteur = new JLabel("Inspecteur de zone");
		lblInspecteur.setForeground(Color.RED);
		lblInspecteur.setBounds(99, 5, 157, 14);
		add(lblInspecteur);
		
		lblGravite = new JLabel("Gravite:");
		lblGravite.setBounds(10, 20, 276, 16);
		add(lblGravite);
		
		composantGravite = new ComposantGravite(this);
		composantGravite.setBounds(68, 139, 150, 150);
		add(composantGravite);
		
		lblOrientation = new JLabel("Orientation:");
		lblOrientation.setBounds(12, 35, 276, 16);
		add(lblOrientation);
		
		lblNorme = new JLabel("Norme (N):");
		lblNorme.setBounds(10, 70, 79, 16);
		add(lblNorme);
		
		spnNorme = new JSpinner();
		spnNorme.setFocusable(false);
		spnNorme.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				orientationChangee(composantGravite.getOrientation());
			}
		});
		spnNorme.setModel(new SpinnerNumberModel(new Double(0), new Double(0), new Double(100), new Double(2)));
		spnNorme.setBounds(99, 69, 92, 20);
		add(spnNorme);
		setVisible(true);
	}
	/**
	 * Affiche les nouvelles propri�t�s de la zone de gravit� apr�s un changement.
	 */
	public void actualiser() {
		double angle=Math.floor(zone.getGravite().getOrientation()*100)/100.0;
		angle=Double.isNaN(angle)?0:angle;
		lblOrientation.setText("Orientation: "+angle+" rad");
		double x=Math.floor(zone.getGravite().getX()*100)/100.0;
		double y=Math.floor(zone.getGravite().getY()*100)/100.0;
		lblGravite.setText("Gravite: ("+(Double.isNaN(x)?0:x)+", "+(Double.isNaN(y)?0:y)+") N");
		revalidate();
	}
	/**
	 * Change la zone de gravit� dont les propri�t�s seront affich�es.
	 * @param zone La zone de gravit� dont les propri�t�s seront affich�es.
	 */
	public void setZone(ZoneDeGravite zone) {
		this.zone=zone;
		norme=zone.getGravite().longueur();
		orientation=Double.isNaN(zone.getGravite().getOrientation())?0:zone.getGravite().getOrientation();
		composantGravite.setOrientation(orientation);
		spnNorme.setValue(new Double(norme));
		actualiser();
	}
	public void reinitialiser() {}

	@Override
	public void orientationChangee(double orientation) {
		norme=(double)spnNorme.getValue();
		zone.setGravite(new Vecteur(Math.cos(orientation), Math.sin(orientation)).mul(norme));
		actualiser();
	}

	@Override
	public void redessiner() {
		repaint();
	}
}
