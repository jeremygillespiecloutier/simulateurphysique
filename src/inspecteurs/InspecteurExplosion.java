package inspecteurs;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ecouteurs.EcouteurGravite;
import physique.Explosion;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'afficher et de modifier les propri�t�s d'un objet de type explosion
 */
public class InspecteurExplosion extends JPanel implements Inspecteur, EcouteurGravite {
	private static final long serialVersionUID = 7053304898511833769L;
	private JLabel lblInspecteur;
	private Explosion explosion;
	private JSpinner spnVitesse;
	private JSpinner spnRayon;
	/**
	 * Create the panel.
	 */
	public InspecteurExplosion() {
		setSize(new Dimension(300, 300));
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		lblInspecteur = new JLabel("Inspecteur explosion");
		lblInspecteur.setForeground(Color.RED);
		lblInspecteur.setBounds(99, 5, 157, 14);
		add(lblInspecteur);
		
		JLabel lblVitesse = new JLabel("vitesse des objets (m/s):");
		lblVitesse.setBounds(10, 35, 139, 14);
		add(lblVitesse);
		
		JLabel lblRayon = new JLabel("Rayon d'explosion (m)");
		lblRayon.setBounds(10, 72, 139, 14);
		add(lblRayon);
		
		spnVitesse = new JSpinner();
		spnVitesse.setModel(new SpinnerNumberModel(new Double(0), new Double(0), new Double(10), new Double(0.5)));
		spnVitesse.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				explosion.setVitesse((double) spnVitesse.getValue());
			}
		});
		spnVitesse.setBounds(174, 30, 97, 20);
		add(spnVitesse);
		
		spnRayon = new JSpinner();
		spnRayon.setModel(new SpinnerNumberModel(new Double(0), new Double(0), new Double(10), new Double(0.5)));
		spnRayon.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				explosion.setRayon((double) spnRayon.getValue());
			}
		});
		spnRayon.setBounds(174, 61, 97, 20);
		add(spnRayon);
		setVisible(true);
	}
	/**
	 * Change l'explosion dont les propri�t�s seront affich�es.
	 * @param explosion L'explosion dont les propri�t�s seront affich�es.
	 */
	public void setExplosion(Explosion explosion){
		this.explosion=explosion;
		actualiser();
	}
	/**
	 * Affiche les nouvelles propri�t�s de l'exposion suite � un changement.
	 */
	public void actualiser() {
		spnVitesse.setValue(explosion.getVitesse());
		spnRayon.setValue(explosion.getRayon());
		revalidate();
		repaint();
	}
	
	public void reinitialiser() {}
	/**
	 * Redessine l'inspecteur.
	 */
	@Override
	public void redessiner() {
		repaint();
	}
	/**
	 * 
	 */
	@Override
	public void orientationChangee(double orientation) {
		// TODO Auto-generated method stub
		
	}
}
