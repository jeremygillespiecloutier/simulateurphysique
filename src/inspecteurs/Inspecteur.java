package inspecteurs;

import java.io.Serializable;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Interface de base pour les classes Inspecteur
 */
public interface Inspecteur extends Serializable{
	public void actualiser();
	public void reinitialiser();
	public void redessiner();
}
