package inspecteurs;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import composants.ComposantGravite;
import ecouteurs.EcouteurGravite;
import ecouteurs.EcouteurUniversel;
import materiaux.TypeMateriau;
import maths.Vecteur;
import moteur.Scene;

import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
/**
 * Classe permettant d'afficher et de modifier les propri�t�s des objets de type sc�ne.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public class InspecteurScene extends JPanel implements Inspecteur, EcouteurGravite {
	private static final long serialVersionUID = 7053304898511833769L;
	private JLabel lblInspecteur;
	private Scene scene;
	private JLabel lblGravite;
	private JLabel lblOrientation;
	private double norme=10;
	private double orientation=0;
	private JLabel lblNorme;
	private JSpinner spnNorme;
	private ComposantGravite composantGravite;
	private JComboBox<TypeMateriau> cmbMateriau;
	private EcouteurUniversel ecouteur;
	private JCheckBox chckbxVueDeHaut;
	/**
	 * Create the panel.
	 * @param ec L'�couteur de l'inspecteur.
	 */
	public InspecteurScene(EcouteurUniversel ec) {
		this.ecouteur=ec;
		setSize(new Dimension(300, 300));
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		lblInspecteur = new JLabel("Inspecteur de sc\u00E8nes");
		lblInspecteur.setForeground(Color.RED);
		lblInspecteur.setBounds(99, 5, 157, 14);
		add(lblInspecteur);
		
		lblGravite = new JLabel("Gravite:");
		lblGravite.setBounds(10, 20, 276, 16);
		add(lblGravite);
		
		composantGravite = new ComposantGravite(this);
		composantGravite.setBounds(75, 147, 150, 150);
		add(composantGravite);
		
		lblOrientation = new JLabel("Orientation:");
		lblOrientation.setBounds(12, 35, 276, 16);
		add(lblOrientation);
		
		lblNorme = new JLabel("Norme (N):");
		lblNorme.setBounds(10, 63, 79, 16);
		add(lblNorme);
		
		spnNorme = new JSpinner();
		spnNorme.setFocusable(false);
		spnNorme.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				orientationChangee(composantGravite.getOrientation());
			}
		});
		spnNorme.setModel(new SpinnerNumberModel(new Double(10), new Double(0), new Double(100), new Double(2)));
		spnNorme.setBounds(164, 62, 92, 20);
		add(spnNorme);
		
		JLabel lblMateriau = new JLabel("Arri\u00E8re plan:");
		lblMateriau.setBounds(10, 97, 97, 16);
		add(lblMateriau);
		
		cmbMateriau= new JComboBox<TypeMateriau> ();
		cmbMateriau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scene.setMateriau((TypeMateriau) cmbMateriau.getSelectedItem());
				ecouteur.redessiner();
			}
		});
		cmbMateriau.setModel(new DefaultComboBoxModel<TypeMateriau>(new TypeMateriau[] { TypeMateriau.brique, TypeMateriau.abstrait, TypeMateriau.pierre }));
		cmbMateriau.setBounds(164, 95, 92, 20);
		add(cmbMateriau);
		
		chckbxVueDeHaut = new JCheckBox("Mode vue de haut");
		chckbxVueDeHaut.setBackground(Color.LIGHT_GRAY);
		chckbxVueDeHaut.setFocusable(false);
		chckbxVueDeHaut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				scene.setVueDeHaut(chckbxVueDeHaut.isSelected());
			}
		});
		chckbxVueDeHaut.setBounds(10, 121, 165, 23);
		add(chckbxVueDeHaut);
		setVisible(true);
	}
	/**
	 * Affiche les nouvelles propri�t�s de la sc�ne suite � un changement.
	 */
	public void actualiser() {
		double angle=Math.floor(scene.getGravite().getOrientation()*100)/100.0;
		angle=Double.isNaN(angle)?0:angle;
		lblOrientation.setText("Orientation: "+angle+" rad");
		double x=Math.floor(scene.getGravite().getX()*100)/100.0;
		double y=Math.floor(scene.getGravite().getY()*100)/100.0;
		lblGravite.setText("Gravite: ("+(Double.isNaN(x)?0:x)+", "+(Double.isNaN(y)?0:y)+") N");
		cmbMateriau.setSelectedItem(scene.getMateriau());
		chckbxVueDeHaut.setSelected(scene.getVueDeHaut());
		repaint();
	}
	/**
	 * Change la sc�ne dont l'inspecteur affiche les propri�t�s.
	 * @param s La sc�ne dont l'inspecteur affiche les propri�t�s.
	 */
	public void setScene(Scene s) {
		this.scene=s;
		norme=scene.getGravite().longueur();
		orientation=Double.isNaN(scene.getGravite().getOrientation())?0:scene.getGravite().getOrientation();
		composantGravite.setOrientation(orientation);
		spnNorme.setValue(norme);
		actualiser();
	}
	public void reinitialiser() {}
	/**
	 * M�thode appel�e lorsque l'orientation du vecteur du composant gravit� est modifi�e.
	 */
	@Override
	public void orientationChangee(double orientation) {
		norme=(double)spnNorme.getValue();
		scene.setGravite(new Vecteur(Math.cos(orientation), Math.sin(orientation)).mul(norme));
		actualiser();
	}
	/**
	 * Redessine l'inspecteur.
	 */
	@Override
	public void redessiner() {
		repaint();
	}
}
