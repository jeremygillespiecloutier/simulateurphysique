package inspecteurs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import materiaux.TypeMateriau;
import maths.Vecteur;
import physique.Corps;
import dialogues.DialogueFonctions;
import dialogues.EcouteurDialogueFonction;
import ecouteurs.EcouteurUniversel;
import enums.TypeFonction;
import fonctions.Fonction;

import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.border.TitledBorder;
import javax.swing.JButton;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'afficher et de modifier les objets de type corps.
 */
public class InspecteurPolygone extends JPanel implements Inspecteur, EcouteurDialogueFonction{
	private static final long serialVersionUID = 2931875497877515596L;
	private JLabel lblMasse;
	private JLabel lblOrientation;
	private JLabel lblPosition;
	private JLabel lblInspecteur;
	private Corps corps;
	private EcouteurUniversel ecouteur;
	private JComboBox<TypeMateriau> cmbMateriau;
	private JCheckBox chckbxFixe;
	private JCheckBox chckbxSuivre;
	private JLabel lblVitesse;
	private JLabel lblAcceleration;
	private JLabel lblVitesseAngulaire;
	private JLabel lblAire;
	private JLabel lblDensite;
	private JLabel lblInertie;
	private JSpinner spnVitesseAngulaire;
	private JPanel panneauMoteur;
	private JLabel lblFonction;
	private DialogueFonctions dialogue;
	private JButton btnFonction;
	private JButton btnEnlever;
	/**
	 * Create the panel.
	 * @param e L'�couteur de l'inspecteur
	 */
	public InspecteurPolygone(EcouteurUniversel e) {
		dialogue=new DialogueFonctions();
		dialogue.setEcouteur(this);
		this.ecouteur = e;
		setSize(new Dimension(300, 300));
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		lblMasse = new JLabel("Masse:");
		lblMasse.setBounds(12, 30, 276, 16);
		add(lblMasse);

		lblOrientation = new JLabel("Orientation:");
		lblOrientation.setBounds(12, 58, 276, 16);
		add(lblOrientation);

		lblPosition = new JLabel("Position:");
		lblPosition.setBounds(12, 85, 276, 16);
		add(lblPosition);

		lblInspecteur = new JLabel("Inspecteur de polygones");
		lblInspecteur.setForeground(new Color(255, 0, 0));
		lblInspecteur.setBounds(12, 5, 157, 14);
		add(lblInspecteur);

		JLabel lblMateriau = new JLabel("Materiau:");
		lblMateriau.setBounds(12, 112, 56, 14);
		add(lblMateriau);

		cmbMateriau = new JComboBox<TypeMateriau>();
		cmbMateriau.setFocusable(false);
		cmbMateriau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (corps != null && corps.getMateriau()!=(TypeMateriau) cmbMateriau.getSelectedItem()) {
					corps.setMateriau((TypeMateriau) cmbMateriau.getSelectedItem());
					actualiser();
					ecouteur.redessiner();
				}
			}
		});
		cmbMateriau.setModel(new DefaultComboBoxModel<TypeMateriau>(new TypeMateriau[] { TypeMateriau.bois, TypeMateriau.metal, TypeMateriau.glace }));
		cmbMateriau.setBounds(109, 112, 80, 20);
		add(cmbMateriau);
		
		chckbxFixe = new JCheckBox("Fixe");
		chckbxFixe.setFocusable(false);
		chckbxFixe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				corps.setFixe(chckbxFixe.isSelected());
				if(chckbxFixe.isSelected()){
					spnVitesseAngulaire.setEnabled(true);
					btnFonction.setEnabled(true);
					btnEnlever.setEnabled(true);
					corps.setVitesseAngulaire((double)spnVitesseAngulaire.getValue());
				}else{
					spnVitesseAngulaire.setEnabled(false);
					btnFonction.setEnabled(false);
					btnEnlever.setEnabled(false);
					corps.setVitesseAngulaire(0);
				}
			}
		});
		chckbxFixe.setBounds(191, 242, 97, 23);
		add(chckbxFixe);
		
		chckbxSuivre = new JCheckBox("Suivre");
		chckbxSuivre.setFocusable(false);
		chckbxSuivre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ecouteur.corpsSuivi(chckbxSuivre.isSelected()?corps:null);
				ecouteur.redessiner();
			}
		});
		chckbxSuivre.setBounds(191, 211, 97, 23);
		add(chckbxSuivre);
		
		lblVitesse = new JLabel("Vitesse:");
		lblVitesse.setBounds(12, 138, 276, 14);
		add(lblVitesse);
		
		lblAcceleration = new JLabel("Acceleration:");
		lblAcceleration.setBounds(12, 164, 276, 14);
		add(lblAcceleration);
		
		lblVitesseAngulaire = new JLabel("Vitesse Angulaire:");
		lblVitesseAngulaire.setBounds(12, 190, 276, 14);
		add(lblVitesseAngulaire);
		
		lblAire = new JLabel("Aire:");
		lblAire.setBounds(12, 216, 276, 14);
		add(lblAire);
		
		lblDensite = new JLabel("Densite:");
		lblDensite.setBounds(12, 242, 276, 14);
		add(lblDensite);
		
		lblInertie = new JLabel("Inertie:");
		lblInertie.setBounds(12, 272, 276, 14);
		add(lblInertie);
		
		panneauMoteur = new JPanel();
		panneauMoteur.setBorder(new TitledBorder(null, "Moteur", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panneauMoteur.setBounds(191, 5, 97, 198);
		add(panneauMoteur);
		panneauMoteur.setLayout(null);
		
		spnVitesseAngulaire = new JSpinner();
		spnVitesseAngulaire.setFocusable(false);
		spnVitesseAngulaire.setBounds(10, 53, 77, 20);
		panneauMoteur.add(spnVitesseAngulaire);
		spnVitesseAngulaire.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				corps.setVitesseAngulaire((double)spnVitesseAngulaire.getValue());
			}
		});
		spnVitesseAngulaire.setEnabled(false);
		spnVitesseAngulaire.setModel(new SpinnerNumberModel(0.0, -5.0, 5.0, 0.2));
		
		JLabel lblVitesseMoteur = new JLabel("<html>Vitesse<br>angulaire</html>");
		lblVitesseMoteur.setBounds(10, 15, 86, 39);
		panneauMoteur.add(lblVitesseMoteur);
		
		lblFonction = new JLabel("<html>\r\nFonction:\r\n<br>\r\naucune\r\n<html>");
		lblFonction.setBounds(10, 77, 77, 61);
		panneauMoteur.add(lblFonction);
		
		btnFonction = new JButton("Choisir");
		btnFonction.setEnabled(false);
		btnFonction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dialogue.setVisible(true);
			}
		});
		btnFonction.setBounds(10, 140, 77, 23);
		panneauMoteur.add(btnFonction);
		
		btnEnlever = new JButton("Enlever");
		btnEnlever.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enlever();
			}
		});
		btnEnlever.setEnabled(false);
		btnEnlever.setBounds(10, 164, 77, 23);
		panneauMoteur.add(btnEnlever);

	}
	/**
	 * Change le corps dont les propri�t�s seront affich�es.
	 * @param c Le corps dont les propri�t�s seront affich�es.
	 */
	public void setCorps(Corps c) {
		this.corps = c;
	}
	/**
	 * Affiche les nouvelles propri�t�s du corps suite � un changement.
	 */
	public void actualiser() {
		if (corps != null) {
			lblMasse.setText("Masse: " + Math.floor(corps.getMasse() * 100.0) / 100.0 + " kg");
			lblPosition.setText("Position: (" + Math.floor(corps.getPosition().getX() * 100.0) / 100.0 + ", " + Math.floor(corps.getPosition().getY() * 100.0) / 100+") m");
			lblOrientation.setText("Orientation: " + Math.floor(corps.getOrientation() * 100.0) / 100.0 + " rad");
			cmbMateriau.setSelectedItem(corps.getMateriau());
			lblVitesse.setText("Vitesse: ("+Math.floor(corps.getVitesse().getX() * 100.0) / 100.0+", " + Math.floor(corps.getVitesse().getY() * 100.0) / 100.0+" ) m/s");
			Vecteur acceleration=corps.getAcceleration().add(corps.getForce());
			lblAcceleration.setText("Acceleration: ("+Math.floor(acceleration.getX() * 100.0) / 100.0+", " + Math.floor(acceleration.getY() * 100.0) / 100.0+" ) m/s\u00B2");
			lblVitesseAngulaire.setText("Vitesse angulaire: " + Math.floor(corps.getVitesseAngulaire() * 100.0) / 100.0 + " rad/s");
			lblAire.setText("Aire: " + Math.floor(corps.getAire() * 100.0) / 100.0 + " m\u00B2");
			lblDensite.setText("Densite: " + Math.floor(corps.getDensite() * 100.0) / 100.0 +" kg/m\u00B2");
			lblInertie.setText("Inertie: " + Math.floor(corps.getInertie() * 100.0) / 100.0 +" kg*m\u00B2");
			chckbxFixe.setSelected(corps.getFixe());
			chckbxSuivre.setSelected(corps.getSuivi());
			btnEnlever.setEnabled(corps.getFixe());
			spnVitesseAngulaire.setEnabled(corps.getFixe());
			btnFonction.setEnabled(corps.getFixe());
			double moteurAngulaire=corps.getFixe()?corps.getVitesseAngulaire():0;
			spnVitesseAngulaire.setValue(moteurAngulaire);
			String fonction=corps.getFonction()==null?"aucune":corps.getFonction().toString();
			lblFonction.setText("<html>\r\nFonction:\r\n<br>\r\n"+fonction+"\r\n<html>");
		}
	}
	/**
	 * R�initialise le contenu de l'inspecteur.
	 */
	public void reinitialiser() {
		lblMasse.setText("Masse:");
		lblPosition.setText("Position:");
		cmbMateriau.setSelectedItem(TypeMateriau.bois);
		lblVitesse.setText("Vitesse:");
		lblAcceleration.setText("Acceleration:");
		lblVitesseAngulaire.setText("Vitesse angulaire:");
		lblAire.setText("Aire:");
		lblDensite.setText("Densite:");
		lblInertie.setText("Inertie:");
		chckbxFixe.setSelected(false);
		chckbxSuivre.setSelected(false);
		spnVitesseAngulaire.setEnabled(false);
		lblFonction.setText("<html>\r\nFonction:\r\n<br>\r\naucune\r\n<html>");
	}

	@Override
	public void redessiner() {
		repaint();
	}
	public void dialogueAnnule() {}
	/**
	 * M�thode appel�e quand le dialogue de fonctions a �t� accept�.
	 */
	public void dialogueAccepte(Fonction fonction) {
		if(!corps.getFixe())
			corps.setVitesseAngulaire(0);
		corps.setFixe(true);
		corps.setFonction(fonction);
		ecouteur.redessiner();
		actualiser();
	}
	public void fonctionSupprimee(Fonction fonction) {
		
	}
	/**
	 * M�thode servant � ajouter une fonction au corps courant.
	 * @param type Type de fonction � ajouter au corps courant.
	 */
	public void ajouterFonction(TypeFonction type){
		dialogue.montrer(type);
		dialogue.setVisible(true);
	}
	/**
	 * M�thode appel�e pour ajouter au corps courant une fonction d�j� cr��e.
	 * @param f La fonction � ajouter au corps.
	 */
	public void ajouterFonction(Fonction f){
		dialogueAccepte(f);
	}
	/**
	 * M�thode qui ajoute une fonction � la liste des fonctions du dialogue de fonctions.
	 * @param f Fonction � ajouter � la liste des fonctions du dialogue de fonctions.
	 */
	public void ajouterFonctionListe(Fonction f){
		dialogue.ajouterFonctionListe(f);
	}
	/**
	 * Supprime la fonction du corps courant.
	 */
	public void enlever(){
		corps.setFonction(null);
		actualiser();
		ecouteur.redessiner();
	}
}
