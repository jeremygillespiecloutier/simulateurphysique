package inspecteurs;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import physique.Ressort;
import ecouteurs.EcouteurGravite;
import ecouteurs.EcouteurUniversel;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'afficher et de modifier les propri�t�s des objets de type ressort.
 */
public class InspecteurRessort extends JPanel implements Inspecteur, EcouteurGravite {
	private static final long serialVersionUID = 7053304898511833769L;
	private JLabel lblInspecteur;
	private Ressort ressort;
	private JSpinner spnConstante;
	private JSpinner spnLongueur;
	private EcouteurUniversel ecouteur;
	/**
	 * Create the panel.
	 * @param ec L'�couteur de l'inspecteur
	 */
	public InspecteurRessort(EcouteurUniversel ec) {
		this.ecouteur=ec;
		setSize(new Dimension(300, 300));
		this.setBackground(Color.LIGHT_GRAY);
		setLayout(null);

		lblInspecteur = new JLabel("Inspecteur Ressort");
		lblInspecteur.setForeground(Color.RED);
		lblInspecteur.setBounds(99, 5, 157, 14);
		add(lblInspecteur);
		
		JLabel lblConstante = new JLabel("Constante (N/m):");
		lblConstante.setBounds(36, 64, 114, 14);
		add(lblConstante);
		
		spnConstante = new JSpinner();
		spnConstante.setModel(new SpinnerNumberModel(4.0, 1.0, 10.0, 1.0));
		spnConstante.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ressort.setConstante((double) spnConstante.getValue());
			}
		});
		spnConstante.setBounds(160, 61, 109, 20);
		add(spnConstante);
		
		JLabel lblLongueur = new JLabel("Longueur (m):");
		lblLongueur.setBounds(36, 101, 81, 14);
		add(lblLongueur);
		
		spnLongueur = new JSpinner();
		spnLongueur.setModel(new SpinnerNumberModel(0.3, 0.1, 1.0, 0.1));
		spnLongueur.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				ressort.setLongueur((double) spnLongueur.getValue());
				ecouteur.redessiner();
			}
		});
		spnLongueur.setBounds(160, 98, 109, 20);
		add(spnLongueur);
		setVisible(true);
	}
	/**
	 * Change le ressort dont l'inspecteur affichera les propri�t�s.
	 * @param ressort Le ressort dont l'inspecteur affichera les propri�t�s.
	 */
	public void setRessort(Ressort ressort){
		this.ressort=ressort;
		actualiser();
	}
	/**
	 * Affiche les nouvelles propri�t�s du ressort suite � un changement.
	 */
	public void actualiser() {
		spnConstante.setValue(ressort.getConstante());
		spnLongueur.setValue(ressort.getLongueur());
		revalidate();
		repaint();
	}
	public void reinitialiser() {}
	/**
	 * Redessinne l'inspecteur;
	 */
	@Override
	public void redessiner() {
		repaint();
	}

	@Override
	public void orientationChangee(double orientation) {
		// TODO Auto-generated method stub
		
	}
}
