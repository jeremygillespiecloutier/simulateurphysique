package collision;

import java.io.Serializable;

import formes.Cercle;
import formes.Forme;
import formes.Polygone;

/**
 * @author Francis Labelle
 * Classe prenant en charge les collisions peu importe le type de forme
 * (on rel�gue le travail aux classes CollisionCercle, CollisionPolygone et CollisionPolyCercle).
 */
public class Collision implements Serializable{
	private static final long serialVersionUID = 8888736802211654074L;
	
	/**
	 * V�rifie s'il y a une intersection entre deux formes.
	 * @param a : Forme 1
	 * @param b : Forme 2
	 * @return L'intersection s'il y en a une ou null s'il n'y en a pas.
	 */
	public synchronized static Intersection testerIntersection(Forme a, Forme b) {
		Class<? extends Forme> classeA = a.getClass();
		Class<? extends Forme> classeB = b.getClass();
		boolean aCercle = classeA.equals(Cercle.class);
		boolean aPolygone = !aCercle;
		boolean bCercle = classeB.equals(Cercle.class);
		boolean bPolygone = !bCercle;
		
		if(aPolygone && bPolygone) {
			Polygone polygoneA = (Polygone) a;
			Polygone polygoneB = (Polygone) b;
			return CollisionPolygone.testerIntersection(polygoneA, polygoneB);
		} else if (aCercle && bCercle) {
			Cercle cercleA = (Cercle) a;
			Cercle cercleB = (Cercle) b;
			return CollisionCercle.testerIntersection(cercleA, cercleB);
		} else {
			Polygone polygone;
			Cercle cercle;
			
			if(aCercle) {
				polygone = (Polygone) b;
				cercle = (Cercle) a;
			} else {
				polygone = (Polygone) a;
				cercle = (Cercle) b;
			}
			
			return CollisionPolyCercle.testerIntersection(polygone, cercle);
		}
	}
	
	/**
	 * V�rifie s'il y a une collision entre deux formes.
	 * @param a : Forme 1
	 * @param b : Forme 2
	 * @return L'information sur la collision ou null s'il n'y a pas de collision.
	 */
	public static InfoCollision testerCollision(Forme a, Forme b) {
		Class<? extends Forme> classeA = a.getClass();
		Class<? extends Forme> classeB = b.getClass();
		boolean aCercle = classeA.equals(Cercle.class);
		boolean aPolygone = !aCercle;
		boolean bCercle = classeB.equals(Cercle.class);
		boolean bPolygone = !bCercle;

		Intersection intersection = testerIntersection(a, b);

		if(intersection == null) {
			return null;
		} else {
			if(aPolygone && bPolygone) {
				Polygone polygoneA = (Polygone) a;
				Polygone polygoneB = (Polygone) b;
				return CollisionPolygone.testerCollision(polygoneA, polygoneB);
			} else if(aCercle && bCercle) {
				Cercle cercleA = (Cercle) a;
				Cercle cercleB = (Cercle) b;
				return CollisionCercle.testerCollision(cercleA, cercleB);
			} else {
				Polygone polygone;
				Cercle cercle;
				
				if(aCercle) {
					polygone = (Polygone) b;
					cercle = (Cercle) a;
				} else {
					polygone = (Polygone) a;
					cercle = (Cercle) b;
				}
				
				InfoCollision collision = CollisionPolyCercle.testerCollision(polygone, cercle);
				
				if(collision != null) {
					//La normal pointe actuellement de polygone vers cercle.
					//Elle doit pointer de A vers B.
					if(a == cercle) {
						collision = new InfoCollision(collision.getNormale().mul(-1), collision.getProfondeur(), collision.getPoints());
					}
				}
				
				return collision;
			}
		}
	}
}
