package collision;

import java.io.Serializable;
import java.util.ArrayList;

import maths.Vecteur;
import formes.Cercle;
import formes.Polygone;

/**
 * @author Francis Labelle
 * Classe prenant en charge les collisions entre les polygones et les cercles.
 */
public class CollisionPolyCercle implements Serializable{
	private static final long serialVersionUID = 4848825331313005555L;

	/**
	 * V�rifie s'il y a une intersection entre un polygone et un cercle.
	 * @param polygone : Polygone
	 * @param cercle : Cercle
	 * @return L'intersection ou null s'il n'y en a pas.
	 */
	public static Intersection testerIntersection(Polygone polygone, Cercle cercle) {
		Vecteur centreCercle = cercle.getPosition();
		boolean contenu=polygone.contient(centreCercle);
		boolean normaleSpeciale=polygone.getNormaleSpeciale()!=null;
		double rayon = cercle.getRayon();
		int nbrePoints = polygone.getNombrePoints();
		Vecteur normale = null;
		double distanceMinimum = 0.0;
		
		for(int i = 0 ; i < nbrePoints ; i++) {
			Vecteur pointPrecedent = polygone.getPoint(i);
			Vecteur pointActuel = polygone.getPoint((i + 1) % nbrePoints);
			Vecteur meilleurPoint = trouverMeilleurPoint(centreCercle, pointPrecedent, pointActuel);
			Vecteur distance = centreCercle.sub(meilleurPoint);
			double longueurDistance = distance.longueur();
			
			if(longueurDistance < rayon) {
				if(longueurDistance < distanceMinimum || normale == null) {
					normale = distance.unitaire();
					distanceMinimum = longueurDistance;
				}
			}
		}
		if(contenu && normaleSpeciale)
			normale=polygone.getNormaleSpeciale().mul(-1);
		if(normale != null) {
			double profondeur;
			if(contenu && !normaleSpeciale) {
				normale = normale.mul(-1);
				profondeur = distanceMinimum + rayon;
			} else {
				profondeur = rayon - distanceMinimum;
			}
			return new Intersection(normale, profondeur);
		} else {
			return null;
		}
	}
	
	/**
	 * Trouve le "meilleur point" pour d�terminer la distance entre un cercle et un segment, c'est-�-dire le point le plus pr�s du centre.
	 * @param centreCercle : Centre du cercle.
	 * @param p1 : Point 1 du segment.
	 * @param p2 : Point 2 du segment.
	 * @return Le point le plus pr�s du cercle.
	 */
	private static Vecteur trouverMeilleurPoint(Vecteur centreCercle, Vecteur p1, Vecteur p2) {
		Vecteur segment = p2.sub(p1);
		Vecteur directionSegment = segment.unitaire();
		double longueurSegment = segment.longueur();
		Vecteur distanceP1 = centreCercle.sub(p1);
		double projection = distanceP1.produitScalaire(directionSegment);
		
		if(projection < 0.0) {
			return p1;
		} else if(projection > longueurSegment) {
			return p2;
		} else {
			return p1.add(directionSegment.mul(projection));
		}
	}
	
	/**
	 * Trouve les points de contact de la collision (un seul en r�alit�).
	 * @param intersection : Intersection.
	 * @param cercle : Cercle intervenant dans la collision.
	 * @return Les points de contact.
	 */
	public static ArrayList<Vecteur> trouverPointsDeContact(Intersection intersection, Cercle cercle) {
		Vecteur normale = intersection.getAxe();
		Vecteur centre = cercle.getPosition();
		double rayon = cercle.getRayon();
		Vecteur point = centre.add(normale.mul(-1.0 * rayon));
		ArrayList<Vecteur> points = new ArrayList<Vecteur>();
		points.add(point);
		return points;
	}
	
	/**
	 * V�rifie s'il y a une collision entre un polygone et un cercle.
	 * @param polygone : Polygone
	 * @param cercle : Cercle
	 * @return L'information sur la collision s'il y en a une ou null s'il n'y en a pas.
	 */
	public static InfoCollision testerCollision(Polygone polygone, Cercle cercle) {
		Intersection intersection = testerIntersection(polygone, cercle);
		ArrayList<Vecteur> points = trouverPointsDeContact(intersection, cercle);
		
		if(intersection != null) {
			return new InfoCollision(intersection.getAxe(), intersection.getProfondeur(), points);
		} else {
			return null;
		}
	}
}
