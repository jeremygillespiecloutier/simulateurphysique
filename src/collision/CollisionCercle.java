package collision;

import java.io.Serializable;
import java.util.ArrayList;

import maths.Vecteur;
import formes.Cercle;

/**
 * @author Francis Labelle
 * Classe prenant en charge les collisions entre les cercles.
 */
public class CollisionCercle implements Serializable{
	private static final long serialVersionUID = -7783306105888607405L;
	private static final double EPSILON=0.000001;

	/**
	 * V�rifie s'il y a une intersection entre deux cercles.
	 * @param a : Cercle 1
	 * @param b : Cercle 2
	 * @return L'intersection ou null s'il n'y en a pas.
	 */
	public static Intersection testerIntersection(Cercle a, Cercle b) {
		if(a.getPosition().equals(b.getPosition()))
			a.setPosition(new Vecteur(a.getPosition().getX()+EPSILON, a.getPosition().getY()+EPSILON));
		Vecteur distance = b.getPosition().sub(a.getPosition());
		double normeDistance = distance.longueur();
		double sommeRayons = a.getRayon() + b.getRayon();
		
		if(normeDistance < sommeRayons) {
			return new Intersection(distance.unitaire(), sommeRayons - normeDistance);
		} else {
			return null;
		}
	}

	/**
	 * Trouve les points de contact de la collision entre deux cercles (un seul point en r�alit�).
	 * @param intersection : L'intersection.
	 * @param cercle : Le cercle duquel "sort" la normale (cercle A).
	 * @return Les points de contact.
	 */
	public static ArrayList<Vecteur> trouverPointsDeContact(Intersection intersection, Cercle cercle) {
		ArrayList<Vecteur> points = new ArrayList<Vecteur>();
		Vecteur normale = intersection.getAxe();
		Vecteur centre = cercle.getPosition();
		
		points.add(centre.add(normale.mul(cercle.getRayon())));
		
		return points;
	}
	
	/**
	 * V�rifie s'il y a une collision entre deux cercles.
	 * @param a : Cercle 1
	 * @param b : Cercle 2
	 * @return L'information sur la collision s'il y en a une ou null s'il n'y en a pas.
	 */
	public static InfoCollision testerCollision(Cercle a, Cercle b) {
		Intersection intersection = testerIntersection(a, b);
		Vecteur axe;
		double profondeur;
		ArrayList<Vecteur> points;

		if(intersection != null) {
			axe = intersection.getAxe();
			profondeur = intersection.getProfondeur();
			points = trouverPointsDeContact(intersection, a);
			return new InfoCollision(axe, profondeur, points);
		} else {
			return null;
		}
	}
}
