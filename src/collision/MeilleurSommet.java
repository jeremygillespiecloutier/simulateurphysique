package collision;

import java.io.Serializable;

import maths.Vecteur;

/**
 * @author Francis Labelle
 * Classe repr�sentant le sommet d'un polygone �tant le plus utile � la d�tection de collisions
 * (celui qui est le plus enfonc� dans l'autre polygone).
 */
public class MeilleurSommet implements Serializable{
	private static final long serialVersionUID = 4510568343835201005L;
	private Vecteur point;
	private int indice;
	
	/**
	 * Construit un meilleur sommet.
	 * @param point : Le sommet.
	 * @param indice : L'indice du sommet (num�ro du point dans le polygone).
	 */
	MeilleurSommet(Vecteur point, int indice) {
		this.point = point;
		this.indice = indice;
	}
	
	/**
	 * @return Le meilleur sommet.
	 */
	public Vecteur getPoint() {
		return point;
	}
	
	/**
	 * @return L'indice du point dans le polygone.
	 */
	public int getIndice() {
		return indice;
	}
}