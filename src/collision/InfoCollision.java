package collision;

import java.io.Serializable;
import java.util.ArrayList;

import maths.*;

/**
 * @author Francis Labelle
 * Classe repr�sentant une collision (la profondeur de la collision et la normale).
 */
public class InfoCollision implements Serializable{
	private static final long serialVersionUID = 6788745750043640555L;
	private Vecteur normale;
	private double profondeur = 1.0;
	private ArrayList<Vecteur> points = new ArrayList<Vecteur>();

	/**
	 * Construit l'information sur la collision.
	 * @param normale : Normale de la collision.
	 * @param profondeur : Profondeur de la collision.
	 * @param points : Les points intervenant dans cette collision.
	 */
	public InfoCollision(Vecteur normale, double profondeur, ArrayList<Vecteur> points) {
		this.normale = new Vecteur(normale);
		this.profondeur = profondeur;
		this.points = points;
	}

	/**
	 * @return La normale de la collision.
	 */
	public Vecteur getNormale() {
		return this.normale;
	}

	/**
	 * @return La profondeur de la collision.
	 */
	public double getProfondeur() {
		return this.profondeur;
	}

	/**
	 * @return Les points intervenant dans la collision.
	 */
	public ArrayList<Vecteur> getPoints() {
		//Copier pour �viter que le tableau soit modifi� de l'ext�rieur.
		return new ArrayList<Vecteur>(points);
	}
}
