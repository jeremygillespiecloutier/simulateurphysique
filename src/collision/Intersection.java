package collision;

import java.io.Serializable;

import maths.Vecteur;

/**
 * @author Francis Labelle
 * Classe repr�sentant une intersection (axe et profondeur).
 */
public class Intersection implements Serializable{
	private static final long serialVersionUID = -329176764467838383L;
	private Vecteur axe;
	private double profondeur;

	/**
	 * Construit une intersection � partir de l'axe et la profondeur donn�s.
	 * @param axe : Axe de l'intersection.
	 * @param profondeur : Profondeur de l'intersection.
	 */
	Intersection(Vecteur axe, double profondeur) {
		this.axe = axe;
		this.profondeur = profondeur;
	}

	/**
	 * @return L'axe de l'intersection.
	 */
	public Vecteur getAxe() {
		return axe;
	}

	/**
	 * @return La profondeur de l'intersection.
	 */
	public double getProfondeur() {
		return profondeur;
	}
}