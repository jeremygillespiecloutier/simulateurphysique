package collision;

import java.io.Serializable;
import java.util.ArrayList;

import maths.Projection;
import maths.Vecteur;
import formes.Cote;
import formes.Polygone;

/**
 * @author Francis Labelle
 * Classe prenant en charge les collisions entre les polygones.
 */
public class CollisionPolygone implements Serializable{
	private static final long serialVersionUID = 5837515818103421392L;
	/**
	 * V�rifie s'il y a une intersection entre deux polygones.
	 * @param a : Polygone 1
	 * @param b : Polygone 2
	 * @return L'intersection ou null s'il n'y en a pas. L'axe (ou normale) de s�paration pointe toujours de A vers B.
	 */
	public static Intersection testerIntersection(Polygone a, Polygone b) {
		Vecteur axes[][] = {a.getAxesProjection(), b.getAxesProjection()};
		Vecteur axe, axeDeSeparation = null;
		Projection p1, p2;
		double profondeur = 0.0, profondeurMinimum = -1.0;
		ArrayList<Vecteur> axesTestes = new ArrayList<Vecteur>();
		
		for(int i = 0; i < axes.length; i++) {
			for(int j = 0; j < axes[i].length; j++) {
				axe = axes[i][j];

				if(!axesTestes.contains(axe)) {
					p1 = a.projeter(axe);
					p2 = b.projeter(axe);
					profondeur = p1.chevauche(p2);

					if(profondeur < 0.0) {
						return null;
					} else if(profondeur < profondeurMinimum || profondeurMinimum < 0.0) {
						profondeurMinimum = profondeur;
						axeDeSeparation = axe;
					}
				}
			}
		}
		
		if(axeDeSeparation != null) {
			//Parfois, quand il y a deux axes parall�les, la normale est fauss�e.
			//C'est parce que les deux c�t�s parall�les donnent une projection �quivalente.
			//Toutefois, ils ont une normale oppos�e puisque les axes de projection "sortent" toujours du polygone.
			//C'est pourquoi on v�rifie si la normale est bien dans la direction de B par rapport � A.
			//Si ce n'est pas le cas, cela signifie qu'on a pris une mauvaise normale, on prend donc la normale oppos�e.
			//
			//Deux vecteurs ne sont pas dans la m�me "direction g�n�rale"
			//si leur produit scalaire est inf�rieur � 0 <==> si l'angle entre les deux est entre 90 et 270 degr�s.
			//
			Vecteur distance = b.getPosition().sub(a.getPosition());
			if(axeDeSeparation.produitScalaire(distance) < 0.0) {
				axeDeSeparation = axeDeSeparation.mul(-1);
			}
			
			return new Intersection(axeDeSeparation, profondeurMinimum);
		} else {
			return null;
		}
	}
	
	/**
	 * V�rifie s'il y a une collision entre deux polygones.
	 * @param a : Polygone 1
	 * @param b : Polygone 2
	 * @return L'information sur la collision s'il y en a une ou null s'il n'y en a pas.
	 */
	static InfoCollision testerCollision(Polygone a, Polygone b) {
		Intersection intersection = testerIntersection(a, b);
		MeilleurSommet sommet1, sommet2;
		Cote cote1, cote2;
		ArrayList<Vecteur> pointsDeContact;
		Vecteur axe = intersection.getAxe();
		double profondeur = intersection.getProfondeur();

		Sommet1 = sommet1 = trouverMeilleurSommet(a, axe);
		Cote1 = cote1 = trouverMeilleurCote(a, sommet1, axe);

		// L'axe doit �tre invers� pour le c�t� B car l'axe pointe de A vers B.
		Sommet2 = sommet2 = trouverMeilleurSommet(b, axe.mul(-1));
		Cote2 = cote2 = trouverMeilleurCote(b, sommet2, axe.mul(-1));
		if(b.getNormaleSpeciale() != null)
			axe = b.getNormaleSpeciale();
		
		pointsDeContact = trouverPointsDeContact(a, b, cote1, cote2, sommet1, sommet2, axe);

		return new InfoCollision(axe, profondeur, pointsDeContact);
	}

	static public MeilleurSommet Sommet1;
	static public MeilleurSommet Sommet2;

	/**
	 * D�termine le sommet le plus pertinent du polygone pour effectuer la d�tection des points de contact.
	 * Consiste � d�terminer quel point se trouve le plus loin sur l'axe de s�paration.
	 * @param polygone : Polygone auquel il faut trouver un sommet.
	 * @param axeDeSeparation : Axe de moindre s�paration.
	 * @return Le "meilleur sommet" du polygone.
	 */
	static MeilleurSommet trouverMeilleurSommet(Polygone polygone, Vecteur axeDeSeparation) {
		MeilleurSommet sommet = null;
		double maximum = 0.0;
		double projection;

		for(int i = 0; i < polygone.getNombrePoints(); i++) {
			Vecteur point = polygone.getPoint(i);
			projection = axeDeSeparation.produitScalaire(point);

			if(i == 0 || projection > maximum) {
				maximum = projection;
				sommet = new MeilleurSommet(point, i);
			}
		}

		return sommet;
	}

	static public Cote Cote1;
	static public Cote Cote2;

	/**
	 * D�termine le c�t� le plus pertinent du polygone pour effectuer la d�tection des points de contact.
	 * @param polygone : Polygone auquel il faut trouver un c�t�.
	 * @param meilleurSommet : Meilleur sommet du polygone.
	 * @param axeDeSeparation : Axe de moindre s�paration.
	 * @return Le c�t� le plus pertinent pour effectuer la d�tection des points de contact.
	 */
	static Cote trouverMeilleurCote(Polygone polygone, MeilleurSommet meilleurSommet, Vecteur axeDeSeparation) {
		Vecteur sommet = meilleurSommet.getPoint();
		int indice = meilleurSommet.getIndice();
		int indicePrecedent = (indice == 0) ? polygone.getNombrePoints() - 1 : indice - 1;
		int indiceSuivant = (indice + 1) % polygone.getNombrePoints();

		Vecteur pointPrecedent = polygone.getPoint(indicePrecedent);
		Vecteur pointSuivant = polygone.getPoint(indiceSuivant);

		// cotePrecedent et coteSuivant doivent pointer le meilleur sommet
		Vecteur cotePrecedent = sommet.sub(pointPrecedent).unitaire();
		Vecteur coteSuivant = sommet.sub(pointSuivant).unitaire();

		double produitPrecedent = cotePrecedent.produitScalaire(axeDeSeparation);
		double produitSuivant = coteSuivant.produitScalaire(axeDeSeparation);

		if(produitSuivant < produitPrecedent) {
			return new Cote(sommet, pointSuivant);
		} else {
			return new Cote(pointPrecedent, sommet);
		}
	}

	/**
	 * "Coupe" chacun des points pass�s en param�tre s'ils ne d�passent pas la position donn�e sur l'axe.
	 * Si un seul des points est coup�, on en trouve un autre pour le remplacer.
	 * @param point1 : Point 1 � couper
	 * @param point2 : Point 2 � couper
	 * @param axe : Axe sur lequel la comparaison doit �tre faite.
	 * @param positionSurAxe : Position que les points doivent atteindre pour ne pas �tre coup�s.
	 * @return Les points restants.
	 */
	static ArrayList<Vecteur> couper(Vecteur point1, Vecteur point2, Vecteur axe, double positionSurAxe) {
		ArrayList<Vecteur> pointsRestant = new ArrayList<Vecteur>();

		double positionPoint1 = point1.produitScalaire(axe);
		double positionPoint2 = point2.produitScalaire(axe);
		double distancePoint1 = positionPoint1 - positionSurAxe;
		double distancePoint2 = positionPoint2 - positionSurAxe;

		if(distancePoint1 >= 0.0) {
			pointsRestant.add(point1);
		}

		if(distancePoint2 >= 0.0) {
			pointsRestant.add(point2);
		}

		if(distancePoint1 * distancePoint2 < 0.0) {
			Vecteur cote = point2.sub(point1);
			double facteur = distancePoint1 / (distancePoint1 - distancePoint2);
			Vecteur nouveauPoint = point1.add(cote.mul(facteur));
			pointsRestant.add(nouveauPoint);
		}

		return pointsRestant;
	}

	/**
	 * Trouve les points de contact entre les polygones A et B avec les c�t�s, les sommets et l'axe donn�s.
	 * @param a : Polygone A.
	 * @param b : Polygone B.
	 * @param coteA : Meilleur c�t� du polygone A.
	 * @param coteB : Meilleur c�t� du polygone B.
	 * @param sommetA : Meilleur sommet du polygone A.
	 * @param sommetB : Meilleur sommet du polygone B.
	 * @param axe : Axe de moindre s�paration.
	 * @return Les points de contact entre les polygones A et B.
	 */
	static ArrayList<Vecteur> trouverPointsDeContact(Polygone a, Polygone b, Cote coteA, Cote coteB, MeilleurSommet sommetA, MeilleurSommet sommetB, Vecteur axe) {
		ArrayList<Vecteur> points = new ArrayList<Vecteur>();

		double produitScalaire1 = coteA.getDirection().produitScalaire(axe);
		double produitScalaire2 = coteB.getDirection().produitScalaire(axe);
		Cote coteIncident;
		Cote coteReference;
		MeilleurSommet sommetReference;
		boolean inverser = false;

		if(Math.abs(produitScalaire1) <= Math.abs(produitScalaire2)) {
			coteReference = coteA;
			sommetReference = sommetA;
			coteIncident = coteB;
		} else {
			coteReference = coteB;
			sommetReference = sommetB;
			coteIncident = coteA;
			inverser = true;
		}

		Vecteur incidentPoint1 = coteIncident.getPoint1();
		Vecteur incidentPoint2 = coteIncident.getPoint2();
		Vecteur referencePoint1 = coteReference.getPoint1();
		Vecteur referencePoint2 = coteReference.getPoint2();
		Vecteur axeDeReference = coteReference.getDirection();
		
		Vecteur normaleAReference = axeDeReference.croix(-1.0);
		double point1SurAxe = referencePoint1.produitScalaire(axeDeReference);
		double point2SurAxe = referencePoint2.produitScalaire(axeDeReference);
		double hauteurMaximum;

		points.add(incidentPoint1);
		points.add(incidentPoint2);
		
		
		points = couper(points.get(0), points.get(1), axeDeReference, point1SurAxe);
		if(points.size() < 2) {
			return points;
		}

		points = couper(points.get(0), points.get(1), axeDeReference.mul(-1), -point2SurAxe);
		if(points.size() < 2) {
			return points;
		}
		
		if(inverser) {
			normaleAReference = normaleAReference.mul(1);
		}

		hauteurMaximum = sommetReference.getPoint().produitScalaire(normaleAReference);
		
		int indice = 1;
		if(normaleAReference.produitScalaire(points.get(0)) - hauteurMaximum < 0.0) {
			indice = 0;
			points.remove(0);
		}
		
		if(normaleAReference.produitScalaire(points.get(indice)) - hauteurMaximum < 0.0) {
			points.remove(indice);
		}
		
		return points;
	}
}
