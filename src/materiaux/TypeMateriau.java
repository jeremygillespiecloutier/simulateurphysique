package materiaux;

import java.io.Serializable;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Énumération des types de matériaux.
 *
 */
public enum TypeMateriau implements Serializable{
	bois, glace, metal, brique, abstrait, pierre;
}
