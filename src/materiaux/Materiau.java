package materiaux;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'obtenir les valeurs num�riques associ�es uax propri�t�s physiques d'objets
 * en fonction du mat�riau les composant.
 */
public class Materiau implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1661227560737448559L;
	private static BufferedImage bois;
	private static BufferedImage glace;
	private static BufferedImage metal;
	private static BufferedImage brique;
	private static BufferedImage abstrait;
	private static BufferedImage pierre;
	private static ArrayList<TypeMateriau> materiaux = new ArrayList<TypeMateriau>(Arrays.asList(new TypeMateriau[] { TypeMateriau.bois, TypeMateriau.metal, TypeMateriau.glace }));
	private static double[][] restitutions = { { 0.6, 0.5, 0.4 }, { 0.5, 0.2, 0.37 }, { 0.4, 0.37, 0.65 } };
	private static double[][] statiques = { { 0.8, 0.75, 0.45 }, { 0.75, 0.85, 0.55 }, { 0.45, 0.55, 0.3 } };
	private static double[][] cinetiques = { { 0.4, 0.35, 0.15 }, { 0.35, 0.45, 0.2 }, { 0.15, 0.2, 0.1 } };
	private static double[] densites = { 540, 2720, 917 };
	/**
	 * Charge les images utilis�es dans l'affichage des textures selon le type de mat�riau.
	 */
	public static void charger() {
		try {
			glace = ImageIO.read(Materiau.class.getClassLoader().getResource("glace.jpg"));
			bois = ImageIO.read(Materiau.class.getClassLoader().getResource("bois.jpg"));
			metal = ImageIO.read(Materiau.class.getClassLoader().getResource("metal.jpg"));
			brique = ImageIO.read(Materiau.class.getClassLoader().getResource("brique.jpg"));
			abstrait = ImageIO.read(Materiau.class.getClassLoader().getResource("abstrait.jpg"));
			pierre = ImageIO.read(Materiau.class.getClassLoader().getResource("pierre.jpg"));
		} catch(IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Retourne la texture repr�sentant un type de mat�riau.
	 * @param type Le type de mat�riau.
	 * @return La texture repr�sentant le mat�riau.
	 */
	public static BufferedImage getTexture(TypeMateriau type) {
		if(type == TypeMateriau.bois) {
			return bois;
		} else if(type == TypeMateriau.glace) {
			return glace;
		} else if(type == TypeMateriau.metal) {
			return metal;
		} else if(type == TypeMateriau.brique) {
			return brique;
		} else if(type == TypeMateriau.abstrait) {
			return abstrait;
		} else if(type == TypeMateriau.pierre) {
			return pierre;
		}
		return null;
	}
	/**
	 * Retourne le coefficient de restitution pour la collision entre deux mat�riaux.
	 * @param type1 Le premier type de mat�riau.
	 * @param type2 Le second type de mat�riau.
	 * @return Le coefficient de restitution.
	 */
	public static double getRestitution(TypeMateriau type1, TypeMateriau type2) {
		return restitutions[materiaux.indexOf(type1)][materiaux.indexOf(type2)];
	}
	/**
	 * Retourne le coefficient de frottement statique entre deux mat�riaux.
	 * @param type1 Le premier mat�riau.
	 * @param type2 Le second mat�raiu.
	 * @return Le coefficient de frottement statique.
	 */
	public static double getStatique(TypeMateriau type1, TypeMateriau type2) {
		return statiques[materiaux.indexOf(type1)][materiaux.indexOf(type2)];
	}
	/**
	 * Retourn le coefficient de frottement cin�tique entre deux mat�riaux.
	 * @param type1 Le premier mat�riau.
	 * @param type2 Le second mat�riau.
	 * @return Le coefficient de frottement cin�tique.
	 */
	public static double getCinetique(TypeMateriau type1, TypeMateriau type2) {
		return cinetiques[materiaux.indexOf(type1)][materiaux.indexOf(type2)];
	}
	/**
	 * Retourn la densit� du mat�riau.
	 * @param type Le type de mat�riau.
	 * @return La densit� du mat�riau.
	 */
	public static double getDensite(TypeMateriau type) {
		return densites[materiaux.indexOf(type)];
	}
}
