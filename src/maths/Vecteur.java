package maths;

import java.io.Serializable;

/**
 * @author Francis Labelle
 * Classe de vecteur utilis�e pour les calculs math�matiques.
 */
public class Vecteur implements Serializable{
	private static final long serialVersionUID = -8260274102420389768L;
	double x = 0.0;
	double y = 0.0;
	
	/**
	 * Constructeur par d�faut des vecteurs.
	 * Initialise le vecteur � (0, 0).
	 */
	public Vecteur() {
	}
	
	/**
	 * Constructeur de la classe Vecteur.
	 * @param x La coordonn�e en x du vecteur.
	 * @param y La coordonn�e en y du vecteur.
	 */
	public Vecteur(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Constructeur de la classe Vecteur.
	 * @param v Le vecteur d'initialisation.
	 */
	public Vecteur(Vecteur v) {
		this.x = v.getX();
		this.y = v.getY();
	}
	
	/**
	 * Retourne l'addition de ce vecteur � un autre vecteur.
	 * @param v Le vecteur � additionner.
	 * @return L'addition des vecteurs.
	 */
	public Vecteur add(Vecteur v) {
		return new Vecteur(x + v.getX(), y + v.getY());
	}
	
	/**
	 * Retourne la soustraction de ce vecteur � un autre vecteur.
	 * @param v Le vecteur � soustraire.
	 * @return La soustraction des vecteurs.
	 */
	public Vecteur sub(Vecteur v) {
		return new Vecteur(x - v.getX(), y - v.getY());
	}
	
	/**
	 * Retourne la multiplication de ce vecteur avec un nombre r�el.
	 * @param k Le nombre r�el � mutiplier. 
	 * @return Le vecteur multipli�.
	 */
	public Vecteur mul(double k) {
		return new Vecteur(k * x, k * y);
	}
	
	/**
	 * Retourne la division de ce vecteur avec un nombre r�el.
	 * @param k Le nombre r�el � diviser.
	 * @return Le vecteur divis�.
	 */
	public Vecteur div(double k) {
		return new Vecteur(x / k, y / k);
	}
	
	/**
	 * Retourne le module du vecteur.
	 * @return Le module du vecteur.
	 */
	public double longueur() {
		double longueur = Math.sqrt(x * x + y * y);
		return Double.isNaN(longueur) ? 0 : longueur;
	}
	
	/**
	 * Retourne le produit scalaire entre ce vecteur et un autre vecteur.
	 * @param v L'autre vecteur.
	 * @return Le produit scalaire des vecteurs.
	 */
	public double produitScalaire(Vecteur v) {
		return x * v.getX() + y * v.getY();
	}
	
	/**
	 * Retourne le produit croix entre ce vecteur et un autre vecteur.
	 * @param v L'autre vecteur.
	 * @return Le produit croix
	 */
	public double produitCroix(Vecteur v) {
		return x * v.getY() - y * v.getX();
	}
	
	/**
	 * Retourne le vecteur produit croix entre ce vecteur et un nombre r�el.
	 * @param d Le nombre r�el avec lequel on effectue le produit croix.
	 * @return Le vecteur produit croix.
	 */
	public Vecteur croix(double d) {
		return new Vecteur(this.y * -d, this.x * d);
	}
	
	/**
	 * Retourne un vecteur perpendiculaire � ce vecteur.
	 * @return Le vecteur perpendiculaire.
	 */
	public Vecteur perpendiculaire() {
		return new Vecteur(-y, x);
	}
	
	/**
	 * Retourne le vecteur unitaire du vecteur.
	 * @return Le vecteur unitaire.
	 */
	public Vecteur unitaire() {
		return this.div(longueur());
	}
	
	/**
	 * Retourne la coordonn�e en x du vecteur.
	 * @return La coordonn�e en x.
	 */
	public double getX() {
		return x;
	}
	
	/**
	 * Retourne la coordonn�e en y du vecteur.
	 * @return La coordonn�e en y du vecteur.
	 */
	public double getY() {
		return y;
	}
	
	/**
	 * Retourne une chaine de caract�re repr�sentant le vecteur.
	 * @return Une chaine de caract�re repr�sentant le vecteur.
	 */
	public String toString() {
		return "( x = " + x + ", y = " + y + " )";
	}
	
	/**
	 * D�termine si ce vecteur est �gal � un autre vecteur.
	 * @param obj L'objet � comparer.
	 * @return Bool�en indiquant si le vecteur est identique � l'objet.
	 */
	public boolean equals(Object obj) {
		if (obj.getClass() == this.getClass()) {
			Vecteur autre = (Vecteur) obj;
			return autre.getX() == this.x && autre.getY() == this.y;
		} else {
			return false;
		}
	}
	
	/**
	 * Retourne l'orientation du vecteur.
	 * @return L'orientation du vecteur.
	 */
	public double getOrientation() {
		double arc = Math.atan(y / x);
		if(x < 0.0) {
			return arc + Math.PI;
		} else {
			return arc;
		}
	}
	
	/**
	 * M�thode qui v�rifie si les coordonn�s du vecteur sont des nombres r�els.
	 * @return Bool�en indiquant si les coordonn�es du vecteur sont des nombres r�els.
	 */
	public boolean isNaN(){
		return Double.isNaN(x) || Double.isNaN(y);
	}
}
