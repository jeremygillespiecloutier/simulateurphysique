package maths;

import java.io.Serializable;

/**
 * @author Francis Labelle
 * Classe repr�sentant la projection d'une forme sur un axe.
 */
public class Projection implements Serializable{
	private static final long serialVersionUID = -7827051848001689763L;
	private double min, max;

	
	/**
	 * Construit une projection � partir de p1 et p2.
	 * @param p1 : point 1 de la projection.
	 * @param p2 : point 2 de la projection.
	 */
	public Projection(double p1, double p2) {
		this.min = Math.min(p1, p2);
		this.max = Math.max(p1, p2);
	}

	/**
	 * V�rifie si la projection en touche une autre.
	 * 
	 * @param p : Autre projection.
	 * @return La profondeur de l'intersection. La valeur retourn�e est n�gative
	 *         s'il n'y a pas d'intersection.
	 */
	public double chevauche(Projection p) {
		return Math.min(getMax(), p.getMax()) - Math.max(getMin(), p.getMin());
	}
	
	/**
	 * Minimum des deux points de la projection.
	 * @return Minimum des deux points de la projection.
	 */
	public double getMin() {
		return min;
	}
	
	/**
	 * Maximum des deux points de la projection.
	 * @return Maximum des deux points de la projection.
	 */
	public double getMax() {
		return max;
	}
}
