package maths;

import java.io.Serializable;

/**
 * @author Francis Labelle
 * Classe servant � repr�senter certaines op�rations sous forme de cha�nes de caract�res.
 */
public class Conversion implements Serializable{
	private static final long serialVersionUID = -601475547755257785L;

	/**
	 * Repr�sente une addition sous forme de cha�ne de caract�res.
	 * @param x : Chiffre � additionner.
	 * @return La repr�sentation de l'addition.
	 */
	public static String strAddition(double x) {
		if(x == 0.0) {
			return "";
		} else if(x > 0.0) {
			return " + " + x;
		} else {
			return " - " + -x;
		}
	}
	
	/**
	 * Repr�sente une multiplication sous forme de cha�ne de caract�res.
	 * @param x : Chiffre � multiplier.
	 * @return La repr�sentation de la multiplication.
	 */
	public static String strMultiplication(double x) {
		if(x == 1.0) {
			return "";
		} else if(x == -1.0) {
			return "-";
		} else {
			return "" + x;
		}
	}
}
