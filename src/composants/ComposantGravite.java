package composants;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.*;

import javax.swing.*;

import dessinable.VecteurDessinable;
import ecouteurs.EcouteurGravite;
import maths.Vecteur;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe dialogue permettant de s�lectionner l'orientation d'un vecteur.
 */
public class ComposantGravite extends JPanel implements MouseMotionListener{
	
	private static final long serialVersionUID = 5632114172339056974L;
	private Vecteur position=new Vecteur(75, 75);
	private final double NORME=75;
	private VecteurDessinable gravite=new VecteurDessinable(new Vecteur(0, NORME), position);
	EcouteurGravite ecouteur;
	private AffineTransform mat;
	private boolean premiereFois=true;
	private double orientation=Math.PI/2.0;
	private Color couleurFond=Color.LIGHT_GRAY;
	/**
	 * Constructeur du composant
	 * @param ecouteur L'�couteur qui sera appel�e lorsque l'orientation du vecteur du composant est chang�.
	 */
	public ComposantGravite(EcouteurGravite ecouteur) {
		this.ecouteur=ecouteur;
		this.addMouseMotionListener(this);
		gravite.setTailleTrait(2);
	}
	/**
	 * M�thode de dessin du composant
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		setFocusable(false);
		Graphics2D g2d=(Graphics2D)g;
		if(premiereFois){
			premiereFois=false;
			mat=new AffineTransform();
		}
		Color couleurInitiale=g2d.getColor();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setColor(couleurFond);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.setColor(Color.BLACK);
		g2d.setStroke(new BasicStroke(2.0f));
		g2d.draw(new Ellipse2D.Double(0, 0, getWidth(), getHeight()));
		gravite.dessiner(g2d, mat, 0);
		g2d.setColor(couleurInitiale);
	}
	/**
	 * M�thode appel� au drag de la souris. Change l'orientation du vecteur.
	 */
	public void mouseDragged(MouseEvent arg0) {
		orientation=new Vecteur(arg0.getX()-position.getX(), arg0.getY()-position.getY()).getOrientation();
		gravite.setVecteur(new Vecteur(Math.cos(orientation)*NORME, Math.sin(orientation)*NORME));
		ecouteur.orientationChangee(2*Math.PI-orientation);
		repaint();
	}
	public void mouseMoved(MouseEvent arg0) {
		
	}
	/**
	 * Change la couleur de fond du composant.
	 * @param couleurFond La couleur de fond du composant.
	 */
	public void setCouleurFond(Color couleurFond){
		this.couleurFond=couleurFond;
	}
	/**
	 * Change l'orientation du vecteur du composant.
	 * @param orientation L'orientation du vecteur du composant.
	 */
	public void setOrientation(double orientation){
		orientation=2*Math.PI-orientation;
		this.orientation=orientation;
		gravite.setVecteur(new Vecteur(Math.cos(orientation)*NORME, Math.sin(orientation)*NORME));
		repaint();
	}
	/**
	 * Retourne l'orientation du vecteur du composant.
	 * @return L'orientation du vecteur du composant.
	 */
	public double getOrientation(){
		return 2*Math.PI-this.orientation;
	}
}
