package composants;

import javax.swing.JPanel;

import java.awt.Rectangle;

import javax.swing.JLabel;

import moteur.InfoAnimation;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant d'afficher les propri�t�s d'objets de type InfoAnimation.
 */
public class ComposantAnimation extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9019756447119493140L;
	private JLabel lblTemps;
	private JLabel lblFreqCible;
	private JLabel lblFreq;
	private JLabel lblDeltaT;
	private JLabel lblEcart;
	private JLabel lblTempsReel;
	/**
	 * Create the panel.
	 */
	public ComposantAnimation() {
		setBounds(new Rectangle(0, 0, 300, 70));
		setLayout(null);
		
		lblTemps = new JLabel("Temps \u00E9coul\u00E9: 0.0s");
		lblTemps.setBounds(10, 6, 140, 14);
		add(lblTemps);
		
		lblFreqCible = new JLabel("Fr\u00E9quence image cible: 60 ips");
		lblFreqCible.setBounds(10, 27, 198, 14);
		add(lblFreqCible);
		
		lblFreq = new JLabel("Fr\u00E9quence image: 60 ips");
		lblFreq.setBounds(10, 50, 171, 14);
		add(lblFreq);
		
		lblDeltaT = new JLabel("Delta t: 0.03s");
		lblDeltaT.setBounds(199, 27, 91, 14);
		add(lblDeltaT);
		
		lblEcart = new JLabel("\u00C9cart : 0%");
		lblEcart.setBounds(185, 50, 105, 14);
		add(lblEcart);
		
		lblTempsReel = new JLabel("Temps r\u00E9el: 0s");
		lblTempsReel.setBounds(160, 6, 140, 14);
		add(lblTempsReel);

	}
	/**
	 * M�thode permettant d'actualiser les paneau avec les nouvelles informations relatives � l'animation.
	 * @param info L'objet contentant les informations relatives � l'animation.
	 */
	public void actualiser(InfoAnimation info){
		lblTemps.setText("Temps \u00E9coul\u00E9: "+ Math.floor(info.getTempsEcoule()*100)/100.0+" s");
		lblFreqCible.setText("Fr\u00E9quence image cible: "+Math.floor(info.getFreqCible()*100)/100.0+" ips");
		lblFreq.setText("Fr\u00E9quence image: "+Math.floor(info.getFreq()*100)/100.0+" ips");
		lblDeltaT.setText("Delta t: "+Math.floor(info.getDeltaT()*1000)/1000.0+" s");
		lblEcart.setText("\u00C9cart : "+Math.floor(Math.abs((info.getFreqCible()-info.getFreq())/info.getFreqCible()*100)*100)/100.0+"%");
		lblTempsReel.setText("Temps r\u00E9el: "+Math.floor(info.getTempsReel()*100)/100.0+"s");
		repaint();
	}
}
