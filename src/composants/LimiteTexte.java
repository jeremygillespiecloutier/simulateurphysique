package composants;

import javax.swing.text.*;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe permettant de limiter le nombre de caract�res saisis dans une boite de texte.
 */
public class LimiteTexte extends PlainDocument {
	private static final long serialVersionUID = 8136821672105258680L;
	private int limit;
    /**
     * Constructeur
     * @param limit La limite repr�sentant le nombre de caract�res maximal pouvant �tres contenus dans la bo�te de texte.
     */
    public LimiteTexte(int limit) {
        super();
        this.limit = limit;
    }
    /**
     * M�thode qui ajoute un caract�re seulement si le caract�re ajout� ne d�passe pas la limite �tablie.
     */
    public void insertString
            (int offset, String  str, AttributeSet attr)
            throws BadLocationException {
        if (str == null) return;
        if ((getLength() + str.length()) <= limit) {
            super.insertString(offset, str, attr);
        }
    }
}