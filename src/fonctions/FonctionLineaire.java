package fonctions;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.io.Serializable;

import maths.Vecteur;
import enums.TypeFonction;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe pour d�placer les corps avec une trajectoire de fonction lin�aire.
 */
public class FonctionLineaire extends Fonction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1925692118840283028L;
	private double pente;
	private double longueur;
	private double angle=0;
	private double position=0;
	private int augmentation=1;
	/**
	 * Constructeur de fonction lin�aire.
	 * @param f La fonction lin�aire � copier.
	 */
	public FonctionLineaire(FonctionLineaire f){
		super(f.getVitesse(), TypeFonction.LINEAIRE, f.getNom());
		this.pente=f.getPente();
		this.longueur=f.getLongueur();
		this.angle=f.getAngle();
		this.position=f.getPosition();
		this.augmentation=f.getAugmentation();
		setVelocite(f.getVelocite());
		setForme(new Line2D.Double(0, 0, longueur*Math.cos(angle), longueur*Math.sin(angle)));
	}
	/**
	 * Contructeur de fonction lin�aire.
	 * @param pente La pente de la fonction.
	 * @param longueur La longueur de la fonction.
	 * @param vitesse La vitesse de la fonction.
	 * @param nom Le nom de la fonction.
	 */
	public FonctionLineaire(double pente, double longueur, double vitesse, String nom){
		super(vitesse, TypeFonction.LINEAIRE, nom);
		this.pente=pente;
		this.longueur=longueur;
		this.angle=Math.atan(pente);
		setForme(new Line2D.Double(0, 0, longueur*Math.cos(angle), longueur*Math.sin(angle)));
		setVelocite(new Vecteur(longueur*Math.cos(angle), longueur*Math.sin(angle)).unitaire().mul(vitesse));
	}
	public Vecteur getDeplacement(double deltaT){
		double difference=getVitesse()*deltaT*augmentation;
		double positionFinale=position;
		if(position+difference>longueur){
			augmentation*=-1;
			positionFinale=longueur-(position+difference-longueur);
		}else if(position+difference<0){
			augmentation*=-1;
			positionFinale=-(position+difference);
		}else{
			positionFinale=position+difference;
		}
		double changement=positionFinale-position;
		position=positionFinale;
		Vecteur velocite=new Vecteur(changement*Math.cos(angle), changement*Math.sin(angle));
		setVelocite(velocite.unitaire().mul(getVitesse()));
		return velocite;
	}
	public String toString() {
		String n=getNom().equals("")?"":"["+getNom()+"] ";
		return n+"y="+pente+"x"+" | v="+getVitesse();
	}
	/**
	 * Retourne la pente de la fcontion.
	 * @return La pente de la fonction.
	 */
	public double getPente(){
		return this.pente;
	}
	/**
	 * Retourne la longueur de la fonction.
	 * @return La longueur de la fonction.
	 */
	public double getLongueur(){
		return this.longueur;
	}
	/**
	 * Retourne l'angle d'inclinaison de la fonction.
	 * @return L'angle d'inclinaison de la fonction.
	 */
	public double getAngle(){
		return this.angle;
	}
	/**
	 * Retourne la position du corps sur la fonction.
	 * @return La position du corps sur la fonction.
	 */
	public double getPosition(){
		return this.position;
	}
	/**
	 * Retourne le sens de d�placement actuel sur la fonction (positif ou n�gatif).
	 * @return Le sens actuel de d�placement sur la fonction.
	 */
	public int getAugmentation(){
		return this.augmentation;
	}
	public Fonction copie() {
		return new FonctionLineaire(this);
	}
	@Override
	public void dessinerFonction(Graphics2D g2d, AffineTransform mat, Vecteur pos) {
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(Color.BLUE);
		g2d.setStroke(new BasicStroke(2.0f));
		Vecteur rel=new Vecteur(position*Math.cos(angle), position*Math.sin(angle));
		mat.translate(pos.getX()-rel.getX(), pos.getY()-rel.getY());
		g2d.draw(mat.createTransformedShape(getForme()));
		mat.translate(-(pos.getX()-rel.getX()), -(pos.getY()-rel.getY()));
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	public boolean equals(Object obj){
		if(obj!=null && obj.getClass().equals(FonctionLineaire.class)){
			FonctionLineaire f=(FonctionLineaire)obj;
			return super.equals(f) && this.pente==f.getPente() && this.longueur==f.getLongueur() && this.getAngle()==f.getAngle() && this.position==f.getPosition() && this.augmentation==f.getAugmentation();
		}
		return false;
	}
}
