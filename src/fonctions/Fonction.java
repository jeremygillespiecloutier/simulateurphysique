package fonctions;

import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.io.Serializable;

import enums.TypeFonction;
import maths.Vecteur;
/**
 * 
 * @author Francis Labelle
 * Classe de base pour les fonctions.
 */
public abstract class Fonction implements Serializable {
	private static final long serialVersionUID = 5784165707921437026L;
	private double vitesse;
	private TypeFonction type;
	private String nom;
	private Shape forme = null;
	private Vecteur velocite = new Vecteur(0, 0);

	/**
	 * Constructeur de fonction.
	 * @param vitesse La vitesse de d�placement de la fonction.
	 * @param type Le type de la fonction.
	 * @param nom Le nom de la fonction.
	 */
	public Fonction(double vitesse, TypeFonction type, String nom) {
		this.vitesse = vitesse;
		this.type = type;
		this.nom = nom;
	}

	/**
	 * Retourne la vitesse de la fonction.
	 * @return La vitesse de la fonction.
	 */
	public double getVitesse() {
		return this.vitesse;
	}

	/**
	 * Retourne la v�locit� (vitesse vectorielle) actuelle de la fonction.
	 * @return La v�locit� actuelle de la fonction.
	 */
	public Vecteur getVelocite() {
		return this.velocite;
	}

	/**
	 * Change la v�locit� de la fonction.
	 * @param velocite La v�locit� de la fonction.
	 */
	public void setVelocite(Vecteur velocite) {
		this.velocite = velocite;
	}

	/**
	 * Retourne le type de la fonction.
	 * @return Le typ de la fonction.
	 */
	public TypeFonction getType() {
		return this.type;
	}

	/**
	 * Retourne le nom de la fonction.
	 * @return Le nom de la fonction.
	 */
	public String getNom() {
		return this.nom;
	}

	/**
	 * Change la forme utilis�e pour dessiner la fonction.
	 * @param f La forme utilis�e pour dessiner la fonction.
	 */
	public void setForme(Shape f) {
		this.forme = f;
	}

	/**
	 * Retourne la forme utilis�e pour dessiner la fonction.
	 * @return La forme utilis�e pour dessiner la fonction.
	 */
	public Shape getForme() {
		return this.forme;
	}

	public abstract String toString();

	/**
	 * Retourne le d�placement � effectuer par l'objet qui suit la fonction.
	 * @param deltaT La diff�rence de temps �coul�e.
	 * @return Le d�placement � effectuer.
	 */
	public abstract Vecteur getDeplacement(double deltaT);

	/**
	 * Retourne une copie de la fonction.
	 * @return Copie de la fonction.
	 */
	public abstract Fonction copie();

	/**
	 * Dessine la fonction.
	 * @param g2d L'objet graphique.
	 * @param mat La matrice de transformation.
	 * @param pos La position du corps qui suit la fonction.
	 */
	public abstract void dessinerFonction(Graphics2D g2d, AffineTransform mat, Vecteur pos);
	public boolean equals(Object obj){
		if(obj!=null && (obj instanceof Fonction)){
			Fonction f=(Fonction)obj;
			return vitesse == f.getVitesse() && type.equals(f.getType()) && nom.equals(f.getNom()) && velocite.equals(f.getVelocite());
		}
		return false;
	}
}
