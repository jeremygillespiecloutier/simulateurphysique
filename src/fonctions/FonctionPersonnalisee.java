package fonctions;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.io.Serializable;
import java.util.ArrayList;

import maths.Vecteur;
import enums.TypeFonction;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe pour d�placer les corps avec une trajectoire personnalis�e.
 */
public class FonctionPersonnalisee extends Fonction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4131816629859068727L;
	private ArrayList<Vecteur> points;
	private int index=0;
	private boolean inversion;
	private int sens=1;
	private Vecteur position=new Vecteur(0,0);
	/**
	 * Constructeur de fonction personnalis�e.
	 * @param f Fonction personnalis�e � copier.
	 */
	public FonctionPersonnalisee(FonctionPersonnalisee f) {
		super(f.getVitesse(), TypeFonction.PERSONNALISEE, f.getNom());
		this.points=new ArrayList<Vecteur>(f.getPoints());
		this.index=f.getIndex();
		this.inversion=f.getInversion();
		this.sens=f.getSens();
		this.position=f.getPosition();
		setVelocite(f.getVelocite());
		creerForme();
	}
	/**
	 * Constructeur de fonction personnalis�e.
	 * @param vitesse La vitesse de la fonction.
	 * @param points Les points � suivre par la fonction.
	 * @param inversion Bool�en indiquant si la fonction doit changer de sens quand elle arrive au bout.
	 * @param nom Le nom de la fonction.
	 */
	public FonctionPersonnalisee(double vitesse, ArrayList<Vecteur> points, boolean inversion, String nom) {
		super(vitesse, TypeFonction.PERSONNALISEE, nom);
		this.points=points;
		this.inversion=inversion;
		this.position=points.get(0);
		creerForme();
		setVelocite(points.get(1).sub(points.get(0)).unitaire().mul(vitesse));
	}
	/**
	 * Cr�e la forme de la fonction.
	 */
	private void creerForme(){
		Path2D.Double chemin=new Path2D.Double();
		chemin.moveTo(0, 0);
		for(int i=1;i<points.size();i++){
			chemin.lineTo(points.get(i).getX()-points.get(0).getX(), points.get(i).getY()-points.get(0).getY());
		}
		if(!inversion){
			chemin.closePath();
		}
		setForme(chemin);
	}
	public String toString() {
		String n=getNom().equals("")?"":"["+getNom()+"] ";
		return n+"Perso"+" | v="+getVitesse();
	}
	public Vecteur getDeplacement(double deltaT) {
		Vecteur positionInitiale=position;
		double difference=deltaT*getVitesse();
		while(difference>0){
			int suivant;
			if(index+sens==points.size()){
				if(inversion){
					suivant=index-sens;
					sens*=-1;
				}else{
					suivant=0;
				}
			}else if(index+sens==-1){
				if(inversion){
					suivant=index-sens;
					sens*=-1;
				}else{
					suivant=points.size()-1;
				}
			}else{
				suivant=index+sens;
			}
			Vecteur vSuivant=points.get(suivant);
			Vecteur deplacement=vSuivant.sub(position);
			if(difference>deplacement.longueur()){
				difference-=deplacement.longueur();
				index=suivant;
				position=points.get(index);
			}else{
				position=position.add(deplacement.unitaire().mul(difference));
				difference=0;
			}
		}
		Vecteur velocite=position.sub(positionInitiale);
		setVelocite(velocite.unitaire().mul(getVitesse()));
		return velocite;
	}
	public Fonction copie() {
		return new FonctionPersonnalisee(this);
	}
	/**
	 * Retourne les points de la fonction.
	 * @return Les points de la fonction.
	 */
	public ArrayList<Vecteur> getPoints(){
		return this.points;
	}
	/**
	 * Retourne l'index courant des points de la fonction.
	 * @return L'index courant des points de la fonction.
	 */
	public int getIndex(){
		return this.index;
	}
	/**
	 * Retourne une bool�en indiquant si la fonction change de sens.
	 * @return Bool�en indiquant si la fonction change de sens.
	 */
	public boolean getInversion(){
		return this.inversion;
	}
	/**
	 * Retourne le sens de d�placement de la fonction (positif ou n�gatif).
	 * @return Le sens de d�placement de la fonction.
	 */
	public int getSens(){
		return this.sens;
	}
	/**
	 * Retourne la position du corps sur la fonction.
	 * @return La position du corps sur la fonction.
	 */
	public Vecteur getPosition(){
		return this.position;
	}
	@Override
	public void dessinerFonction(Graphics2D g2d, AffineTransform mat, Vecteur pos) {
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(Color.BLUE);
		g2d.setStroke(new BasicStroke(2.0f));
		mat.translate(pos.getX()-position.getX()+points.get(0).getX(), pos.getY()-position.getY()+points.get(0).getY());
		g2d.draw(mat.createTransformedShape(getForme()));
		mat.translate(-(pos.getX()-position.getX()+points.get(0).getX()), -(pos.getY()-position.getY()+points.get(0).getY()));
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	public boolean equals(Object obj){
		if(obj!=null && obj.getClass().equals(FonctionPersonnalisee.class)){
			FonctionPersonnalisee f=(FonctionPersonnalisee)obj;
			return super.equals(f) && this.points.equals(f.getPoints()) && this.index == f.getIndex() && this.inversion==f.getInversion() && this.sens==f.getSens() && this.getPosition().equals(f.getPosition());
		}
		return false;
	}
}
