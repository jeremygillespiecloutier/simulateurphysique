package fonctions;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.io.Serializable;

import enums.TypeFonction;
import maths.Vecteur;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe pour d�placer les corps avec une trajectoire de fonction circulaire.
 */
public class FonctionCercle extends Fonction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5222721034647795250L;
	private double rayon;
	private double circonference;
	private double angle=0;
	private double vAng=0;
	/**
	 * Constructeur de fonction cercle.
	 * @param f La fonction cercle � copier.
	 */
	public FonctionCercle(FonctionCercle f){
		super(f.getVitesse(), TypeFonction.CERCLE, f.getNom());
		this.rayon=f.getRayon();
		this.circonference=f.getCirconference();
		this.angle=f.getAngle();
		this.vAng=f.getVAng();
		setVelocite(f.getVelocite());
		setForme(new Ellipse2D.Double(-rayon, -rayon, 2*rayon, 2*rayon));
	}
	/**
	 * Constructeur fonction cercle.
	 * @param rayon Le rayon de la fonction .
	 * @param vitesse La vitesse de la fonction.
	 * @param nom Le nom de la fonction.
	 */
	public FonctionCercle(double rayon, double vitesse, String nom) {
		super(vitesse, TypeFonction.CERCLE, nom);
		this.circonference=2*rayon*Math.PI;
		this.rayon=rayon;
		this.vAng=getVitesse()/rayon;
		setForme(new Ellipse2D.Double(-rayon, -rayon, 2*rayon, 2*rayon));
		setVelocite(new Vecteur(0, vitesse>0?1:-1).mul(Math.abs(getVitesse())));
	}
	public Vecteur getDeplacement(double deltaT) {
		Vecteur position1=new Vecteur(Math.cos(angle)*rayon, Math.sin(angle)*rayon);
		angle=(angle+vAng*deltaT)%(2*Math.PI);
		Vecteur position2=new Vecteur(Math.cos(angle)*rayon, Math.sin(angle)*rayon);
		Vecteur velocite=position2.sub(position1);
		setVelocite(velocite.unitaire().mul(Math.abs(getVitesse())));
		return velocite;
	}
	public String toString() {
		String n=getNom().equals("")?"":"["+getNom()+"] ";
		return n+rayon+"\u00B2=x\u00B2+y\u00B2"+" | v="+getVitesse();
	}
	/**
	 * Retourne le rayon de la fonction.
	 * @return Le rayon de la fonction.
	 */
	public double getRayon(){
		return this.rayon;
	}
	/**
	 * Retourne la circonf�rence de la fonction.
	 * @return La circonf�rence de la fonction.
	 */
	public double getCirconference(){
		return this.circonference;
	}
	/**
	 * Retourne l'angle sur le cercle trigonom�trique du corps par rapport � la fonction.
	 * @return L'angle de la fonction.
	 */
	public double getAngle(){
		return this.angle;
	}
	/**
	 * Retourne la vitesse angulaire de d�placement sur la fonction.
	 * @return La vitesse angulaire de d�placement.
	 */
	public double getVAng(){
		return this.vAng;
	}
	public Fonction copie() {
		return new FonctionCercle(this);
	}
	@Override
	public void dessinerFonction(Graphics2D g2d, AffineTransform mat, Vecteur pos) {
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(Color.BLUE);
		g2d.setStroke(new BasicStroke(2.0f));
		Vecteur rel=new Vecteur(rayon*Math.cos(angle), rayon*Math.sin(angle));
		mat.translate(pos.getX()-rel.getX(), pos.getY()-rel.getY());
		g2d.draw(mat.createTransformedShape(getForme()));
		mat.translate(-(pos.getX()-rel.getX()), -(pos.getY()-rel.getY()));
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	public boolean equals(Object obj){
		if(obj!=null && obj.getClass().equals(FonctionCercle.class)){
			FonctionCercle f=(FonctionCercle)obj;
			return super.equals(f) && this.rayon == f.getRayon() && this.circonference==f.getCirconference() && this.angle==f.getAngle() && this.vAng==f.getVAng();
		}
		return false;
	}
}
