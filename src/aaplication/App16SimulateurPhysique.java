package aaplication;

import inspecteurs.Inspecteur;
import inspecteurs.InspecteurExplosion;
import inspecteurs.InspecteurPolygone;
import inspecteurs.InspecteurRessort;
import inspecteurs.InspecteurScene;
import inspecteurs.InspecteurZoneDeGravite;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import materiaux.Materiau;
import maths.Vecteur;
import moteur.ObjetPhysique;
import moteur.Scene;
import moteur.Vue;
import physique.Corps;
import physique.Explosion;
import physique.Ressort;
import physique.ZoneDeGravite;
import ecouteurs.AdapteurUniversel;
import ecouteurs.EcouteurUniversel;
import moteur.InfoAnimation;
import enums.TypeAffichage;
import enums.TypeFonction;
import enums.TypeObjet;
import enums.TypeSelection;
import fonctions.Fonction;
import formes.Forme;
import composants.ComposantAnimation;
import dialogues.DialogueAPropos;
import dialogues.DialogueConcepts;
import dialogues.DialogueDemonstration;
import dialogues.DialogueInstructions;
import dialogues.DialogueJeuxEssai;
import dialogues.DialogueSources;
import dialogues.InsertionExplosion;
import dialogues.InsertionLigne;
import dialogues.InsertionPolygone;
import dialogues.InsertionRectangle;
import dialogues.InsertionSphere;
import dialogues.InsertionZone;

import java.awt.Font;
/**
 * Classe principale de l'application. Contient les �l�ments de l'interface.
 * @author Jeremy Gillespie-Cloutier (interface) et Francis Labelle (raccourcis clavier)
 *
 */
public class App16SimulateurPhysique extends JFrame implements Serializable, EcouteurUniversel{
	private static final long serialVersionUID = -2653787971627313486L;
	
	private JPanel contentPane;
	private final ButtonGroup groupeOutils = new ButtonGroup();
	private Vue vue;
	private ArrayList<Vue> vues = new ArrayList<Vue>();
	private JToggleButton tgbtnDemarrer;
	private JTabbedPane panneauScenes;
	private TypeSelection selection = TypeSelection.SOURIS;
	private App16SimulateurPhysique parent = this;
	private JList<String> listeObjets = null;
	private JToggleButton tgbtnQuadrillage;
	private boolean doitBouger = true;
	private JPanel panneauBordure;
	private JToggleButton tgbtnScientifique;
	private boolean enCours = false;
	private HashMap<Integer, TypeSelection> touchesOutils = new HashMap<Integer, TypeSelection>();
	private HashMap<TypeSelection, JToggleButton> boutonsOutils = new HashMap<TypeSelection, JToggleButton>();
	private HashMap<Integer, TypeAffichage> touchesAffichage = new HashMap<Integer, TypeAffichage>();
	private int toucheDemarrer = KeyEvent.VK_SPACE;
	private int touchePlusLent = KeyEvent.VK_LEFT;
	private int touchePlusVite = KeyEvent.VK_RIGHT;
	private Inspecteur inspecteurCourant;
	private InspecteurScene inspecteurScene;
	private InspecteurPolygone inspecteurPolygone;
	private InspecteurZoneDeGravite inspecteurZoneDeGravite;
	private InspecteurExplosion inspecteurExplosion;
	private InspecteurRessort inspecteurRessort;
	private boolean ignorer=false;
	private ComposantAnimation composantAnimation;

	private JToggleButton tgbtnPerformance;
	private JLabel lblZoom;
	private JMenu menuFonctions;
	private JToggleButton tgbtnChemin;
	private JToggleButton tgbtnRotation;
	private JToggleButton tgbtnRessort;
	private JToggleButton tgbtnCrayon;
	private JToggleButton tgbtnSouris;
	private JMenu menuGroupe;
	private JMenuItem itemReseau;
	private JMenuItem itemBoucle;
	private JMenuItem itemChaine;
	/**
	 * Launch the application.
	 * @param args args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Materiau.charger();
					App16SimulateurPhysique frame = new App16SimulateurPhysique();
					frame.setVisible(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public App16SimulateurPhysique() {
		setResizable(false);
		setTitle("Simulateur de physique");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1059, 778);

		touchesOutils.put(KeyEvent.VK_C, TypeSelection.SOURIS);
		touchesOutils.put(KeyEvent.VK_D, TypeSelection.CRAYON);
		touchesOutils.put(KeyEvent.VK_R, TypeSelection.RECTANGLE);
		touchesOutils.put(KeyEvent.VK_E, TypeSelection.EXPLOSION);
		touchesOutils.put(KeyEvent.VK_L, TypeSelection.LIGNE);
		touchesOutils.put(KeyEvent.VK_Z, TypeSelection.ZONE);
		touchesOutils.put(KeyEvent.VK_S, TypeSelection.SPHERE);
		touchesOutils.put(KeyEvent.VK_T, TypeSelection.ROTATION);
		touchesOutils.put(KeyEvent.VK_W, TypeSelection.RESSORT);
		touchesOutils.put(KeyEvent.VK_G, TypeSelection.GROUPE);

		touchesAffichage.put(KeyEvent.VK_1, TypeAffichage.AFFICHAGE_GRILLE);
		touchesAffichage.put(KeyEvent.VK_2, TypeAffichage.AFFICHAGE_SCIENTIFIQUE);
		touchesAffichage.put(KeyEvent.VK_3, TypeAffichage.AFFICHAGE_CHEMIN);
		touchesAffichage.put(KeyEvent.VK_4, TypeAffichage.AFFICHAGE_PERFORMANCE);

		this.setIconImage(new ImageIcon(getClass().getClassLoader().getResource("outilSphere50x50.png")).getImage());
		contentPane = new JPanel();
		contentPane.setFocusable(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tgbtnDemarrer = new JToggleButton("D\u00E9marrer");
		tgbtnDemarrer.setToolTipText("<html>D\u00E9marrer <b>(Espace)</b></html>");
		tgbtnDemarrer.setFocusable(false);
		tgbtnDemarrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enCours = !enCours;
				if(enCours) {
					vue.demarrer();
					tgbtnDemarrer.setText("Arr�ter");
				} else {
					vue.arreter();
					tgbtnDemarrer.setText("D�marrer");
				}
			}
		});
		tgbtnDemarrer.setBounds(110, 0, 134, 26);
		contentPane.add(tgbtnDemarrer);

		panneauScenes = new JTabbedPane(JTabbedPane.TOP);
		panneauScenes.setFocusable(false);
		panneauScenes.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(panneauScenes.getSelectedIndex() != -1) {
					arreterAnimation();
					vue = vues.get(panneauScenes.getSelectedIndex());
					vue.setTypeSelection(selection);
					preparerNouvelleScene();
				}
				vue.repaint();
			}
		});
		panneauScenes.setBounds(335, 80, 698, 637);
		contentPane.add(panneauScenes);
		vue = new Vue(new Scene(), this);
		vues.add(vue);
		panneauScenes.addTab("Scene 1", null, vue, null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Outils", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(16, 68, 124, 324);
		contentPane.add(panel);
		panel.setLayout(null);

		JToggleButton tgbtnExplosion = new JToggleButton("");
		tgbtnExplosion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.EXPLOSION;
				vue.setTypeSelection(selection);
			}
		});
		tgbtnExplosion.setFocusable(false);
		tgbtnExplosion.setBounds(68, 78, 50, 50);
		panel.add(tgbtnExplosion);
		tgbtnExplosion.setToolTipText("<html>Explosion <b>(E)</b></html>");
		groupeOutils.add(tgbtnExplosion);
		tgbtnExplosion.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilExplosion50x50.png")));

		tgbtnSouris = new JToggleButton("");
		tgbtnSouris.setFocusable(false);
		tgbtnSouris.setBounds(6, 16, 50, 50);
		panel.add(tgbtnSouris);
		tgbtnSouris.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.SOURIS;
				vue.setTypeSelection(selection);
			}
		});
		tgbtnSouris.setSelected(true);
		tgbtnSouris.setToolTipText("<html>S\u00E9lection <b>(C)</b></html>");
		groupeOutils.add(tgbtnSouris);
		tgbtnSouris.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilSouris50x50.png")));

		JToggleButton tgbtnRectangle = new JToggleButton("");
		tgbtnRectangle.setFocusable(false);
		tgbtnRectangle.setBounds(6, 78, 50, 50);
		panel.add(tgbtnRectangle);
		tgbtnRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.RECTANGLE;
				vue.setTypeSelection(TypeSelection.RECTANGLE);
			}
		});
		tgbtnRectangle.setToolTipText("<html>Rectangle <b>(R)</b></html>");
		groupeOutils.add(tgbtnRectangle);
		tgbtnRectangle.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilRectangle50x50.png")));

		JToggleButton tgbtnSphere = new JToggleButton("");
		tgbtnSphere.setFocusable(false);
		tgbtnSphere.setBounds(6, 201, 50, 50);
		panel.add(tgbtnSphere);
		tgbtnSphere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.SPHERE;
				vue.setTypeSelection(TypeSelection.SPHERE);
			}
		});
		tgbtnSphere.setToolTipText("<html>Sph\u00E8re <b>(S)</b></html>");
		groupeOutils.add(tgbtnSphere);
		tgbtnSphere.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilSphere50x50.png")));

		tgbtnCrayon = new JToggleButton("");
		tgbtnCrayon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selection = TypeSelection.CRAYON;
				vue.setTypeSelection(TypeSelection.CRAYON);
			}
		});
		tgbtnCrayon.setFocusable(false);
		tgbtnCrayon.setBounds(68, 263, 50, 50);
		panel.add(tgbtnCrayon);
		groupeOutils.add(tgbtnCrayon);
		tgbtnCrayon.setToolTipText("<html>Dessin libre <b>(D)</b></html>");
		tgbtnCrayon.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilCrayon50x50.png")));

		JToggleButton tgbtnZone = new JToggleButton("");
		tgbtnZone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.ZONE;
				vue.setTypeSelection(TypeSelection.ZONE);
			}
		});
		tgbtnZone.setFocusable(false);
		tgbtnZone.setBounds(68, 140, 50, 50);
		panel.add(tgbtnZone);
		groupeOutils.add(tgbtnZone);
		tgbtnZone.setToolTipText("<html>Zone de gravit\u00E9 <b>(Z)</b></html>");
		tgbtnZone.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilZone50x50.png")));

		tgbtnRotation = new JToggleButton("");
		tgbtnRotation.setFocusable(false);
		tgbtnRotation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.ROTATION;
				vue.setTypeSelection(TypeSelection.ROTATION);
			}
		});
		tgbtnRotation.setBounds(68, 201, 50, 50);
		panel.add(tgbtnRotation);
		groupeOutils.add(tgbtnRotation);
		tgbtnRotation.setToolTipText("<html>Tourner <b>(T)</b></html>");
		tgbtnRotation.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilRotation50x50.png")));

		JToggleButton tgbtnLigne = new JToggleButton("");
		tgbtnLigne.setFocusable(false);
		tgbtnLigne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.LIGNE;
				vue.setTypeSelection(TypeSelection.LIGNE);
			}
		});
		tgbtnLigne.setBounds(6, 139, 50, 50);
		panel.add(tgbtnLigne);
		groupeOutils.add(tgbtnLigne);
		tgbtnLigne.setToolTipText("<html>Ligne <b>(L)</b></html>");
		tgbtnLigne.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilLigne50x50.png")));

		panneauBordure = new JPanel();
		panneauBordure.setBorder(new TitledBorder(null, "Inspecteur", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panneauBordure.setBounds(16, 395, 310, 328);
		contentPane.add(panneauBordure);
		panneauBordure.setLayout(null);

		inspecteurScene = new InspecteurScene(this);
		inspecteurScene.setScene(vue.getScene());
		inspecteurScene.setBounds(5, 17, 300, 300);
		panneauBordure.add(inspecteurScene);
		inspecteurScene.setToolTipText("Inspecteur");
		inspecteurCourant = inspecteurScene;
		
		inspecteurPolygone = new InspecteurPolygone(this);
		inspecteurPolygone.setBounds(5, 17, 300, 300);
		
		inspecteurZoneDeGravite = new InspecteurZoneDeGravite();
		inspecteurZoneDeGravite.setBounds(5, 17, 300, 300);
		inspecteurZoneDeGravite.setZone(new ZoneDeGravite(new ArrayList<Vecteur>()));
		
		inspecteurExplosion = new InspecteurExplosion();
		inspecteurExplosion.setBounds(5, 17, 300, 300);
		
		inspecteurRessort = new InspecteurRessort(this);
		inspecteurRessort.setBounds(5, 17, 300, 300);

		JScrollPane panneauListeObjets = new JScrollPane();
		panneauListeObjets.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panneauListeObjets.setBounds(164, 73, 155, 319);
		contentPane.add(panneauListeObjets);

		DefaultListModel<String> listModel = new DefaultListModel<String>();
		listeObjets = new JList<String>(listModel);
		listeObjets.setFocusable(false);
		listeObjets.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				if(ignorer)
					return;
				if(listeObjets.getSelectedIndex() != -1) {
					ObjetPhysique objet = vue.getObjet(listeObjets.getSelectedIndex());
					if(doitBouger) {
						vue.bougerAPosition(objet.getPosition());
					}
					vue.setSelection(objet);
					vue.repaint();
				}
				listeObjets.ensureIndexIsVisible(listeObjets.getSelectedIndex());
			}
		});
		listeObjets.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		listeObjets.setVisibleRowCount(listeObjets.getModel().getSize());
		panneauListeObjets.setViewportView(listeObjets);

		tgbtnQuadrillage = new JToggleButton("");
		tgbtnQuadrillage.setFocusable(false);
		tgbtnQuadrillage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vue.setQuadrillage(!Vue.getQuadrillage());
				tgbtnPerformance.setSelected(false);
				Vue.setPerformance(false);
				vue.repaint();
			}
		});
		tgbtnQuadrillage.setToolTipText("<html>Affichage de la grille <b>(1)</b></html>");
		tgbtnQuadrillage.setBounds(335, 11, 50, 50);
		tgbtnQuadrillage.setIcon(new ImageIcon(getClass().getClassLoader().getResource("optionQuadrillage50x50.png")));
		contentPane.add(tgbtnQuadrillage);

		tgbtnScientifique = new JToggleButton("");
		tgbtnScientifique.setFocusable(false);
		tgbtnScientifique.setSelected(true);
		tgbtnScientifique.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vue.setScientifique(!Vue.getScientifique());
				tgbtnPerformance.setSelected(false);
				Vue.setPerformance(false);
				vue.repaint();
			}
		});
		tgbtnScientifique.setToolTipText("<html>Affichage du mode scientifique <b>(2)</b></html>");
		tgbtnScientifique.setBounds(400, 11, 50, 50);
		tgbtnScientifique.setIcon(new ImageIcon(getClass().getClassLoader().getResource("optionScientifique50x50.png")));
		contentPane.add(tgbtnScientifique);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnScene = new JMenu("Sc\u00E8ne");
		menuBar.add(mnScene);

		JMenuItem itemNouvelleScene = new JMenuItem("Nouvelle sc\u00E8ne");
		itemNouvelleScene.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		itemNouvelleScene.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ajouterScene(new Scene());
			}
		});
		mnScene.add(itemNouvelleScene);

		JMenuItem itemOuvrir = new JMenuItem("Ouvrir");
		itemOuvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
		itemOuvrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				chargerScene();
			}
		});
		mnScene.add(itemOuvrir);

		JMenuItem itemEnregistrer = new JMenuItem("Enregistrer");
		itemEnregistrer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		itemEnregistrer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enregistrerScene();
			}
		});
		
		JMenuItem mntmDmonstrations = new JMenuItem("D\u00E9monstrations");
		mntmDmonstrations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueDemonstration(parent).setVisible(true);
			}
		});
		mntmDmonstrations.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));
		mnScene.add(mntmDmonstrations);
		mnScene.add(itemEnregistrer);

		JMenuItem itemEnregistrerSous = new JMenuItem("Enregistrer sous");
		itemEnregistrerSous.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enregistrerSceneSous();
			}
		});
		itemEnregistrerSous.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		mnScene.add(itemEnregistrerSous);

		JMenuItem itemSupprimer = new JMenuItem("Supprimer la sc\u00E8ne");
		itemSupprimer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_MASK));
		itemSupprimer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.arreter();
				vues.remove(panneauScenes.getSelectedIndex());
				panneauScenes.removeTabAt(panneauScenes.getSelectedIndex());
				if(panneauScenes.getTabCount() == 0) {
					vue = new Vue(new Scene(), parent);
					vues.add(vue);
					panneauScenes.addTab("Scene 1", vue);
				} else {
					for(int i = 1; i <= vues.size(); i++) {
						panneauScenes.setTitleAt(i - 1, "Scene " + i);
					}
				}
			}
		});
		mnScene.add(itemSupprimer);

		JMenuItem itemQuitter = new JMenuItem("Quitter");
		itemQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		itemQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnScene.add(itemQuitter);

		JMenu mnInsertion = new JMenu("Insertion");
		menuBar.add(mnInsertion);

		JMenuItem itemSphere = new JMenuItem("Sph\u00E8re");
		itemSphere.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_MASK));
		itemSphere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionSphere(new AdapteurUniversel() {
					private static final long serialVersionUID = 4375175090604059940L;

					public void formeCree(Forme f) {
						vue.ajouterObjetCentre(f);
						parent.objetsChanges(false);
					}
				}).setVisible(true);
			}
		});
		mnInsertion.add(itemSphere);

		JMenuItem itemLigne = new JMenuItem("Ligne");
		itemLigne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionLigne(new AdapteurUniversel() {
					private static final long serialVersionUID = -2058926309414708159L;

					public void formeCree(Forme f) {
						vue.ajouterObjetCentre(f, true);
						parent.objetsChanges(false);
					}
				}).setVisible(true);
			}
		});
		itemLigne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.SHIFT_MASK));
		mnInsertion.add(itemLigne);

		JMenuItem itemPolygoneRegulier = new JMenuItem("Polygone r\u00E9gulier");
		itemPolygoneRegulier.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.SHIFT_MASK));
		itemPolygoneRegulier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionPolygone(new AdapteurUniversel() {
					private static final long serialVersionUID = 7707617363783420458L;

					public void formeCree(Forme f) {
						vue.ajouterObjetCentre(f);
						parent.objetsChanges(false);
					}
				}).setVisible(true);
			}
		});
		mnInsertion.add(itemPolygoneRegulier);

		JMenuItem itemRectangle = new JMenuItem("Rectangle");
		itemRectangle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.SHIFT_MASK));
		itemRectangle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionRectangle(new AdapteurUniversel() {
					private static final long serialVersionUID = -7723654642388566592L;

					public void formeCree(Forme f) {
						vue.ajouterObjetCentre(f);
						parent.objetsChanges(false);
					}
				}).setVisible(true);
			}
		});
		mnInsertion.add(itemRectangle);

		JMenuItem itemZoneDeGravite = new JMenuItem("Zone de gravit\u00E9");
		itemZoneDeGravite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionZone(new AdapteurUniversel() {
				private static final long serialVersionUID = -7723654642388566592L;

				public void objetCree(ObjetPhysique objet) {
					vue.ajouterObjetCentre(objet);
					parent.objetsChanges(false);
				}
			}).setVisible(true);
			}
		});
		itemZoneDeGravite.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.SHIFT_MASK));
		mnInsertion.add(itemZoneDeGravite);

		JMenuItem itemCopier = new JMenuItem("Copier le/les objet(s) s\u00E9lectionn\u00E9(s)");
		itemCopier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.copier();
			}
		});
		
		JMenuItem itemExplosion = new JMenuItem("Explosion");
		itemExplosion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new InsertionExplosion(new AdapteurUniversel() {
					private static final long serialVersionUID = -7723654642388566592L;

					public void objetCree(ObjetPhysique obj) {
						vue.ajouterObjetCentre(obj);
						parent.objetsChanges(false);
					}
				}).setVisible(true);
			}
		});
		itemExplosion.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.SHIFT_MASK));
		mnInsertion.add(itemExplosion);

		JSeparator separator = new JSeparator();
		mnInsertion.add(separator);
		
		JMenuItem mntmCouperLelesObjets = new JMenuItem("Couper le/les objet(s) s\u00E9lectionn\u00E9(s)");
		mntmCouperLelesObjets.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.copier();
				vue.supprimerObjet(vue.getSelection());
				listeObjets.setModel(vue.getListeObjets());
				sceneSelectionnee();
			}
		});
		mntmCouperLelesObjets.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
		mnInsertion.add(mntmCouperLelesObjets);
		itemCopier.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		mnInsertion.add(itemCopier);

		JMenuItem itemColler = new JMenuItem("Coller le/les objet(s) s\u00E9lectionn\u00E9(s)");
		itemColler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.coller();
			}
		});
		itemColler.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK));
		mnInsertion.add(itemColler);

		JMenuItem itemSupprimerObjet = new JMenuItem("Supprimer le/les objet(s) s\u00E9lectionn\u00E9(s)");
		itemSupprimerObjet.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
		itemSupprimerObjet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				vue.supprimerObjet(vue.getSelection());
				listeObjets.setModel(vue.getListeObjets());
				sceneSelectionnee();
			}
		});
		
		JMenuItem mntmCollerMultiples = new JMenuItem("Coller multiples");
		mntmCollerMultiples.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean erreur=false;
				int nombre=0;
				try{
					nombre=Integer.parseInt(JOptionPane.showInputDialog("Entrez le nombre de copies (max 100): "));
				}catch(Exception ex){
					erreur=true;
				}
				if(erreur || nombre>100 || nombre<=0){
					JOptionPane.showMessageDialog(null, "Nombre de copies invalide.");
				}else{
					for(int i=0;i<nombre;i++){
						vue.coller();
					}
				}
			}
		});
		mntmCollerMultiples.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK));
		mnInsertion.add(mntmCollerMultiples);
		mnInsertion.add(itemSupprimerObjet);
		
		menuFonctions = new JMenu("Fonctions");
		menuFonctions.setEnabled(false);
		menuBar.add(menuFonctions);
		
		JMenuItem itemLineaire = new JMenuItem("Ajouter fonction lin\u00E9aire");
		itemLineaire.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.ALT_MASK));
		itemLineaire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inspecteurPolygone.ajouterFonction(TypeFonction.LINEAIRE);
			}
		});
		menuFonctions.add(itemLineaire);
		
		JMenuItem itemCercle = new JMenuItem("Ajouter fonction cercle");
		itemCercle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_MASK));
		itemCercle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inspecteurPolygone.ajouterFonction(TypeFonction.CERCLE);
			}
		});
		menuFonctions.add(itemCercle);
		
		JMenuItem itemTrigo = new JMenuItem("Ajouter fonction trigonom\u00E9trique");
		itemTrigo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.ALT_MASK));
		itemTrigo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inspecteurPolygone.ajouterFonction(TypeFonction.SINUS);
			}
		});
		menuFonctions.add(itemTrigo);
		
		JMenuItem itemEnleverFonction = new JMenuItem("Supprimer fonction");
		itemEnleverFonction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inspecteurPolygone.enlever();
			}
		});
		itemEnleverFonction.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.ALT_MASK));
		menuFonctions.add(itemEnleverFonction);
		
		menuGroupe = new JMenu("Groupe");
		menuGroupe.setEnabled(false);
		menuBar.add(menuGroupe);
		
		itemReseau = new JMenuItem("Ajouter un r\u00E9seau de ressorts");
		itemReseau.setEnabled(false);
		itemReseau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.ajouterReseau();
			}
		});
		itemReseau.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuGroupe.add(itemReseau);
		
		itemBoucle = new JMenuItem("Ajouter une boucle de ressorts");
		itemBoucle.setEnabled(false);
		itemBoucle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.ajouterBoucle();
			}
		});
		itemBoucle.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuGroupe.add(itemBoucle);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Entourer d'une zone");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.entourerZone();
			}
		});
		
		itemChaine = new JMenuItem("Ajouter une chaine de ressorts");
		itemChaine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.ajouterChaine();
			}
		});
		itemChaine.setEnabled(false);
		itemChaine.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuGroupe.add(itemChaine);
		mntmNewMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuGroupe.add(mntmNewMenuItem);
		
		JMenuItem itemMurs = new JMenuItem("Entourer de murs");
		itemMurs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.entourerMurs();
			}
		});
		itemMurs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuGroupe.add(itemMurs);
		
		JMenu menuSelection = new JMenu("S\u00E9lection");
		menuBar.add(menuSelection);
		
		JMenuItem itemToutSelectionner = new JMenuItem("Tout selectionner");
		itemToutSelectionner.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
		itemToutSelectionner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(TypeObjet.CORPS, true);
			}
		});
		menuSelection.add(itemToutSelectionner);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("S\u00E9lectionner corps");
		mntmNewMenuItem_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(TypeObjet.CORPS, false);
			}
		});
		menuSelection.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("S\u00E9lectionner explosions");
		mntmNewMenuItem_4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(TypeObjet.EXPLOSION, false);
			}
		});
		menuSelection.add(mntmNewMenuItem_4);
		
		JMenuItem mntmNewMenuItem_5 = new JMenuItem("S\u00E9lectionner zones");
		mntmNewMenuItem_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(TypeObjet.ZONE_DE_GRAVITE, false);
			}
		});
		menuSelection.add(mntmNewMenuItem_5);
		
		JMenuItem mntmSlectionnerRessorts = new JMenuItem("S\u00E9lectionner ressorts");
		mntmSlectionnerRessorts.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		mntmSlectionnerRessorts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(TypeObjet.RESSORT, false);
			}
		});
		menuSelection.add(mntmSlectionnerRessorts);
		
		JMenuItem mntmNewMenuItem_6 = new JMenuItem("D\u00E9selectionner");
		mntmNewMenuItem_6.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0));
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vue.selectionner(null, false);
			}
		});
		menuSelection.add(mntmNewMenuItem_6);

		JMenu mnAide = new JMenu("Aide");
		menuBar.add(mnAide);

		JMenuItem itemJeuxEssai = new JMenuItem("Jeux d'essai");
		itemJeuxEssai.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		itemJeuxEssai.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueJeuxEssai().setVisible(true);
			}
		});
		mnAide.add(itemJeuxEssai);

		JMenuItem itemInstructions = new JMenuItem("Instructions compl\u00E8tes");
		itemInstructions.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0));
		itemInstructions.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueInstructions().setVisible(true);
			}
		});
		mnAide.add(itemInstructions);

		JMenuItem itemConcepts = new JMenuItem("Concepts scientifiques");
		itemConcepts.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0));
		itemConcepts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueConcepts().setVisible(true);
			}
		});
		mnAide.add(itemConcepts);

		JMenuItem itemSources = new JMenuItem("Sources");
		itemSources.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0));
		itemSources.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueSources().setVisible(true);
			}
		});
		mnAide.add(itemSources);

		JMenuItem itemAPropos = new JMenuItem("\u00C0 propos");
		itemAPropos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));
		itemAPropos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new DialogueAPropos().setVisible(true);
			}
		});
		mnAide.add(itemAPropos);
		
		tgbtnRessort = new JToggleButton("");
		tgbtnRessort.setBounds(6, 263, 50, 50);
		panel.add(tgbtnRessort);
		tgbtnRessort.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilRessort50x50.png")));
		groupeOutils.add(tgbtnRessort);
		tgbtnRessort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selection = TypeSelection.RESSORT;
				vue.setTypeSelection(selection);
			}
		});
		tgbtnRessort.setToolTipText("<html>Ressort <b>(W)</b></html>");
		tgbtnRessort.setFocusable(false);
		
		boutonsOutils.put(TypeSelection.SOURIS, tgbtnSouris);
		boutonsOutils.put(TypeSelection.CRAYON, tgbtnCrayon);
		boutonsOutils.put(TypeSelection.RECTANGLE, tgbtnRectangle);
		boutonsOutils.put(TypeSelection.EXPLOSION, tgbtnExplosion);
		boutonsOutils.put(TypeSelection.LIGNE, tgbtnLigne);
		boutonsOutils.put(TypeSelection.ZONE, tgbtnZone);
		boutonsOutils.put(TypeSelection.SPHERE, tgbtnSphere);
		boutonsOutils.put(TypeSelection.ROTATION, tgbtnRotation);
		boutonsOutils.put(TypeSelection.RESSORT, tgbtnRessort);
		
				JToggleButton tgbtnGroupe = new JToggleButton("");
				tgbtnGroupe.setBounds(68, 16, 50, 50);
				panel.add(tgbtnGroupe);
				tgbtnGroupe.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						selection = TypeSelection.GROUPE;
						vue.setTypeSelection(selection);
					}
				});
				groupeOutils.add(tgbtnGroupe);
				tgbtnGroupe.setIcon(new ImageIcon(getClass().getClassLoader().getResource("outilGroupe50x50.png")));
				tgbtnGroupe.setToolTipText("<html>Groupe <b>(G)</b></html>");
				tgbtnGroupe.setFocusable(false);
				boutonsOutils.put(TypeSelection.GROUPE, tgbtnGroupe);
		
		JPanel panneauPerformance = new JPanel();
		panneauPerformance.setBorder(new TitledBorder(null, "Performance", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panneauPerformance.setBounds(721, 6, 312, 93);
		contentPane.add(panneauPerformance);
		panneauPerformance.setLayout(null);
		
		composantAnimation = new ComposantAnimation();
		composantAnimation.setBounds(6, 16, 300, 70);
		panneauPerformance.add(composantAnimation);
		final JSlider curseurVitesseAnimation = new JSlider();
		curseurVitesseAnimation.setToolTipText("<html>Vitesse d'animation <b>(Fl\u00E8ches horizontales)</b></html>");
		curseurVitesseAnimation.setFocusable(false);
		curseurVitesseAnimation.setValue(0);
		curseurVitesseAnimation.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				double valeur = curseurVitesseAnimation.getValue();
				if(valeur >= 0) {
					valeur += 1;
				} else {
					valeur = 1 - Math.abs(valeur) / 10.0;
				}
				Vue.setVitesseAnimation(valeur);
				tempsChange(vue.getInfoAnimation());
			}
		});

		curseurVitesseAnimation.setMinorTickSpacing(1);
		curseurVitesseAnimation.setMinimum(-9);
		curseurVitesseAnimation.setPaintTicks(true);
		curseurVitesseAnimation.setPaintLabels(true);
		curseurVitesseAnimation.setMajorTickSpacing(10);
		curseurVitesseAnimation.setMaximum(9);
		curseurVitesseAnimation.setBounds(16, 27, 309, 45);
		Hashtable<Integer, JLabel> tableVitesses = new Hashtable<Integer, JLabel>();
		tableVitesses.put(new Integer(-9), new JLabel("0.1x"));
		tableVitesses.put(new Integer(0), new JLabel("1x"));
		tableVitesses.put(new Integer(9), new JLabel("10x"));
		curseurVitesseAnimation.setLabelTable(tableVitesses);
		contentPane.add(curseurVitesseAnimation);
		
		tgbtnChemin = new JToggleButton("");
		tgbtnChemin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vue.setChemin(!Vue.getChemin());
				tgbtnPerformance.setSelected(false);
				Vue.setPerformance(false);
				vue.repaint();
			}
		});
		tgbtnChemin.setIcon(new ImageIcon(getClass().getClassLoader().getResource("optionChemin50x50.png")));
		tgbtnChemin.setToolTipText("<html>Affichage du mode Chemin <b>(3)</b></html>");
		tgbtnChemin.setFocusable(false);
		tgbtnChemin.setBounds(460, 11, 50, 50);
		contentPane.add(tgbtnChemin);
		
		tgbtnPerformance = new JToggleButton("");
		tgbtnPerformance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Vue.setPerformance(!Vue.getPerformance());
				if(Vue.getPerformance()){
					Vue.setScientifique(false);
					tgbtnScientifique.setSelected(false);
					Vue.setQuadrillage(false);
					tgbtnQuadrillage.setSelected(false);
					Vue.setChemin(false);
					tgbtnChemin.setSelected(false);
				}
				vue.repaint();
			}
		});
		tgbtnPerformance.setIcon(new ImageIcon(getClass().getClassLoader().getResource("optionPerformance50x50.png")));
		tgbtnPerformance.setToolTipText("<html>Affichage du mode performance <b>(4)</b></html>");
		tgbtnPerformance.setFocusable(false);
		tgbtnPerformance.setBounds(519, 11, 50, 50);
		contentPane.add(tgbtnPerformance);
		
		lblZoom = new JLabel("Zoom: 0%");
		lblZoom.setToolTipText("<html>Zoom <b>(Molette de souris)</b></html>");
		lblZoom.setFont(new Font("Trebuchet MS", Font.PLAIN, 15));
		lblZoom.setBounds(616, 27, 95, 34);
		contentPane.add(lblZoom);

		this.setFocusable(true);
		this.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int touche = e.getKeyCode();
				Vue.setMajPresse(e.isShiftDown());
				if(!e.isAltDown() && !e.isAltGraphDown() && !e.isControlDown() && !e.isMetaDown() && !e.isShiftDown()) {
					if(touchesOutils.containsKey(touche) && boutonsOutils.get(touchesOutils.get(touche)).isEnabled()) {
						boutonsOutils.get(selection).setSelected(false);

						selection = touchesOutils.get(touche);
						boutonsOutils.get(selection).setSelected(true);
						vue.setTypeSelection(selection);

						e.consume();
					} else if(touchesAffichage.containsKey(touche)) {
						TypeAffichage affichage = touchesAffichage.get(touche);
						switch(affichage) {
						case AFFICHAGE_GRILLE:
							tgbtnQuadrillage.setSelected(!Vue.getQuadrillage());
							Vue.setQuadrillage(!Vue.getQuadrillage());
							tgbtnPerformance.setSelected(false);
							Vue.setPerformance(false);
							break;

						case AFFICHAGE_SCIENTIFIQUE:
							tgbtnScientifique.setSelected(!Vue.getScientifique());
							Vue.setScientifique(!Vue.getScientifique());
							tgbtnPerformance.setSelected(false);
							Vue.setPerformance(false);
							break;
							
						case AFFICHAGE_PERFORMANCE:
							tgbtnPerformance.setSelected(!Vue.getPerformance());
							Vue.setPerformance(!Vue.getPerformance());
							if(Vue.getPerformance()){
								tgbtnScientifique.setSelected(false);
								Vue.setScientifique(false);
								tgbtnQuadrillage.setSelected(false);
								Vue.setQuadrillage(false);
								Vue.setChemin(false);
								tgbtnChemin.setSelected(false);
							}
							break;
						case AFFICHAGE_CHEMIN:
							tgbtnChemin.setSelected(!Vue.getChemin());
							Vue.setChemin(!Vue.getChemin());
							tgbtnPerformance.setSelected(false);
							Vue.setPerformance(false);
							break;
						}
						
						vue.repaint();
						e.consume();
					} else if(touche == toucheDemarrer) {
						enCours = !enCours;
						if(enCours) {
							vue.demarrer();
							tgbtnDemarrer.setText("Arr�ter");
						} else {
							vue.arreter();
							tgbtnDemarrer.setText("D�marrer");
						}

						tgbtnDemarrer.setSelected(enCours);
						e.consume();
					} else if(touche == touchePlusLent) {
						int valeur = curseurVitesseAnimation.getValue();
						if(valeur > curseurVitesseAnimation.getMinimum()) {
							curseurVitesseAnimation.setValue(valeur - curseurVitesseAnimation.getMinorTickSpacing());
						}
					} else if(touche == touchePlusVite) {
						int valeur = curseurVitesseAnimation.getValue();
						if(valeur < curseurVitesseAnimation.getMaximum()) {
							curseurVitesseAnimation.setValue(valeur + curseurVitesseAnimation.getMinorTickSpacing());
						}
					} else {
						super.keyPressed(e);
					}
				}
			}
			public void keyReleased(KeyEvent e) {
				Vue.setMajPresse(e.isShiftDown());
			}
		});
		preparerNouvelleScene();
	}

	private void chargerScene() {
		JFileChooser selecteur = new JFileChooser();
		selecteur.addChoosableFileFilter(new FileNameExtensionFilter("Scenes", "mlg"));
		selecteur.setAcceptAllFileFilterUsed(false);
		int response = selecteur.showOpenDialog(this);
		if(response == JFileChooser.APPROVE_OPTION) {
			try {
				FileInputStream fin = new FileInputStream(selecteur.getSelectedFile());
				ObjectInputStream ois = new ObjectInputStream(fin);
				Object obj = ois.readObject();
				if(obj.getClass() == Scene.class) {
					Scene s = (Scene) obj;
					String location=selecteur.getSelectedFile().getAbsoluteFile().toString();
					s.setLocationFichier(location.substring(0, location.lastIndexOf(".mlg")));
					ois.close();
					ajouterScene(s);
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		vue.repaint();
	}

	private void enregistrerSceneSous() {
		arreterAnimation();
		JFileChooser selecteur = new JFileChooser();
		selecteur.addChoosableFileFilter(new FileNameExtensionFilter("Scenes", "mlg"));
		selecteur.setAcceptAllFileFilterUsed(false);
		int response = selecteur.showSaveDialog(this);
		if(response == JFileChooser.APPROVE_OPTION) {
			try {
				FileOutputStream fout = new FileOutputStream(selecteur.getSelectedFile().getAbsoluteFile() + ".mlg");
				ObjectOutputStream oos = new ObjectOutputStream(fout);
				vue.getScene().setLocationFichier(selecteur.getSelectedFile().getAbsoluteFile().toString());
				oos.writeObject(vue.getScene());
				oos.close();
			} catch(Exception e) {

			}
		}
	}

	private void enregistrerScene() {
		arreterAnimation();
		if(vue.getScene().getLocationFichier().equals("")) {
			enregistrerSceneSous();
		} else {
			try {
				FileOutputStream fout = new FileOutputStream(vue.getScene().getLocationFichier()+".mlg");
				ObjectOutputStream oos = new ObjectOutputStream(fout);
				oos.writeObject(vue.getScene());
				oos.close();
			} catch(Exception e) {

			}
		}
	}

	public void formeCree(Forme f) {}
	public void objetCree(ObjetPhysique obj) {}

	@Override
	public void objetSelectionne(ObjetPhysique objet) {
		if(objet==null){
			return;
		}
		verifierSelection(objet.getTypeObjet() == TypeObjet.CORPS);
		int index=vue.getNombreObjets()==0?-1:vue.getScene().getIndexObjet(objet);
		switch(objet.getTypeObjet()) {
		case CORPS:
			menuFonctions.setEnabled(true);
			Corps corps = (Corps)objet;
			inspecteurPolygone.setCorps(corps);
			inspecteurCourant = inspecteurPolygone;
			panneauBordure.removeAll();
			panneauBordure.add(inspecteurPolygone);
			doitBouger = false;
			listeObjets.setSelectedIndex(index);
			doitBouger = true;
			listeObjets.repaint();
			inspecteurCourant.actualiser();
			inspecteurCourant.redessiner();
			break;

		case ZONE_DE_GRAVITE:
			menuFonctions.setEnabled(false);
			ZoneDeGravite zone=(ZoneDeGravite)objet;
			inspecteurZoneDeGravite.setZone(zone);
			inspecteurCourant = inspecteurZoneDeGravite;
			panneauBordure.removeAll();
			panneauBordure.add(inspecteurZoneDeGravite);
			doitBouger = false;
			listeObjets.setSelectedIndex(index);
			doitBouger = true;
			listeObjets.repaint();
			inspecteurCourant.redessiner();
			break;
		case EXPLOSION:
			menuFonctions.setEnabled(false);
			Explosion explosion=(Explosion)objet;
			inspecteurExplosion.setExplosion(explosion);
			inspecteurCourant = inspecteurExplosion;
			panneauBordure.removeAll();
			panneauBordure.add(inspecteurExplosion);
			doitBouger = false;
			listeObjets.setSelectedIndex(index);
			doitBouger = true;
			listeObjets.repaint();
			inspecteurCourant.redessiner();
			break;
		case RESSORT:
			menuFonctions.setEnabled(false);
			Ressort ressort=(Ressort)objet;
			inspecteurRessort.setRessort(ressort);
			inspecteurCourant = inspecteurRessort;
			panneauBordure.removeAll();
			panneauBordure.add(inspecteurRessort);
			doitBouger = false;
			listeObjets.setSelectedIndex(index);
			doitBouger = true;
			listeObjets.repaint();
			inspecteurCourant.redessiner();
			break;
		}
	}

	public void proprietesChangees() {
		inspecteurCourant.actualiser();
	}

	public void objetsChanges(boolean seulementAffichage) {
		if(!seulementAffichage){
			listeObjets.setModel(vue.getListeObjets());
			if(vue.getNombreObjets() > 0) {
				vue.setSelection(vue.getNombreObjets() - 1);
				objetSelectionne(vue.getSelection());
			} else {
				sceneSelectionnee();
			}
		}else{
			listeObjets.setModel(vue.getListeObjets());
			ignorer=true;
			listeObjets.setSelectedIndex(vue.getIndexObjet(vue.getSelection()));
			ignorer=false;
		}
	}

	public void preparerNouvelleScene() {
		if(listeObjets != null) {
			listeObjets.setModel(vue.getListeObjets());
			tempsChange(vue.getInfoAnimation());
			zoomChange(vue.getZoom());
			sceneSelectionnee();
			vue.verifierSelection();
		}
	}

	public void redessiner() {
		repaint();
	}

	public void sceneSelectionnee() {
		verifierSelection(false);
		menuFonctions.setEnabled(false);
		inspecteurScene.setScene(vue.getScene());
		inspecteurScene.setBounds(5, 17, 300, 300);
		inspecteurCourant = inspecteurScene;
		panneauBordure.removeAll();
		panneauBordure.add(inspecteurScene);
		listeObjets.clearSelection();
		listeObjets.repaint();
		inspecteurCourant.redessiner();
	}

	public void corpsSuivi(Corps c) {
		vue.setSuivi(c);
	}

	@Override
	public void bougerScene(Vecteur position) {
	}

	@Override
	public void tempsChange(InfoAnimation info) {
		composantAnimation.actualiser(info);
	}

	@Override
	public void zoomChange(double zoom) {
		lblZoom.setText("Zoom: "+(100-(int)Math.floor(zoom))+"%");
	}

	@Override
	public void ajouterFonction(Fonction f) {
		inspecteurPolygone.ajouterFonction(f);
		inspecteurPolygone.ajouterFonctionListe(f);
	}
	public void verifierSelection(boolean estUnCorps){
		tgbtnCrayon.setEnabled(estUnCorps);
		tgbtnRessort.setEnabled(estUnCorps);
		if(selection == TypeSelection.CRAYON || selection == TypeSelection.RESSORT || selection == TypeSelection.ROTATION){
			selection=TypeSelection.SOURIS;
			vue.setTypeSelection(selection);
			tgbtnSouris.setSelected(true);
		}
	}
	public void selectionMultiple(boolean multiple, boolean deuxCorps) {
		menuGroupe.setEnabled(multiple);
		itemReseau.setEnabled(deuxCorps);
		itemBoucle.setEnabled(deuxCorps);
		itemChaine.setEnabled(deuxCorps);
	}

	@Override
	public boolean animationEnCours() {return false;}
	/**
	 * Arr�te l'animation de la sc�ne.
	 */
	private void arreterAnimation(){
		tgbtnDemarrer.setSelected(false);
		tgbtnDemarrer.setText("D�marrer");
		enCours=false;
		vue.arreter();
	}

	@Override
	public void ajouterScene(Scene scene) {
		if(vues.size() < 5) {
			vue.arreter();
			Vue nouvelleVue = new Vue(scene, parent);
			vues.add(nouvelleVue);
			panneauScenes.addTab("Scene " + vues.size(), null, vues.get(vues.size() - 1), null);
			panneauScenes.setSelectedIndex(panneauScenes.getTabCount() - 1);
			vue = vues.get(vues.size() - 1);
		} else {
			JOptionPane.showMessageDialog(null, "Le maximum de 5 sc�nes est atteint!");
		}
	}
}
