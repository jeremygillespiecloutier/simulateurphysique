package formes;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import maths.Projection;
import maths.Vecteur;
import collision.Collision;

/**
 * @author Francis Labelle
 * Classe repr�sentant un polygone.
 */
public class Polygone extends Forme implements Serializable{
	private static final long serialVersionUID = -3960817208195047007L;
	private ArrayList<Vecteur> points = new ArrayList<Vecteur>();
	private Vecteur normaleSpeciale = null;
	
	/**
	 * Construit un rectangle centr� � la position donn�e.
	 * @param largeur : Largeur du rectangle.
	 * @param hauteur : Hauteur du rectangle.
	 * @param position : Centre du rectangle.
	 */
	public Polygone(double largeur, double hauteur, Vecteur position) {
		points.add(new Vecteur(-largeur / 2.0, hauteur / 2.0));
		points.add(new Vecteur(largeur / 2.0, hauteur / 2.0));
		points.add(new Vecteur(largeur / 2.0, -hauteur / 2.0));
		points.add(new Vecteur(-largeur / 2.0, -hauteur / 2.0));
		setPosition(position);
		setAire(trouverAire());
		this.setShapeInitiale(getShape(new AffineTransform()));
		setPositionInitiale(this.getPosition());
	}

	/**
	 * Construit un polygone r�gulier.
	 * @param nombreDeCotes : Nombre de c�t�s du polygone.
	 * @param largeurCote : Largeur d'un c�t� du polygone.
	 */
	public Polygone(int nombreDeCotes, double largeurCote) {
		double angle = Math.toRadians(180.0 - (nombreDeCotes - 2.0) * 180.0 / (double) nombreDeCotes);
		double apotheme = largeurCote / (2.0 * Math.tan(Math.PI / (double) nombreDeCotes));
		Vecteur points[] = new Vecteur[nombreDeCotes];
		Vecteur precedent = new Vecteur(largeurCote / 2.0, -apotheme);
		points[0] = precedent;
		for (int i = 1; i < nombreDeCotes; i++) {
			double a = i * angle;
			precedent = precedent.add(new Vecteur(Math.cos(a) * largeurCote, Math.sin(a) * largeurCote));
			points[i] = precedent;
		}
		
		this.points = new ArrayList<Vecteur>(Arrays.asList(points));
		setPosition(new Vecteur());
		setAire(trouverAire());
		this.setShapeInitiale(getShape(new AffineTransform()));
		setPositionInitiale(this.getPosition());
	}

	/**
	 * Construit un polygone � partir d'un autre.
	 * @param polygone : Polygone � copier.
	 */
	public Polygone(Polygone polygone) {
		super(polygone);
		this.points = new ArrayList<Vecteur>(polygone.points);
		setAire(trouverAire());
		this.setRotation(polygone.getRotation());
		this.setShapeInitiale(polygone.getShapeInitiale());
		setPositionInitiale(polygone.getPositionInitiale());
	}

	/**
	 * @return Le nombre de points du polygone.
	 */
	public int getNombrePoints() {
		return points.size();
	}
	
	/**
	 * Retourne un point du polygone.
	 * @param indice : Indice du point.
	 * @return Le point se trouvant � l'indice donn�.
	 */
	public Vecteur getPoint(int indice) {
		return new Vecteur(points.get(indice));
	}

	/**
	 * Change la position du polygone.
	 * @param position: Nouvelle position du polygone.
	 */
	@Override
	public void setPosition(Vecteur position) {
		Vecteur translation = position.sub(getPosition());
		this.deplacer(translation);
	}

	/**
	 * D�place le polygone.
	 * @param deplacement: D�placement � effectuer.
	 */
	@Override
	public void deplacer(Vecteur deplacement) {
		super.deplacer(deplacement);

		for (int i = 0; i < points.size(); i++) {
			points.set(i, points.get(i).add(deplacement));
		}
	}
	
	/**
	 * D�termine si le polygone intersecte une autre forme.
	 * @param forme: Forme � tester.
	 * @return Vrai si les formes sont en intersection.
	 */
	@Override
	public boolean intersecte(Forme forme) {
		return Collision.testerIntersection(this, forme) == null;
	}
	
	/**
	 * @return Les axes sur lesquels projeter le polygone pour les collisions (� l'aide du th�or�me de la s�paration des convexes).
	 */
	public Vecteur[] getAxesProjection() {
		ArrayList<Vecteur> axes = new ArrayList<Vecteur>();
		
		int nbPoints = this.getNombrePoints();
		
		for(int i = 0 ; i < nbPoints ; i++) {
			Vecteur pointPrecedent = getPoint(i);
			Vecteur pointActuel = getPoint((i + 1) % nbPoints);
			Vecteur pointSuivant = getPoint((i + 2) % nbPoints);
			
			Vecteur coteActuel = pointActuel.sub(pointPrecedent).unitaire();
			Vecteur coteSuivant = pointSuivant.sub(pointActuel).unitaire();
			
			Vecteur normale = coteActuel.perpendiculaire();
			
			//Si la normale calcul�e se dirige vers le centre du polygone, on la change de bord
			if(coteSuivant.produitScalaire(normale) > 0.0) {
				normale = normale.mul(-1.0);
			}
			
			axes.add(normale);
		}
		
		return axes.toArray(new Vecteur[axes.size()]);
	}
	
	/**
	 * Projette le polygone sur un axe.
	 * @param axe: Axe sur lequel projeter le polygone.
	 * @return La projection du polygone sur l'axe.
	 */
	@Override
	public Projection projeter(Vecteur axe) {
 		Vecteur point = getPoint(0);
		double min = point.produitScalaire(axe);
		double max = min;
		double projectionSegment;

		for (int j = 1; j < getNombrePoints(); j++) {
			point = getPoint(j);
			projectionSegment = point.produitScalaire(axe);

			if (projectionSegment < min) {
				min = projectionSegment;
			} else if (projectionSegment > max) {
				max = projectionSegment;
			}
		}

		return new Projection(min, max);
	}

	/**
	 * Tourne le polygone.
	 * @param rotation: Rotation � effectuer.
	 */
	@Override
	public void tourner(double rotation) {
		super.tourner(rotation);
		
		Vecteur position = getPosition();
		double posX = position.getX();
		double posY = position.getY();
		Vecteur point, pointTransfo;
		double x, y;
		double xTransfo, yTransfo;
		double cos = Math.cos(rotation);
		double sin = Math.sin(rotation);
		
		for (int i = 0; i < points.size(); i++) {
			point = points.get(i);
			x = point.getX();
			y = point.getY();
			xTransfo = cos * (x - posX) - sin * (y - posY) + posX;
			yTransfo = sin * (x - posX) + cos * (y - posY) + posY;
			pointTransfo = new Vecteur(xTransfo, yTransfo);
			points.set(i, pointTransfo);
		}
	}

	/**
	 * @return L'aire du polygone.
	 */
	@Override
	public double trouverAire() {
		double aire = 0;
		int j = points.size() - 1;
		for(int i = 0; i < points.size(); i++) {
			aire += (points.get(j).getX() + points.get(i).getX()) * (points.get(j).getY() - points.get(i).getY());
			j = i;
		}
		return Math.abs(aire) * 0.5;
	}

	/**
	 * Calcule l'inertie du polygone � partir de sa masse et de sa densit�.
	 * @param masse: Masse du polygone.
	 * @param densite: Densit� du polygone.
	 * @return L'inertie du polygone.
	 */
	@Override
	public double calculerInertie(double masse, double densite) {
		//Ratio pour compenser la formule pour les polygones plats.
		double ratio = 5;
		double somme1 = 0;
		double somme2 = 0;
		double n = points.size();
		for(int i = 0; i < n; i++) {
			int j = (i + 1) == n ? 0 : i + 1;
			Vecteur p1 = points.get(i).sub(getPosition());
			Vecteur p2 = points.get(j).sub(getPosition());
			somme1 += Math.abs(p2.produitCroix(p1)) * (p2.produitScalaire(p1) + p1.produitScalaire(p1));
			somme2 += Math.abs(p2.produitCroix(p1));
		}
		return masse / 6.0 * somme1 / somme2 * ratio;
	}
	
	/**
	 * @return Le plus petit rectangle englobant le polygone.
	 */
	@Override
	public Rectangle2D.Double getRectangle() {
		double minX, maxX;
		double minY, maxY;
		Vecteur point = points.get(0);
		
		minX = maxX = point.getX();
		minY = maxY = point.getY();
		
		for(int i = 1 ; i < points.size() ; i++) {
			point = points.get(i);
			minX = Math.min(minX, point.getX());
			maxX = Math.max(maxX, point.getX());
			minY = Math.min(minY, point.getY());
			maxY = Math.max(maxY, point.getY());
		}
		
		return new Rectangle2D.Double(minX, minY, maxX - minX, maxY - minY);
	}

	/**
	 * @return Le point en bas � gauche du rectangle englobant le polygone.
	 */
	@Override
	public Vecteur getMin() {
		Rectangle2D rect = getRectangle();
		return new Vecteur(rect.getMinX(), rect.getMinY());
	}

	/**
	 * @return Le point en haut � droite du rectangle englobant le polygone.
	 */
	@Override
	public Vecteur getMax() {
		Rectangle2D rect = getRectangle();
		return new Vecteur(rect.getMaxX(), rect.getMaxY());
	}

	/**
	 * D�termine la "shape" du polygone.
	 * @param mat: Matrice de transformation.
	 * @return La "shape" du polygone.
	 */
	@Override
	public Shape getShape(AffineTransform mat) {
		if(getNombrePoints() > 0) {
			Vecteur sommet = getPoint(0);
			Path2D.Double contour = new Path2D.Double();
			contour.moveTo(sommet.getX(), sommet.getY());

			for(int i = 1; i <= getNombrePoints(); i++) {
				sommet = getPoint(i >= getNombrePoints() ? 0 : i);
				contour.lineTo(sommet.getX(), sommet.getY());
			}
			contour.closePath();
			return mat.createTransformedShape(contour);
		} else {
			return null;
		}
	}
	
	/**
	 * Retourne la normale "sp�ciale" � utiliser lors de collision. Cette normale vient remplacer la normale
	 * r�elle dans certains cas. Utilis� pour les collisions avec les murs de la sc�ne.
	 * @return La normale sp�ciale du polygone ou null s'il n'y en a pas.
	 */
	public Vecteur getNormaleSpeciale(){
		return this.normaleSpeciale;
	}
	
	/**
	 * Change la normale sp�ciale du polygone.
	 * @param normaleSpeciale: La nouvelle normale sp�ciale.
	 */
	public void setNormaleSpeciale(Vecteur normaleSpeciale){
		this.normaleSpeciale = normaleSpeciale;
	}
}
