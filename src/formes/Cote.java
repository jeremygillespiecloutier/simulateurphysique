package formes;

import java.io.Serializable;

import maths.Vecteur;

/**
 * @author Francis Labelle
 * Classe repr�sentant un c�t�.
 */
public class Cote implements Serializable{
	private static final long serialVersionUID = -3279730389068872113L;
	private Vecteur direction;
	private Vecteur p1;
	private Vecteur p2;
	
	/**
	 * Construit un c�t� � partir de deux points.
	 * @param p1 : Point 1 du c�t�.
	 * @param p2 : Point 2 du c�t�.
	 */
	public Cote(Vecteur p1, Vecteur p2) {
		this.p1 = p1;
		this.p2 = p2;
		this.direction = p2.sub(p1).unitaire();
	}

	/**
	 * Retourne la direction du c�t� sous forme de vecteur unitaire.
	 * @return Un vecteur unitaire repr�sentant la direction du c�t�.
	 */
	public Vecteur getDirection() {
		return new Vecteur(this.direction);
	}

	/**
	 * Retourne le premier point utilis� pour construire le c�t�.
	 * @return Le premier point.
	 */
	public Vecteur getPoint1() {
		return new Vecteur(this.p1);
	}
	
	/**
	 * Retourne le deuxi�me point utilis� pour construire le c�t�.
	 * @return Le deuxi�me point.
	 */
	public Vecteur getPoint2() {
		return new Vecteur(this.p2);
	}

	/**
	 * Retourne le point ayant la plus petite coordonn�e en X
	 * @return Le point ayant la plus petite coordonn�e en X.
	 */
	public Vecteur getMinX() {
		if (p1.getX() <= p2.getX()) {
			return new Vecteur(p1);
		} else {
			return new Vecteur(p2);
		}
	}

	/**
	 * Retourne le point ayant la plus grande coordonn�e en X.
	 * @return Le point ayant la plus grande coordonn�e en X.
	 */
	public Vecteur getMaxX() {
		if (p1.getX() >= p2.getX()) {
			return new Vecteur(p1);
		} else {
			return new Vecteur(p2);
		}
	}

	/**
	 * Retourne le point ayant la plus petit coordonn�e en Y.
	 * @return Le point ayant la plus petite coordonn�e en Y.
	 */
	public Vecteur getMinY() {
		if (p1.getY() <= p2.getY()) {
			return new Vecteur(p1);
		} else {
			return new Vecteur(p2);
		}
	}

	/**
	 * Retourne le point ayant la plus grande coordonn�e en Y.
	 * @return Le point ayant la plus grande coordonn�e en Y.
	 */
	public Vecteur getMaxY() {
		if (p1.getY() >= p2.getY()) {
			return new Vecteur(p1);
		} else {
			return new Vecteur(p2);
		}
	}
}
