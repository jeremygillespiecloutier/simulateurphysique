package formes;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import maths.Projection;
import maths.Vecteur;
import collision.Collision;

/**
 * @author Francis Labelle
 * Classe repr�sentant un cercle.
 */
public class Cercle extends Forme implements Serializable{
	private static final long serialVersionUID = 2129951270411045307L;
	private double rayon = 0.0;

	/**
	 * Construit un cercle centr� � l'origine.
	 * @param rayon : Rayon du cercle.
	 */
	public Cercle(double rayon) {
		this.rayon = rayon;
		setAire(trouverAire());
		this.setShapeInitiale(getShape(new AffineTransform()));
		setPositionInitiale(this.getPosition());
	}
	
	/**
	 * Construit un cercle centr� � la position donn�e.
	 * @param pos : Centre du cercle.
	 * @param rayon : Rayon du cercle.
	 */
	public Cercle(Vecteur pos, double rayon) {
		this.rayon = rayon;
		setPosition(pos);
		setAire(trouverAire());
		this.setShapeInitiale(getShape(new AffineTransform()));
		setPositionInitiale(this.getPosition());
	}

	/**
	 * Construit un cercle � partir d'un autre cercle.
	 * @param cercle : Cercle � copier.
	 */
	public Cercle(Cercle cercle) {
		this.rayon = cercle.rayon;
		setPosition(cercle.getPosition());
		setAire(trouverAire());
		this.setRotation(cercle.getRotation());
		this.setShapeInitiale(cercle.getShapeInitiale());
		setPositionInitiale(cercle.getPositionInitiale());
	}

	/**
	 * D�termine si le cercle est en intersection avec une autre forme.
	 * @param forme : Forme � tester.
	 * @return Vrai si les formes sont en intersection.
	 */
	@Override
	public boolean intersecte(Forme forme) {
		return Collision.testerIntersection(this, forme) == null;
	}

	/**
	 * @return Le rayon du cercle.
	 */
	public double getRayon() {
		return rayon;
	}

	/**
	 * Change le rayon du cercle.
	 * @param rayon : Nouveau rayon du cercle.
	 */
	public void setRayon(double rayon) {
		this.rayon = rayon;
	}

	/**
	 * Projette le cercle sur un axe.
	 * @param axe: Axe sur lequel projeter le cercle.
	 * @return La projection du cercle sur l'axe.
	 */
	@Override
	public Projection projeter(Vecteur axe) {
		double centre = getPosition().produitScalaire(axe);
		return new Projection(centre - rayon, centre + rayon);
	}
	
	/**
	 * @return L'aire du cercle.
	 */
	@Override
	public double trouverAire() {
		return Math.PI * rayon * rayon;
	}
	
	/**
	 * D�termine l'inertie du cercle en fonction de sa masse et sa densit�.
	 * @param masse: Masse du cercle.
	 * @param densite: Densit� du cercle.
	 * @return L'inertie associ�e au cercle.
	 */
	@Override
	public double calculerInertie(double masse, double densite) {
		//ratio pour compenser la formule pour sphere plane
		double ratio=15;
		return 1/4.0*masse*rayon*rayon*ratio;
	}

	/**
	 * @return Le plus petit rectangle englobant le cercle.
	 */
	@Override
	public Rectangle2D.Double getRectangle() {
		return new Rectangle2D.Double(getPosition().getX() - rayon, getPosition().getY() - rayon, 2 * rayon, 2 * rayon);
	}

	/**
	 * @return Le point en bas � gauche du rectangle englobant le cercle.
	 */
	@Override
	public Vecteur getMin() {
		return getPosition().sub(new Vecteur(rayon, rayon));
	}

	/**
	 * @return Le point en haut � droite du rectangle englobant le cercle.
	 */
	@Override
	public Vecteur getMax() {
		return getPosition().add(new Vecteur(rayon, rayon));
	}

	/**
	 * D�termine la "shape" de la forme.
	 * @param mat: Matrice de transformation.
	 * @return La "shape" de la forme.
	 */
	@Override
	public Shape getShape(AffineTransform mat) {
		Ellipse2D.Double forme = new Ellipse2D.Double(getPosition().getX() - rayon, getPosition().getY() - rayon, rayon * 2, rayon * 2);
		return forme;
	}
}
