package formes;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import maths.Projection;
import maths.Vecteur;

/**
 * @author Francis Labelle
 * Classe de base pour les formes (Polygone et Cercle).
 */
public abstract class Forme implements Serializable{
	private static final long serialVersionUID = 331884577068140227L;
	private Vecteur position = new Vecteur();
	private double rotation = 0.0;
	private double aire=0;
	private Shape shapeInitiale;
	private Vecteur positionInitiale=new Vecteur();
	/**
	 * Constructeur par d�faut.
	 */
	public Forme() {
	}

	/**
	 * Constructeur de copie.
	 * @param forme : Forme � copier.
	 */
	public Forme(Forme forme) {
		this.position = forme.position;
		this.rotation = forme.rotation;
		this.aire = forme.aire;
		shapeInitiale = forme.shapeInitiale;
	}

	/**
	 * @return La position de la forme.
	 */
	public Vecteur getPosition() {
		return position;
	}

	/**
	 * Change la position de la forme.
	 * @param position: Nouvelle position de la forme.
	 */
	public void setPosition(Vecteur position) {
		this.position = position;
	}

	/**
	 * D�place la forme.
	 * @param deplacement : D�placement � effectuer.
	 */
	public void deplacer(Vecteur deplacement) {
		this.position = this.position.add(deplacement);
	}

	/**
	 * @return L'orientation de la forme.
	 */
	public double getRotation() {
		return rotation;
	}

	/**
	 * Change l'orientation de la forme.
	 * @param rotation : La nouvelle orientation de la forme.
	 */
	public void setRotation(double rotation) {
		tourner(this.rotation - rotation);
	}

	/**
	 * Tourne la forme.
	 * @param rotation : Rotation � effectuer.
	 */
	public void tourner(double rotation) {
		this.rotation =(this.rotation+ rotation)%(2.0*Math.PI);
	}
	
	/**
	 * @return Le plus petit rectangle qui englobe la forme.
	 */
	public abstract Rectangle2D.Double getRectangle();

	/**
	 * D�termine si la forme est en intersection avec une autre.
	 * @param forme : Forme � tester.
	 * @return Vrai si les formes sont en intersection.
	 */
	public abstract boolean intersecte(Forme forme);

	/**
	 * D�termine si la forme contient un point.
	 * @param point : Point � tester.
	 * @return Vrai si la forme contient le point.
	 */
	public boolean contient(Vecteur point) {
		Shape shape = getShape(new AffineTransform());
		if (shape != null) {
			Area aire = new Area(getShape(new AffineTransform()));
			return aire.contains(new Point2D.Double(point.getX(), point.getY()));
		} else {
			return false;
		}
	}
	
	/**
	 * D�termine si la forme contient une aire.
	 * @param aire : Aire � tester.
	 * @return Vrai si la forme contient l'aire.
	 */
	public boolean contient(Area aire){
		Shape shape = getShape(new AffineTransform());
		if (shape != null) {
			Area inter=new Area(aire);
			inter.intersect(new Area(shape));
			return !inter.isEmpty();
		} else {
			return false;
		}
	}
	
	/**
	 * @return L'aire de la forme.
	 */
	public double getAire() {
		return this.aire;
	}
	
	/**
	 * Change l'aire de la forme.
	 * @param aire : Aire de la forme.
	 */
	protected void setAire(double aire) {
		this.aire = aire;
	}

	/**
	 * Change la "Shape" initiale de la forme.
	 * @param shapeInitiale : La "Shape" initiale.
	 */
	protected void setShapeInitiale(Shape shapeInitiale) {
		this.shapeInitiale = shapeInitiale;
	}

	/**
	 * Retourne la "Shape" initiale transform�e.
	 * @param mat : Matrice de transformation.
	 * @return La shape initiale de la forme.
	 */
	public Shape getShapeInitiale(AffineTransform mat) {
		return mat.createTransformedShape(this.shapeInitiale);
	}

	/**
	 * @return La "Shape" initiale.
	 */
	public Shape getShapeInitiale() {
		return this.shapeInitiale;
	}
	
	/**
	 * Retourne la "Shape" de la forme transform�e.
	 * @param mat : La matrice de transformation.
	 * @return La "Shape" de la forme transform�e.
	 */
	public abstract Shape getShape(AffineTransform mat);
	
	/**
	 * Trouve l'inertie de la forme � partir de la masse et la densit� donn�es.
	 * @param masse : Masse de la forme.
	 * @param densite : Densit� de la forme.
	 * @return L'inertie de la forme.
	 */
	public abstract double calculerInertie(double masse, double densite);
	
	/**
	 * Projette la forme sur l'axe donn�.
	 * @param axe : Axe sur lequel projeter la forme.
	 * @return La projection.
	 */
	public abstract Projection projeter(Vecteur axe);
	
	/**
	 * @return L'aire de la forme.
	 */
	public abstract double trouverAire();
	
	/**
	 * @return Le point en bas � gauche.
	 */
	public abstract Vecteur getMin();
	
	/**
	 * @return Le point en haut � droite.
	 */
	public abstract Vecteur getMax();
	/**
	 * Change la position initiale de la forme.
	 * @param positionInitiale La position initiale de la forme.
	 */
	public void setPositionInitiale(Vecteur positionInitiale){
		this.positionInitiale=positionInitiale;
	}
	/**
	 * Retourne la position initiale de la forme.
	 * @return La position initiale de la forme.
	 */
	public Vecteur getPositionInitiale(){
		return this.positionInitiale;
	}
}




