package ecouteurs;
/**
 * Classe �couteur pour les composants gravit�.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public interface EcouteurGravite {
	public void orientationChangee(double orientation);
}
