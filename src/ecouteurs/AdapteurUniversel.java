package ecouteurs;

import java.io.Serializable;

import maths.Vecteur;
import moteur.InfoAnimation;
import moteur.ObjetPhysique;
import moteur.Scene;
import physique.Corps;
import fonctions.Fonction;
import formes.Forme;
/**
 * Classe adapteur pour les écouteurs universels.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public class AdapteurUniversel implements EcouteurUniversel, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5765064491556483678L;

	@Override
	public void formeCree(Forme f) {
		// TODO Auto-generated method stub

	}

	@Override
	public void proprietesChangees() {
		// TODO Auto-generated method stub

	}

	@Override
	public void objetsChanges(boolean seulementAffichage) {
		// TODO Auto-generated method stub

	}

	@Override
	public void redessiner() {
		// TODO Auto-generated method stub

	}

	@Override
	public void sceneSelectionnee() {
		// TODO Auto-generated method stub

	}

	@Override
	public void tempsChange(InfoAnimation info) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void corpsSuivi(Corps c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void bougerScene(Vecteur position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void objetSelectionne(ObjetPhysique objet) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void objetCree(ObjetPhysique obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void zoomChange(double zoom) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ajouterFonction(Fonction f) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectionMultiple(boolean multiple, boolean deuxCorps) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean animationEnCours() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifierSelection(boolean estUnCorps) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ajouterScene(Scene scene) {
		// TODO Auto-generated method stub
		
	}

}
