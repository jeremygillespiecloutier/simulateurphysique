package ecouteurs;

import java.io.Serializable;

import maths.Vecteur;
import moteur.InfoAnimation;
import moteur.ObjetPhysique;
import moteur.Scene;
import physique.Corps;
import fonctions.Fonction;
import formes.Forme;
/**
 * �couteur universel pouvant �tre utilis� par diff�rentes classe.
 * @author Jeremy Gillespie-Cloutier
 *
 */
public interface EcouteurUniversel extends Serializable{
	public void formeCree(Forme f);

	public void objetSelectionne(ObjetPhysique objet);

	public void proprietesChangees();

	public void objetsChanges(boolean seulementAffichage);

	public void redessiner();

	public void sceneSelectionnee();
	
	public void tempsChange(InfoAnimation info);
	
	public void corpsSuivi(Corps c);
	
	public void bougerScene(Vecteur position);
	
	public void objetCree(ObjetPhysique obj);
	
	public void zoomChange(double zoom);
	
	public void ajouterFonction(Fonction f);
	
	public void selectionMultiple(boolean multiple, boolean deuxCorps);
	
	public boolean animationEnCours();
	
	public void verifierSelection(boolean estUnCorps);
	
	public void ajouterScene(Scene scene);
}
