package dessinable;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.io.Serializable;

import maths.Vecteur;
/**
 * 
 * @author Francis Labelle
 * Classe permettant de dessiner des vecteurs.
 */
public class VecteurDessinable implements Dessinable, Serializable {
	private static final long serialVersionUID = -151414814463626227L;
	private Vecteur vecteur;
	private Vecteur position = new Vecteur();
	private double echelle = 1.0;
	private double anglePointe = 3.0 * Math.PI / 4.0;
	private double taillePointe = 0.2;
	private double tailleTrait = 1.0;
	private Color couleur = Color.blue;
	
	/**
	 * Cr�e une vecteur dessinable.
	 * @param vecteur Le vecteur � dessiner.
	 */
	public VecteurDessinable(Vecteur vecteur) {
		this.vecteur = vecteur;
	}
	
	/**
	 * Cr�e un vecteur dessinable.
	 * @param vecteur Le vecteur � dessiner.
	 * @param position La position (origine) du vecteur � dessiner.
	 */
	public VecteurDessinable(Vecteur vecteur, Vecteur position) {
		this.vecteur = vecteur;
		this.position = position;
	}
	
	/**
	 * Retourne le vecteur dessin�.
	 * @return Le vecteur dessin�.
	 */
	public Vecteur getVecteur() {
		return vecteur;
	}
	
	/**
	 * Change le vecteur � dessiner.
	 * @param vecteur Le vecteur � dessiner.
	 */
	public void setVecteur(Vecteur vecteur) {
		this.vecteur = vecteur;
	}
	
	/**
	 * Retourne la position du vecteur dessin�.
	 * @return La position (origine) du vecteur dessin�.
	 */
	public Vecteur getPosition() {
		return position;
	}
	
	/**
	 * Change la position du vecteur � dessiner.
	 * @param position La position (origine) du vecteur � dessiner.
	 */
	public void setPosition(Vecteur position) {
		this.position = position;
	}
	
	/**
	 * Retourne la valeur utilis�e comme �chelle d'agrandissement dans le dessin du vecteur.
	 * @return L'�chelle d'agrandissement.
	 */
	public double getEchelle() {
		return echelle;
	}
	
	/**
	 * Change l'�chelle d'agrandissement du dessin.
	 * @param echelle L'�chelle d'agrandissement du dessin.
	 */
	public void setEchelle(double echelle) {
		this.echelle = echelle;
	}
	
	/**
	 * Retourne l'angle entre le vecteur et la pointe.
	 * @return L'angle entre le vecteur et la pointe (radians).
	 */
	public double getAnglePointe() {
		return anglePointe;
	}
	
	/**
	 * Change l'angle entre le vecteeur et la pointe.
	 * @param anglePointe L.angle entre le vecteur et la pointe (radians).
	 */
	public void setAnglePointe(double anglePointe) {
		this.anglePointe = anglePointe;
	}
	
	/**
	 * Retourne la taille de la pointe du vecteur.
	 * @return La taille de la pointe du vecteur.
	 */
	public double getTaillePointe() {
		return taillePointe;
	}
	
	/**
	 * Change la taille de la pointe du vecteur.
	 * @param taillePointe La taille de la pointe du vecteur.
	 */
	public void setTaillePointe(double taillePointe) {
		this.taillePointe = taillePointe;
	}
	
	/**
	 * Retourne la taille de trait utilis� dans le dessin du vecteur.
	 * @return La taille de trait utilis�.
	 */
	public double getTailleTrait() {
		return tailleTrait;
	}
	
	/**
	 * Change la taille du trait utilis� dans le dessin du vecteur.
	 * @param tailleTrait La taille de trait utilis�.
	 */
	public void setTailleTrait(double tailleTrait) {
		this.tailleTrait = tailleTrait;
	}
	
	/**
	 * Retourne la couleur utilis�e dans le dessin du vecteur.
	 * @return La couleur utilis�e dans le dessin du vecteur.
	 */
	public Color getCouleur() {
		return couleur;
	}
	
	/**
	 * Change la couleur utilis�e dans le dessin du vecteur.
	 * @param couleur La couleur utilis�e dans le dessin du vecteur.
	 */
	public void setCouleur(Color couleur) {
		this.couleur = couleur;
	}
	
	/**
	 * Dessine le vecteur.
	 * @param g2d L'objet graphique sur lequel le vecteur est dessin�
	 * @param mat La matrice de transformation utilis�e dans le dessin
	 */
	@Override
	public void dessiner(Graphics2D g2d, AffineTransform mat, double scale) {
		Color couleurInitiale = g2d.getColor();
		Stroke traitInitial = g2d.getStroke();
		
		double x = position.getX();
		double y = position.getY();
		double vx = vecteur.getX();
		double vy = vecteur.getY();
		double dx2 = x + vx * echelle;
		double dy2 = y + vy * echelle;
		
		g2d.setColor(couleur);
		g2d.setStroke(new BasicStroke((float)tailleTrait));
		
		Line2D.Double direction = new Line2D.Double(x, y, dx2, dy2);
		Line2D.Double tete = new Line2D.Double(dx2, dy2, dx2 + vx * taillePointe * echelle, dy2 + vy * taillePointe * echelle);
		
		g2d.draw(mat.createTransformedShape(direction));
		
		mat.rotate(anglePointe, direction.getX2(), direction.getY2());
		g2d.draw(mat.createTransformedShape(tete));
		
		mat.rotate(-2.0 * anglePointe, direction.getX2(), direction.getY2());
		g2d.draw(mat.createTransformedShape(tete));
		
		mat.rotate(anglePointe, direction.getX2(), direction.getY2());
		
		g2d.setStroke(traitInitial);
		g2d.setColor(couleurInitiale);
	}
}
