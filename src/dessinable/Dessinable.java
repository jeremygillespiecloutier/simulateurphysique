package dessinable;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.Serializable;

/**
 * 
 * @author Francis Labelle
 * Interface pour le dessin d'objets.
 */
public interface Dessinable extends Serializable{
	/**
	 * Dessiner l'objet.
	 * @param g2d : Contexte graphique.
	 * @param mat : Matrice de transformation.
	 * @param scale : �chelle de l'objet.
	 */
	public abstract void dessiner(Graphics2D g2d, AffineTransform mat, double scale);
}
