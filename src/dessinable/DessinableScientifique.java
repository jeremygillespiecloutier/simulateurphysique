package dessinable;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * @author Francis Labelle
 * Interface pour les objets dont le dessin change en mode scientifique.
 */
public interface DessinableScientifique extends Dessinable {
	/**
	 * Dessine l'objet en mode scientifique.
	 * @param g2d : Contexte graphique.
	 * @param mat : Matrice de transformation.
	 */
	public abstract void dessinerScientifique(Graphics2D g2d, AffineTransform mat);
}
