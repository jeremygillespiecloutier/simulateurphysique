package fonctions;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.io.Serializable;

import maths.Vecteur;
import enums.TypeFonction;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe pour d�placer les corps avec une trajectoire de fonction trigonom�trique.
 */
public class FonctionTrigo extends Fonction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1296101941591249401L;
	private double amplitude;
	private double lambda;
	private int nbCycles;
	private boolean inversion;
	private double position=0;
	private double periode=0;
	private int augmentation=1;
	private int sens=1;
	private Vecteur vPos=new Vecteur(0,0);
	/**
	 * Constructeur de fonction trigonom�trique.
	 * @param f La fonction � copier.
	 */
	public FonctionTrigo(FonctionTrigo f){
		super(f.getVitesse(), f.getType(), f.getNom());
		this.amplitude=f.getAmplitude();
		this.lambda=f.getLambda();
		this.nbCycles=f.getNbCycles();
		this.inversion=f.getInversion();
		this.position=f.getPosition();
		this.periode=f.getPeriode();
		this.augmentation=f.getAugmentation();
		this.sens=f.getSens();
		this.vPos=f.getVPos();
		setVelocite(f.getVelocite());
		creerForme();
	}
	/**
	 * Constructeur de fonction trigonom�trique.
	 * @param amplitude L'amplitude de la fonction.
	 * @param lambda La longueur d'onde de la fonction.
	 * @param nbCycles Le nombre de cycles � �x�cuter par la fonction avant de s'inverser.
	 * @param inversion Bool�en indiquant si l'inversion change le sens d'amplitude de la fonction.
	 * @param vitesse La vitesse de la fonction.
	 * @param type Le type de la fonction (sinus ou cosinus).
	 * @param nom Le nom de la fonction.
	 */
	public FonctionTrigo(double amplitude, double lambda, int nbCycles, boolean inversion, double vitesse, TypeFonction type, String nom){
		super(vitesse, type, nom);
		this.lambda=lambda;
		this.amplitude=amplitude;
		this.nbCycles=nbCycles;
		this.inversion=inversion;
		periode=2*Math.PI/lambda;
		creerForme();
		if(type==TypeFonction.SINUS){
			setVelocite(new Vecteur(0, amplitude>0?1:-1).mul(vitesse));
		}else{
			setVelocite(new Vecteur(1, 0).mul(vitesse));
		}
	}
	/**
	 * Cr�e la forme de la fonction.
	 */
	private void creerForme(){
		double precision=20;
		Path2D.Double chemin=new Path2D.Double();
		chemin.moveTo(0, 0);
		if(getType()==TypeFonction.COSINUS){
			vPos=new Vecteur(0, amplitude);
			for(double i=0;i<(double)nbCycles*lambda;i+=1/precision){
				double y=amplitude*Math.cos(i*periode)-amplitude;
				chemin.lineTo(i,y);
			}
		}else{
			for(double i=0;i<(double)nbCycles*lambda;i+=1/precision){
				double y=amplitude*Math.sin(i*periode);
				chemin.lineTo(i,y);
			}
			if(inversion){
				for(double i=(double)nbCycles*lambda;i>=0;i-=1/precision){
					double y=amplitude*Math.sin(i*periode);
					chemin.lineTo(i,-y);
				}
			}
		}
		setForme(chemin);
	}
	public String toString() {
		String fct;
		if(getType()==TypeFonction.SINUS){
			fct="sin";
		}else{
			fct="cos";
		}
		String n=getNom().equals("")?"":"["+getNom()+"] ";
		return n+"y="+amplitude+fct+"(2\u03C0/"+lambda+"x)"+" | v="+getVitesse();
	}
	public Vecteur getDeplacement(double deltaT) {
		double derivee=0;
		if(getType()==TypeFonction.SINUS){
			derivee=amplitude*periode*Math.cos(position*periode);
		}else{
			derivee=-amplitude*periode*Math.sin(position*periode);
		}
		double difference=getVitesse()/Math.sqrt(1+derivee*derivee)*deltaT*augmentation;
		double positionFinale=0;
		Vecteur position1=new Vecteur(0,0);
		Vecteur position2=new Vecteur(0,0);
		if(position+difference>nbCycles*lambda){
			positionFinale=(nbCycles*lambda)-((position+difference)-nbCycles*lambda);
			augmentation*=-1;
			if(inversion){
				sens*=-1;
			}
		}else if(position+difference<0){
			positionFinale=-(position+difference);
			augmentation*=-1;
			if(inversion){
				sens*=-1;
			}
		}else{
			positionFinale=position+difference;
		}
		if(getType()==TypeFonction.SINUS){
			position1=new Vecteur(position, sens*amplitude*Math.sin(position*periode));
			position=positionFinale;
			position2=new Vecteur(position, sens*amplitude*Math.sin(position*periode));
		}else{
			position1=new Vecteur(position, sens*amplitude*Math.cos(position*periode));
			position=positionFinale;
			position2=new Vecteur(position, sens*amplitude*Math.cos(position*periode));
		}
		vPos=position2;
		Vecteur velocite=position2.sub(position1);
		setVelocite(velocite.unitaire().mul(getVitesse()));
		return velocite;
	}
	public Fonction copie() {
		return new FonctionTrigo(this);
	}
	/**
	 * Retourne l'amplitude de la fonction.
	 * @return L'amplitude de la fonction.
	 */
	public double getAmplitude(){
		return this.amplitude;
	}
	/**
	 * Retourne la longueur d'onde de la fonction.
	 * @return La longueur d'onde de la fonction.
	 */
	public double getLambda(){
		return this.lambda;
	}
	/**
	 * Retourne le nombre de cycles de la fonction.
	 * @return Le nombre de cycles de la fonction.
	 */
	public int getNbCycles(){
		return this.nbCycles;
	}
	/**
	 * Retourne un bool�en indiquant si la fonction s'inverse.
	 * @return Bool�en indiquant si la fonction s'inverse.
	 */
	public boolean getInversion(){
		return this.inversion;
	}
	public double getPosition(){
		return this.position;
	}
	/**
	 * Retourne la p�riode de la fonction.
	 * @return La p�riode de la fonction.
	 */
	public double getPeriode(){
		return this.periode;
	}
	/**
	 * Retourne le sens d'augmentation de la fonction (positif ou n�gatif).
	 * @return Le sens d'augmentation de la fonction.
	 */
	public int getAugmentation(){
		return this.augmentation;
	}
	/**
	 * Retourne le sens de d�placement de la fonction (positif ou n�gatif).
	 * @return Le sens de d�placement de la fonction.
	 */
	public int getSens(){
		return this.sens;
	}
	@Override
	public void dessinerFonction(Graphics2D g2d, AffineTransform mat, Vecteur pos) {
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(Color.BLUE);
		g2d.setStroke(new BasicStroke(2.0f));
		mat.translate(pos.getX()-vPos.getX(), pos.getY()-vPos.getY());
		if(getType()==TypeFonction.COSINUS)
			mat.translate(0, amplitude);
		g2d.draw(mat.createTransformedShape(getForme()));
		if(getType()==TypeFonction.COSINUS)
			mat.translate(0, -amplitude);
		mat.translate(-(pos.getX()-vPos.getX()), -(pos.getY()-vPos.getY()));
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	public boolean equals(Object obj){
		if(obj!=null && obj.getClass().equals(FonctionTrigo.class)){
			FonctionTrigo f=(FonctionTrigo)obj;
			return super.equals(f) && this.amplitude == f.getAmplitude() && this.lambda==f.getLambda() && this.nbCycles==f.getNbCycles() && this.inversion==f.getInversion() && this.position==f.getPosition() && this.periode == f.getPeriode() && this.augmentation==f.getAugmentation() && this.sens==f.getSens() && this.vPos.equals(f.getVPos());
		}
		return false;
	}
	public Vecteur getVPos(){
		return this.vPos;
	}
}
