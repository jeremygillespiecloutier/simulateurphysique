package physique;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;

import enums.TypeObjet;
import maths.Vecteur;
import moteur.ObjetPhysique;
/**
 * 
 * @author Jeremy Gillespie-Cloutier
 * Classe repr�sentant une explosion qui applique une vitesse aux objets se situant dans son rayon.
 */
public class Explosion extends ObjetPhysique implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -199486747710203241L;
	private final double taille=0.3;
	private double rayon=0;
	private double vitesse=0;
	private Vecteur position=new Vecteur(0,0);
	/**
	 * Cr�e une explosion.
	 * @param vitesse La vitesse transmise aux objets touch�s par l'explosion.
	 * @param rayon Le rayon de l'explosion.
	 * @param position La position de l'explosion.
	 */
	public Explosion(double vitesse, double rayon, Vecteur position){
		this.vitesse=vitesse;
		this.rayon=rayon;
		this.position=position;
	}
	/**
	 * Dessine le mode scientifique de l'explosion
	 * @param g2d L'objet graphique sur lequel l,explosion est dessin�e
	 * @param mat La matrice de transformation 
	 */
	public void dessinerScientifique(Graphics2D g2d, AffineTransform mat) {
		
	}
	/**
	 * Dessine l'explosion
	 * @param g2d L'objet graphique sur lequel l,explosion est dessin�e
	 * @param mat La matrice de transformation
	 */
	public void dessiner(Graphics2D g2d, AffineTransform mat, double scale) {
		Color couleurInitiale=g2d.getColor();
		Stroke trait=g2d.getStroke();
		g2d.setColor(Color.BLACK);
		g2d.fill(mat.createTransformedShape(getShape()));
		if(getSelectionne()){
			g2d.setStroke(new BasicStroke(1.0f));
			g2d.setColor(Color.WHITE);
			g2d.draw(mat.createTransformedShape(getShape()));
		}
		g2d.setColor(couleurInitiale);
		g2d.setStroke(trait);
	}
	/**
	 * Retourne la vitesse de l'explosion.
	 * @return La vitesse de l'explosion.
	 */
	public double getVitesse(){
		return this.vitesse;
	}
	/**
	 * Retourne le rayon de l'explosion.
	 * @return Le rayon de l,explosion.
	 */
	public double getRayon(){
		return this.rayon;
	}
	/**
	 * Change la vitesse de l'explosion.
	 * @param vitesse La vitesse de l'explosion.
	 */
	public void setVitesse(double vitesse){
		this.vitesse=vitesse;
	}
	/**
	 * Change le rayon de l'explosion.
	 * @param rayon Le rayon de l,explosion.
	 */
	public void setRayon(double rayon){
		this.rayon=rayon;
	}
	/**
	 * Retourne un objet Shape utilis� pour le dessin de l'explosion.
	 * @return Objet Shape repr�sentant graphiquement l'explosion.
	 */
	private Shape getShape(){
		return new Ellipse2D.Double(position.getX()-taille/2.0, position.getY()-taille/2.0, taille, taille);
	}
	/**
	 * Retourne le type d'objet (EXPLOSION).
	 * @return le type d'objet (EXPLOSION).
	 */
	public TypeObjet getTypeObjet() {
		return TypeObjet.EXPLOSION;
	}
	/**
	 * Retourne une copie de l'explosion.
	 * @return Copie de l'explosion.
	 */
	public ObjetPhysique copier() {
		return new Explosion(vitesse, rayon, position);
	}
	/**
	 * D�termine si un point est contenu dans l'explosion.
	 * @param point Le vecteur � tester.
	 * @return Un bool�en indiquant si le point est contenu dans l'explosion.
	 */
	public boolean contient(Vecteur point) {
		return getShape().contains(new Point2D.Double(point.getX(), point.getY()));
	}
	/**
	 * Retourne la position de l'explosion
	 * @return La position de l'explosion.
	 */
	public Vecteur getPosition() {
		return this.position;
	}
	/**
	 * Change la position de l,explosion
	 * @param position La position de l'explosion
	 */
	public void setPosition(Vecteur position) {
		this.position=position;
	}
	/**
	 * Retourne le nom de l'explosion en fonction d'un indice pour son affichage dans la liste d'objets.
	 * @param numero L'indice de l'explosion.
	 * @return Le nom de l,explosion.
	 */
	public String getNomFamilier(int numero) {
		return "Explosion "+numero;
	}
	@Override
	public boolean contient(Area aire) {
		Area inter=new Area(aire);
		inter.intersect(new Area(getShape()));
		return !inter.isEmpty();
	}
	@Override
	public Vecteur getMin() {
		Rectangle2D rect=getShape().getBounds2D();
		return new Vecteur(rect.getMinX(), rect.getMinY());
	}
	@Override
	public Vecteur getMax() {
		Rectangle2D rect=getShape().getBounds2D();
		return new Vecteur(rect.getMaxX(), rect.getMaxY());
	}
	/**
	 * M�thode qui applique l'explosion � un corps.
	 * @param c Le corps sur lequel s'explosion est appliqu�e.
	 */
	public void appliquer(Corps c){
		Vecteur distance=c.getPosition().sub(getPosition());
		Ellipse2D.Double zone=new Ellipse2D.Double(position.getX()-rayon, position.getY()-rayon, rayon*2.0, rayon*2.0);
		Area inter=new Area(zone);
		inter.intersect(new Area(c.getForme().getShape(new AffineTransform())));
		if(!inter.isEmpty() && !c.getFixe()){
			c.setVitesse(c.getVitesse().add(distance.unitaire().mul(getVitesse())));
		}
	}
}
